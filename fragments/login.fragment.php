<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Login Fragment
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

$loginmessage = "";
if(isset($_REQUEST['message'])) {
	switch($_REQUEST['message']) {
		case 'empty':
			$loginmessage = "Please enter fields properly!";
			break;
		case 'auth':
			$loginmessage = "Invalid login";
			break;
		default:
			$loginmessage = "";
			break;
	}
}
$loginform_token = uniqid();
$_SESSION['loginform_token'] = $loginform_token;
 
if(isset($frag)) {
?>

<div class="container">
<h3>Login</h3>
<div class="bbar"></div>
<form name="loginform" id="loginform" method="post" action="index.php">
	<table class="form" style="width: 99%;">
	<tr>
		<td>
			<label for="username">Username</label><br />
			<input type="text" name="username" id="username" style="width: 99%;" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="password">Password</label><br />
			<input type="password" name="password" id="password" style="width: 99%;" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="users/do_login" />
			<input type="hidden" name="loginform_token" value="<?php echo $loginform_token; ?>" />
			<input type="submit" name="login" id="login" value="Login" class="loginbutton" />
		</td>
	</tr>
	</table>
</form>
<p class="para"><small>Not registered? <a href="index.php?route=users/registration">Sign up</a></small></p>
<?php 
}
?>
</div>
