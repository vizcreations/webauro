<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Logout Fragment
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
?>

<div class="container">
<h3>Hi <?php echo $this->registry->username; ?>!</h3>
<div class=""></div>
<form name="logoutform" id="logoutform" method="post" action="index.php?route=users/logout" style="padding: 0;">
<ul class="nav">
	<li><a href="index.php?route=users/mypage">My page</a></li>
	<li><a href="index.php?route=users/profile/<?php echo $this->registry->username; ?>">Profile</a></li>
</ul>
<table class="form">
	<tr>
		<td>
			<input type="submit" name="logout" id="logout" value="Logout" class="loginbutton" />
		</td>
	</tr>
</table>
</form>
</div>
