<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Menu Fragment
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

if($frag) {
	if($menuitems) {
?>
	<ul id="sddm" class="navigation">
<?php
		$model = new MenuFragModel();
		for($i=0; $i<count($menuitems); $i++) {
			$menuitem = $menuitems[$i];
			try {
				$subitems = $model->getSubMenuItems($menuitem['menuitem_id']);
			} catch(Exception $e) {
				$subitems = 0;
			}
?>
	<li><a href="index.php?route=<?php echo $menuitem['menuitem_link']; ?>" onmouseover="mopen('m<?php echo $menuitem['menuitem_id']; ?>');" onmouseout="mclosetime();"><?php echo $menuitem['menuitem_name']; ?></a>
<?php
			if($subitems) {
				echo '<div id="m'.$menuitem['menuitem_id'].'" onmouseover="mcancelclosetime();" onmouseout="mclosetime();" style="display: none;">';
				for($j=0; $j<count($subitems); $j++) {
					$subitem = $subitems[$j];
					echo '<a href="index.php?route='.$subitem['menuitem_link'].'">'.$subitem['menuitem_name'].'</a><br />';
				}
				echo '</div>';
			}
			echo '</li>';
		}
?>
	</ul>
<?php
	}
}
?>
