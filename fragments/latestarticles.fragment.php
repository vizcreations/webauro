<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Latest Articles Fragment
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
if($frag) {
?>

<div class="container">
<h2><?php echo $frag['frag_title']; ?></h2>
<div class="ybar"></div>

<?php
if(!$articles) {
?>
<p class="para red">No articles created yet!</p>
<?php 
} else {
?>
<ul class="nav">
<?php
	for($i=0; $i<count($articles); $i++) {
		$article = $articles[$i];
?>
<li><a href="index.php?route=articles/full_article/<?php echo $article['article_id']; ?>"><?php echo strlen($article['article_title']) > 35 ? substr($article['article_title'], 0, 35).'..' : $article['article_title']; ?></a></li>
<?php
	}
?>
</ul>
<?php
}
?>
<p class="para right"><small class="small"><a href="index.php?route=articles/latest_category_articles/<?php echo $fragargument; ?>">more..</a></small></p>
</div>
<?php
}
?>
