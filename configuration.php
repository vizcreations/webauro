<?php

if(!defined( "__ROOT" )) die( "Unauthorized access!" );

/**
 * @abstract Application Configuration to be used globally
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

// Default application variables
$app = array();
$app['sitename'] 		= "Webauro"; /** No dots or spaces to be inserted when changed */
$app['hostip']			= '127.0.0.1';
$app['platform'] 		= 'PHP 5';
$app['dbms'] 			= 'MySQL 5';
$app['mode'] 			= 0; // 1 for testing, 2 for live, 0 for dev

// Data server variables
$dbserver = array();
$dbserver['host']		= 'localhost';
$dbserver['user']		= '';
$dbserver['pass']		= '';
$dbserver['schema']		= '';
$dbserver['dbms']		= 'mysql';

// Web server configuration variables
$webserver['htaccess']		= false;
$webserver['session']		= false;
$webserver['querystring']	= false;
$webserver['router']		= array("controller" => "index", "action" => "index");
$webserver['singleton']		= true;
$webserver['caching']		= false; // If changing to true, make sure you have a system for clearing the cache/ directory at intervals
$webserver['url']		= 'http://localhost/webauro'; // Use the domain where your app resides currently

// Web template selection variables
$template['curtemplate']	= "1";
$template['deftemplate']	= "1";

// Administrator settings
$admin['email'] 		= "me@mymail.com"; // This email will be used for the "From" address in mail()
$admin['emailex'] 		= "Me <me@mymail.com>"; // As above but with a name with email in the "From" address

