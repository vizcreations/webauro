<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Database/Model Base Class to apply business logic for the application
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class definition for the Database */

Abstract class DB {
	/**
	 * 
	 * @var mixed
	 */
	private static $instance = null;
	public static $error = null;
	public static $conn = null;

	public static $dbhost = '127.0.0.1';
	public static $dbuser = 'root';
	public static $dbpass = '';
	public static $dbschema = 'vcwcms';
	public $record;

	/**
	 * @var resource
	 * data which the model holds
	 */
	protected static $data;

	/**
	 * 
	 * @var integer
	 * holds the id of a row
	 */
	public static $id;

	/**
	 * 
	 * @var string
	 * stores string from latest setQuery() method
	 */
	protected $query = null;
	
	public function DB() {
		$this->record = new stdClass();
	}

	private static function setConnection() {
		$xmlDoc = new DOMDocument();
/*		$config = __ROOT.DS.'configuration.xml'; // Legacy ##CODE## DO NOT USE XML for configuration
		if(file_exists($config)) {
			$xmlDoc->load($config);
			$dbhost = $xmlDoc->getElementsByTagName('dbhost')->item(0)->nodeValue;
			$dbuser = $xmlDoc->getElementsByTagName('dbuser')->item(0)->nodeValue;
			$dbpass = $xmlDoc->getElementsByTagName('dbpass')->item(0)->nodeValue;
			$dbname = $xmlDoc->getElementsByTagName('dbname')->item(0)->nodeValue; // Deprecated Legacy code commented out
		} else {
*/			$config = __ROOT.DS.'configuration.php';
			if(file_exists($config)) {
				$dbhost = self::$dbhost; // These were already set..
				$dbuser = self::$dbuser;
				$dbpass = self::$dbpass;
				$dbname = self::$dbschema;
			} else {
				self::setError( 'Configuration not found. Method: setConnection() in file: '.__FILE__.'; line: '.__LINE__ );
			}
//		}
		if(self::$conn == null) {
			self::$conn = mysqli_connect($dbhost, $dbuser, $dbpass);
			if(!self::$conn) {
				self::setError( '<b>Application failed:</b> Database connection error in '.__FILE__.'; line: '.__LINE__ );
			}
			if(!mysqli_select_db(self::$conn, $dbname)) self::setError( '<b>Application failed:</b> Database connection error in '.__FILE__.'; line: '.__LINE__ );
			self::$instance = self::$conn;
			return self::$instance;
		}
	}

	public static function setError( $string ) {
		self::$error = $string;
	}

	public static function getInstance() {
		if(self::$conn == null) {
			self::setConnection();
		} else {
			return self::$instance;
		}
	}
	
	public static function closeInstance() {
		if(self::$conn != null)
			mysqli_close(self::$conn);
		self::$instance = null;
		self::$conn = null;
	}

	/**
	 * 
	 * @param $vars
	 * @param $entity
	 * @param $option
	 * @return void
	 */
	abstract function save($vars, $entity, $option);

	/**
	 * 
	 * @param $vars
	 * @param $fields
	 * @return bool
	 */
	protected function bind($vars, $fields) {
		if(is_array($vars) && is_array($fields)) {
			if(count($vars) == count($fields)) {
				return true;
			} else {
				$this->setError( "<b>Application failed:</b> Database transaction error (Cannot bind values). Method: bind() in file: ".__FILE__."; line: ".__LINE__ );
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param $query
	 * @return void
	 */
	public function setQuery($query) {
//		$this->query = str_replace("#__", "tbl_", $query);
//		$this->query = $query;
		$this->query = str_replace("#__", "wb_", $query);
	}

	public function execute() {
		//$query = str_replace("#__", "tbl_", $query);
		$result = @mysqli_query(self::$conn, $this->query);
		if(!$result) {
			$message = "<b>Application failed:</b> (".mysqli_error(self::$conn).")";
			throw new Exception($message);
			return false;
		} else {
			return true;
		}
	}


	/**
	* @pull out scalar value
	*/
	public function getResult() {
		if(!$this->query == null) {
			$resource = mysqli_query(self::$conn, $this->query);
			if(!$resource) {
				$message = "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")";
				throw new Exception($message);
				return false;
			} else {
				if(mysqli_num_rows($resource) > 0)
					$row = mysqli_fetch_row($resource); // Only numeric
				else
					$row[0] = 0;
				return $row[0];
			}
		} else {
			throw new Exception( "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")" );
		}
	}

	/**
	* @pull out scalar value
	*/
	public function getCount() {
		if(!$this->query == null) {
			$resource = mysqli_query(self::$conn, $this->query);
			if(!$resource) {
				$message = "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")";
				throw new Exception($message);
			} else {
				$num = mysqli_num_rows($resource);
				return $num;
			}
		} else {
			throw new Exception( "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")" );
		}
	}

	/**
	* @pull out rows of objects mapping table fields
	*/
	public function getRows() {
		$rows = array();
		$obj = NULL;
		if($this->query != null) {
			$resource = mysqli_query(self::$conn, $this->query);
			if(!$resource) {
				$message = "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")";
				throw new Exception($message);
				return false;
			} else {
				$count = $this->getRowsCount();
				if($count > 0) {
					while($row = mysqli_fetch_array($resource)) {
						$rows[] = $row;
					}

/*					while($obj=mysqli_fetch_object($resource))
						$rows[] = $this->record = $obj;
*/
					return $rows;
				} else {
					return 0;
				}
			}
		} else {
			throw new Exception( "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")" );
		}
	}

	public function getRow() {
		$row = $obj = NULL;
		if($this->query != null) {
			$resource = mysqli_query(self::$conn, $this->query);
			if(!$resource) {
				$message = "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")";
				throw new Exception($message);
				return false;
			} else {
				$count = $this->getCount();
				if($count > 0) {
					$row = mysqli_fetch_array($resource, MYSQL_BOTH);
//					$obj = $this->record = mysqli_fetch_object($resource);
//					return $obj;
					return $row;
				} else {
					return 0;
				}
			}
		}
	}

	public function getRowsCount() {
		if($this->query != null) {
			$resource = mysqli_query(self::$conn, $this->query);
			if(!$resource) {
				$message = "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")";
				throw new Exception($message);
			} else {
				$count = mysqli_num_rows($resource);
				return $count;
			}
		} else {
			throw new Exception( "<b>Application failed:</b> Database transaction error (".mysqli_error(self::$conn).")" );
		}
	}

	/**
	 * 
	 * @return integer
	 * @abstract Gets the last value saved in the increment column
	 */
	public function getInsertId() {
		$id = mysqli_insert_id(self::$conn);
		self::$id = $id;
		return $id;
	}

    public function escapeString($vars) {
        if(!is_array($vars)) {
        $vars = mysqli_real_escape_string(self::$conn, $vars);
        } else {
            foreach($vars as $key => $val) {
                if(!is_array($val)) {
                    $vars[$key] = mysqli_real_escape_string(self::$conn, $val);
                } else {
                    foreach($val as $kkey => $vval) {
                        $vars[$key][$kkey] = mysqli_real_escape_string(self::$conn, $vval);
                    }
                }
            }
        }
        return $vars;
    }

	public function escapeSQL($vars) {
		$this->escapeString($vars);
	}
};
