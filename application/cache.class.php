<?php

if(!defined( "__ROOT" )) die( "Unauthorized access!" );

/**
 * @abstract Front End Cache Core Class for web page caching
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

class Cache {
	public $html;
	private $buffer;
	private $cachedFile = null;

	public function createCacheFile($zero, $one, $two=0) {
		$cacheFile = __ROOT.DS."cache".DS.$zero."_".$one."_".$two.".html";
		if(is_writable(__ROOT.DS."cache")) {
			$fp = fopen($cacheFile, "w");
			if($fp) {
				$buffer = ob_get_contents();
				fwrite($fp, $buffer);
				fclose($fp);
			}
			ob_end_flush(); // Clear passage of buffer to output // Can also call $this->flushOB()
			return true;
		} else return false;
	}

	public function cachedFileExists($zero, $one, $two=0) {
		$cachedFile = __ROOT.DS."cache".DS.$zero."_".$one."_".$two.".html";
		if(file_exists($cachedFile)) {
			//$this->readFile($cacheFile);
			$this->cachedFile = $cachedFile;
			return true;
		} else return false;
	}
	
	public function readCachedFile($cachedFile=null) {
		if($cachedFile == null) {
			$cachedFile = $this->cachedFile;
			if($this->cachedFile != null) {
				ob_end_clean(); // Clean the output buffer
				$buffer = file_get_contents($cachedFile);
				printf( "%s", $buffer );
				return true;
			} else return false;
		} else {
			return false;
		}
	}
	
	public function setCacheParams() {
		// TODO CODE
	}
	
	public function startOB() {
		ob_start();
	}
	
	public function flushOB() {
		ob_end_flush();
	}
	
	public function cleanOB() {
		ob_end_clean();
	}
};

