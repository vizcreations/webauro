<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Template Core Class for presentation logic in the application
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class definition for the Template */

Class Template {

	/**
	 * 
	 * @var object
	 */
	private $registry;

	/**
	 * 
	 * @var array
	 */
	public $vars = array();
	protected $webTemplate;

	/**
	 * 
	 * @param $index
	 * @param $value
	 * @return void
	 */

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __set($index, $value) {
		$this->vars[$index] = $value;
	}

	public function show( $view ) {
		foreach($this->vars as $key => $value) {
			$$key = $value;
		}
		
		if($controller == 'index')
			$view = __VIEWS.DS.$view.'.view.php';
		else
			$view = __VIEWS.DS.$controller.DS.$view.'.view.php';
		
		/*if(file_exists($path)) {
			require_once($path);
		} else {
			die( '<html><center><p class="red">Requested page does not exist!</p></center></html>' );
		}*/
		
		$this->webTemplate = $webTemplate;
		if(!isset($indexfile) || (isset($indexfile) && !file_exists($indexfile))) {
			die( '<html><body><b>Application failed:</b> 404 error (Requested page does not exist.) Method: show() in application/template.class.php</body></html>' );
			$msg = 'Application failed: 404 error (Requested page does not exist.) Method: show() in application/template.class.php';
			log_app( $msg );
		}

		require_once( $indexfile );
	}

	/**
	* @abstract AJAX view with no template
	*/
	public function show_( $view ) {
		$controller = $this->registry->router->controller;
		if($controller == 'index')
			$path = __VIEWS.DS.$view.'.view.php';
		else
			$path = __VIEWS.DS.$controller.DS.$view.'.view.php';
			
		foreach($this->vars as $key => $val) {
			$$key = $val;
		}
		$this->webTemplate = $webTemplate;
		
		if(!file_exists($path)) {
			$path = __VIEWS.DS.'home.view.php';
		}

		require_once($path);
	}
	
	public function showseo($view=null) {
		if($view != null) {
			foreach($this->vars as $key => $val) {
				$$key = $val;
			}
			$path = __VIEWS.DS.'seo.view.php';
			if(file_exists($path))
				require_once($path);
		}
	}
};

