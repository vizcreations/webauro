<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Controller Base Class that is the parent of all controllers
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class definition for the Base Controller */

class Controller {
	
	/**
	 * 
	 * @var object
	 */
	protected $registry;
	protected $httpvars; // Wraps everything
	protected $postvars;
	protected $getvars;
	protected $sessionvars;
    protected $server;
	protected $cookies;
	protected $skip;
	protected $msgrowsperpage = 40;
	protected $picsperpage = 40;
	protected $vidsperpage = 25;
	protected $ttcount = 12;
	protected $curwday;
	protected $curmonth;
	protected $curyear;
	protected $curhours;
	protected $curminutes;
	protected $curtime;
    protected $date;
    public $webTemplate;

	/**
	 * 
	 * @param $registry
	 * @return void
	 */
	public function __construct($registry) {
		$this->registry = $registry;
		
		/**
		 * @abstract HTTP and SESSION data instantiation
		 * @var unknown_type
		 */
/*
		$this->httpvars = $this->registry->session->httpvars;
		$this->postvars = $this->registry->session->postvars;
		$this->getvars = $this->registry->session->getvars;
		$this->sessionvars = $this->registry->session->sessionarr;
        $this->server = $this->registry->session->server;
		$this->cookies = $this->registry->session->cookie;
*/
		$this->httpvars = $_REQUEST;
		$this->postvars = $_POST;
		$this->getvars = $_GET;
		$this->sessionvars = $_SESSION;
		$this->server = $_SERVER;
		$this->cookies = $_COOKIE;
		/**
		 * Useful date data ##
		 * @var mixed ##
		 */
		$date = getdate();
		if($date['hours'] < 10) $date['hours'] = '0'.$date['hours'];
		if($date['minutes'] < 10) $date['minutes'] = '0'.$date['minutes'];
		$this->curhours = $date['hours'];
		$this->curminutes = $date['minutes'];
		$this->curwday = $date['mday'];
		$months = array("0" => "0", "January" => "1", "February" => "2", "March" => "3", "April" => "4", "May" => "5",
						"June" => "6", "July" => "7", "August" => "8", "September" => "9", "October" => "10",
						"November" => "11", "December" => "12");
		$this->curmonth = $months[$date['month']];
        if($this->curwday < 10) $this->curwday = '0'.$this->curwday;
        if($this->curmonth < 10) $this->curmonth = '0'.$this->curmonth;
		$this->curyear = $date['year'];
		$this->curtime = $date['hours'].$date['minutes'];
		$this->visitorip = isset($this->server->REMOTE_ADDR)?$this->server->REMOTE_ADDR:$this->registry->hostip;

		$this->date = $this->curyear.'-'.$this->curmonth.'-'.$this->curwday;
		$this->loadWebTemplate();
	}

	public function setTheCookie() {
		//setcookie("socntwkcookie", "true", time()+5, "/");
		$this->registry->session->ck = $_COOKIE;
	}

	public function unsetTheCookie() {
		$this->registry->session->ck = null;
	}

	public function loadWebTemplate() {
		$route = isset($this->registry->router->__http_vars->route) ? $this->registry->router->__http_vars->route : '';
		$umodel = new UsersModel();
		if(empty($route)) {
			// $this->webTemplate = 'vizcreations';
			$template = $this->getMenuitemWebTemplate('Home');
			$this->webTemplate = $template;
		} else {
			$model = new MenusModel();
			try {
				$viewid = $model->getViewId( $this->registry->router->controller, $this->registry->router->action );
				//$this->webTemplate = $this->getWebTemplate($this->controller, $this->action);
				$template = $model->getViewWebTemplate($viewid, $this->registry->router->id);
				if(!$template) $template = $this->getSelWebTemplate();
				if($this->registry->router->controller == 'index' && $this->registry->router->action == 'index') $template = $this->getMenuitemWebTemplate('Home');
			} catch(Exception $e) {
				$template = $this->getSelWebTemplate();
			}
			$this->webTemplate = $template;
		}

		if(!$this->webTemplate || empty($this->webTemplate)) $this->webTemplate = $this->getSelWebTemplate();

		$indexfile = __WEBTEMPLATES.DS.$this->webTemplate.DS.'index.php';
		if(file_exists($indexfile))	{
			try {
				$umodel->saveVisitor($this->registry->router->visitorip);
			} catch(Exception $e) {
				$this->registry->template->message = $e->getMessage();
			}
			$this->registry->template->indexfile = $indexfile;
			$this->registry->template->controller = $this->registry->router->controller;
			$this->registry->template->webTemplate = $this->webTemplate;
		}
		else {
			$msg = "<b>Application failed:</b> Failed to load web template! Method: loadWebTemplate() in ".__FILE__." line ".__LINE__;
			log_app( $msg );
			die ( $msg );
		}
	}

	public function getMenuitemWebTemplate($menuitemname) {
		$mmodel = new MenusModel();
		try {
			$templatename = $mmodel->getMenuitemWebTemplate($menuitemname);
		} catch(Exception $e) {
			$templatename = $this->getWebTemplate();
			$this->registry->template->message = $e->getMessage();
		}
		//if($templatename == 'vizcreations') $templatename = $this->getSelWebTemplate();
		return $templatename;
	}

	public function getWebTemplate($controller='index', $view='home') {
		$model = new IndexModel();
		$template = $model->getWebTemplate($controller, $view);
		if(!$template) {
			$xmlDoc = new DOMDocument();
			$xmlFile = __ROOT.DS.'webauro'.DS.'webTemplate.xml';
			if(!file_exists($xmlFile)) $template = 1;
			else {
				$xmlDoc->load($xmlFile);
				$template = $xmlDoc->getElementsByTagName('selected')->item(0)->nodeValue;
			}
		}
		$templatename = $model->getWebTemplateName($template);
		return $templatename;
	}

	public function getSelWebTemplate() {
		$model = new IndexModel();
		$xmlDoc = new DOMDocument();
		$xmlFile = __ROOT.DS.'webauro'.DS.'webTemplate.xml';
		if(!file_exists($xmlFile)) $template = 1;
		else {
			$xmlDoc->load($xmlFile);
			$template = $xmlDoc->getElementsByTagName('selected')->item(0)->nodeValue;
		}

		$templatename = $model->getWebTemplateName($template);
		if(!$templatename) {
			$template = $this->registry->webTemplate;
			$templatename = $model->getWebTemplateName($template);
		}

		return $templatename;
	}
};
