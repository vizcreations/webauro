<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Frags Template Base Class to apply presentation logic to fragments
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class FragTemplate {
	public $vars = array();
	public $registry;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function __set($index, $value) {
		$this->vars[$index] = $value;
	}
	
	public function show( $fragment ) {
		$fragfile = __FRAGMENTS.DS.$fragment.'.fragment.php';
		foreach($this->vars as $key => $val) {
			$$key = $val;
		}
		if(file_exists($fragfile)) {
			include( $fragfile );
		}
	}
};

