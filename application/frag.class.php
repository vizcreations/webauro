<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Frags Base Class for application logic in fragments
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class Frag {
	protected $registry;
	protected $fragPosition;
	protected $fragtype;
	protected $fragclass;
	
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function load($fragname, $pos='top') {
		$this->getFragType($fragname);
		$classfile = __ROOT.DS.'frags'.DS.strtolower($this->fragtype).'.frag.php';
		if(file_exists($classfile)) {
			require_once($classfile);
			$classname = $this->fragclass;
			$frag = new $classname($this->registry);
			$frag->loadFrag( $fragname );
		}
	}
	
	public function getFrag($fragname, $pos) {
		$this->fragclass = $fragname.'Frag';
		$this->fragname = $fragname;
		$this->pos = $pos;
	}
	
	public function getFragType($fragname) {
		$fmodel = new FragsModel();
		try {
			$fragtype = $fmodel->getFragType($fragname);
		} catch(Exception $e) {
			$fragtype = "Error";
		}
		$this->fragtype = $fragtype;
		$this->fragclass = $fragtype.'Frag';
	}
};

