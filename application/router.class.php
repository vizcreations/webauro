<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Router Core Class to load controller and run it's method to generate template/view
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class definition for the Router */

class Router {
	
	/**
	 * 
	 * @var null
	 */
	private $registry = null;
	
	private static $path = null;
	private static $seopath = null;
	private static $metapath = null;
	public $seo;
	public $controller = 'index';
	public $action;
	public $id;
	public $message;
	public $ck = null;
	public $webTemplate;
	private $fragclass;
	public $fragname;
	public $pos;
	public $visitorip;
	public $__http_vars;
	/**
	 * 
	 * @param $registry
	 * @return void
	 */
	public function __construct($registry) {
		$this->registry = $registry;
		$this->visitorip = $this->registry->session->server->REMOTE_ADDR;
		
		$this->__http_vars = $this->registry->session->httpvars;
    }
	
	public function setControllerPath($thepath) {
		self::$path = $thepath;
	}

	public function loadPage() {
		$this->getController();
		$controllerclassfile = self::$path.DS.$this->controller.'.controller.php';
		if(!$controllerclassfile || !file_exists($controllerclassfile)) {
			//die( '404 file not found <br />'.$controllerclassfile );
			die('<div style="height: 600px;"><b>Application failed:</b> 404 error (Requested page does not exist.) Method: go() in '.__FILE__.' line '.__LINE__.'</div>');
			$msg = "Requested page does not exist. Method: load__() in application/router.class.php";
			log_app( $msg );
			log_db( $msg );
			return false;
		} else {
			require_once($controllerclassfile);

			$controllerclass = strtolower($this->controller).'Controller';
			$controller = new $controllerclass($this->registry);
			if(method_exists($controller, $this->action)) {
				$action = $this->action;
			} else {
				$action = 'index';
			}
			
			if(empty($this->id)) {
				$id = '';
			} else {
				$id = $this->id;
			}
			
			$controller->$action($id);
		}
	}
	
	/**
	 * 
	 * @return void
	 */
	public function getController() {
		$route = isset($_REQUEST['route']) ? $_REQUEST['route'] : '';
		if(empty($route)) {
			$mmodel = new MenusModel();
			try {
				$route = $mmodel->getMenuitemLink('home');
			} catch(Exception $e) {
				$route = 'index/home/';
			}
		}
		$parts = explode('/', $route);
		if(!empty($parts[0])) {
			$this->controller = $parts[0];
		} else {
			$this->controller = 'index';
		}
		
		if(!empty($parts[1])) {
			$this->action = $parts[1];
		} else {
			$this->action = 'index';
		}
		
		if(!empty($parts[2])) {
			$this->id = $parts[2];
		}
	}
	
	public function setSEOPath($path) {
		self::$seopath = $path;
	}
	
	public function setMetaPath($path) {
		self::$metapath = $path;
	}
	
	public function loadSEO() {
		$this->getController();
		$seofile = self::$seopath.DS.$this->controller.'.seo.php';
		if(file_exists($seofile)) {
			require_once( $seofile );
			$seoclass = ucfirst($this->controller).'SEO';
		} else {
			require_once( self::$seopath.DS.'index.seo.php' );
			$seoclass = 'IndexSEO';
		}
		$seo = new $seoclass($this->registry);
		$this->seo = $seo;
		if(method_exists($seo, $this->action))
			$method = $this->action;
		else
			$method = 'index';
		if(!empty($this->id))
			$seo->$method($this->id);
		else
			$seo->$method();
	}
		
	public function redirectJS($uri, $msg='') {
		$this->registry->template->message = $msg;
		?>
		<script type="text/javascript">
		<!--
		window.location.href='<?php echo $uri.$msg; ?>';
		//-->
		</script>
		<?php
	}
	
	public function redirectJSSlow($uri, $msg='') {
		$this->registry->template->message = $msg;
		?>
		<script type="text/javascript">
		<!--
		function direct() {
				window.location.href='<?php echo $uri.$msg; ?>';
		}
		setTimeout(direct(), 5000);
		//-->
		</script>
		<?php
	}
	
	public function header($url, $params="") {
		header("Location: $url".$params);
		exit();
	}/*
	
	public function loadFrag($frag) {
		$fragfile = __FRAGS.DS.$frag.'.frag.php';
		if(file_exists($fragfile))
			require_once( __FRAGS.DS.$frag.'.frag.php' );
		else
			echo "404 Frag template $fragfile not found!";
	}*/

	public function __redirect($url='index.php', $params='') {
		$ref = $url;
		if(!empty($params)) {
			$ref .= "?".$params;
		}
		header( "Location: $ref" );
		exit();
	}
};

