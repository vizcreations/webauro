<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Frags Model Base Class to apply business logic for fragments
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
Abstract class FragModel extends DB {
	public function save($vars, $entity, $option) {
		// TODO CODE
	}
	
	public function getArgument($fragname) {
		$query = " SELECT argument_value FROM #__frags, #__frags_arguments ".
					" WHERE #__frags.frag_name='$fragname' ".
					" AND #__frags_arguments.frag_id=#__frags.frag_id ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}

	Abstract function getFrag($fragid);
};

