#!/usr/local/bin/php
<?php
/**
* @abstract Self executable PHP script - Alter PHP binary path accordingly
* @var SQL
* @param mixed
* @author vizcreations
* @date Sun Oct 20
*/

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
define( "__MAGIXQUOTE", get_magic_quotes_gpc() ); // I just defined the server's behaviour for magic quote
define( "__ROOT", realpath(dirname( __FILE__ )) );
define( "__DS", DIRECTORY_SEPARATOR );
define( "__EXEC", true ); // Should we execute?
define( "__PHP", "mod" );
define( "__bin", "/usr/local/bin/php");

$exec = true;
$all = true;
$global = false;
$http = '_GET'; // The default http method
$httpvars = $_GET;
$argc = 0;
$argv = isset($_SERVER['argv'])?$_SERVER['argv']:array(0=>$_SERVER['PHP_SELF']); // The command line arguments
$schema = array(0);
$argc = count($argv);
$schema['curq'] = 'all'; // default
switch($argc) {
	case 2:
		$schema['curq'] = 'single';
		break;
	case 3:
		$schema['curq'] = 'range';
		break;
		
}

$schema['driver'] = 'mysqli';
$driver = $schema['driver'];
$logsdir = __ROOT.__DS."logs"; // Change to your logs directory and make it world-writable
$tmpdir = __ROOT.__DS."tmp"; // Change to your temp directory and make it world-writable
$config = "configuration.php";

// Your driver/extension
$mysql = array();
$mysql['core']['create'] = false;
$mysql['core']['schema'] = 'travelninja';
$mysql['core']['options'] = array( "", "", "", "", "", "" );
$err = '';

$conn = null;

if(!file_exists(__ROOT.__DS.$config)) {
	log_err( $config.": not found!" );
	exit(2);
}
require_once( __ROOT.__DS.$config ); // Link to your config file

// Function that logs the errors encountered throughout the execution of this script
function log_err($err='') {
	global $logsdir;
	if(!empty($err) 
		&& (is_dir($logsdir)&&is_writable($logsdir))
		) {
		$fp = fopen($logsdir.__DS."merror_log", "a+");
		if($fp) {
			fwrite($fp, $err.PHP_EOL);
			fclose($fp);
		}
	} else {
		printf( "Directory %s isn't writable!!!\n", $logsdir );
	}
}

/**
* Function that logs the current migration script execution to file
*/
function log_file($msg='') {
	global $logsdir;
	$date = getdate();
	$yr = $date['year'];
	$mon = $date['mon'];
	$day = $date['mday'];
	$wday = $date['wday'];
	$hour = $date['hours'];
	$min = $date['minutes'];
	$str = "(Script run on $wday $mon $day, $yr at $hour:$min)".PHP_EOL;
	if(true 
		&& (is_dir($logsdir)&&is_writable($logsdir))
		) {
		$fp = fopen($logsdir.__DS."migrate_log", "a+");
		if($fp) {
			if(!empty($msg)) $str .= " : ".$msg;
			fwrite($fp, $str.PHP_EOL);
			fclose($fp);
		}
	} //else log_err( "Couldn't write migrate error log!" );
	else printf( "Directory %s isn't writable!!!\n", $logsdir );
}

function connect_db($db) {
	$link = mysqli_connect($db['host'], $db['user'], $db['pass']);
	if(!$link) {
		log_err(mysqli_error($link));
		return false;
	}
	if(!mysqli_select_db($link, $db['schema'])) {
		log_err("Couldn't select database!");
		return false;
	}
	return $link;
}

function valid($q=null, $link=null) { // Blueprint that checks the query structure
	if( !__MAGIXQUOTE ) {
		$q = mysqli_real_escape_string($link, $q);
	}
	return true;
}

function execute($q=null, $link=null, $num=0) {
	printf( "%s\t", "Executing query($num): ..." );
	$res = null;
	if($q != null && $link != null) {
		$res = mysqli_query($link, $q);
	}
	sleep(1);
	if(!$res) {
		printf( "%s", "Error!!!" );
		printf( " << Check logs >>".PHP_EOL );
		log_err( mysqli_error($link) );
	} // Just log and do not return or exit
	else printf( "%s\n", "Success...");
	sleep(3);
}

// List of queries that you want to execute
/** Controllers */
$sql1 = <<<MULT
CREATE TABLE `wb_controllers` (
`controller_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`controller_name` varchar(50) NOT NULL,
`timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`controller_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;
MULT;
/** Controllers data */
$sql2 = <<<MULT
INSERT INTO `wb_controllers` VALUES (1001, 'index', NOW()), (1002, 'users', NOW());
MULT;

/** Templates */
$sql3 = <<<MULT
CREATE TABLE `wb_templates` (
`template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`template_name` varchar(50) NOT NULL,
`template_author` varchar(50) NOT NULL,
`template_createddate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
MULT;

/** Templates data */
$sql4 = <<<MULT
INSERT INTO `wb_templates` VALUES (1,'webauro_main','vizcreations',CURDATE(),NOW()),(2,'webauro_inner','vizcreations',CURDATE(),NOW());
MULT;

/** Users table */
$sql5 = <<<MULT
CREATE TABLE `wb_users` (
`user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`user_name` varchar(50) NOT NULL,
`user_password` varchar(255) NOT NULL,
`user_email` varchar(100) NOT NULL,
`user_status` tinyint(1) NOT NULL,
`user_online` tinyint(1) NOT NULL,
`user_location` varchar(255) NOT NULL,
`user_joined` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`user_image` varchar(200) NOT NULL,
`timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=100001 DEFAULT CHARSET=utf8;
MULT;

/** Users data */
$sql6 = <<<MULT
INSERT INTO `wb_users` VALUES (100001,'superadmin',MD5('superadmin'),'superadmin@webauro.com',1,1,'Hyderabad, India',NOW(),'',NOW()),(100002,'admin',MD5('admin'),'admin@webauro.com',1,0,'Hyderabad, India.',NOW(),'',NOW());
MULT;

/** Users roles table & data */
$sql7 = <<<MULT
CREATE TABLE `wb_users_roles` (
`user_role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`user_role_name` varchar(50) NOT NULL,
`user_role_desc` varchar(255) NOT NULL,
`timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`user_role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;
MULT;

$sql8 = <<<MULT
INSERT INTO `wb_users_roles` VALUES (10001,'superadmin','Super Administrator',NOW()),(10002,'admin','Administrator',NOW()),(10003,'user','Standard User',NOW());
MULT;

$sql9 = <<<MULT
CREATE TABLE `wb_ar_categories` (
`ar_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_category_name` varchar(100) NOT NULL,
  `ar_category_description` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ar_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql10 = <<<MULT
CREATE TABLE `wb_ar_comments` (
  `ar_comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_comment_name` varchar(50) NOT NULL,
  `ar_comment_email` varchar(100) NOT NULL,
  `ar_comment_uri` varchar(100) NOT NULL,
  `ar_comment_content` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ar_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql11 = <<<MULT
CREATE TABLE `wb_ar_comments_status` (
`ar_comments_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ar_comments_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ar_comments_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql12 = <<<MULT
CREATE TABLE `wb_articles` (
`article_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_title` varchar(100) NOT NULL,
  `article_author` varchar(100) NOT NULL,
  `article_content` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql13 = <<<MULT
CREATE TABLE `wb_articles_categories_rel` (
`article_category_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ar_category_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_category_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql14 = <<<MULT
CREATE TABLE `wb_articles_comments_rel` (
`article_comment_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ar_comment_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_comment_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql15 = <<<MULT
CREATE TABLE `wb_articles_ratings` (
`article_rating_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `article_rating` int(2) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_rating_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql16 = <<<MULT
CREATE TABLE `wb_articles_seo` (
`article_seo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `seo_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_seo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql17 = <<<MULT
CREATE TABLE `wb_artices_status` (
  `article_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `article_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql18 = <<<MULT
CREATE TABLE wb_contact (
`contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact_email` varchar(100) DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql19 = <<<MULT
CREATE TABLE `wb_controllers_templates` (
`controller_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`controller_template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql20 = <<<MULT
CREATE TABLE `wb_controllers_views` (
  `controller_view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller_id` int(11) NOT NULL,
  `view_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`controller_view_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql21 = <<<MULT
CREATE TABLE `wb_forums` (
`forum_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_title` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql22 = <<<MULT
CREATE TABLE `wb_forums_categories` (
`forum_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_category_name` varchar(50) NOT NULL,
  `forum_category_desc` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql23 = <<<MULT
CREATE TABLE `wb_forums_categories_rel` (
`forum_category_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `forum_category_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_category_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql24 = <<<MULT
CREATE TABLE `wb_forums_posts` (
`forum_post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `forum_post_content` text NOT NULL,
  `forum_post_posted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql25 = <<<MULT
CREATE TABLE `wb_forums_users_rel` (
`forum_user_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_user_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql26 = <<<MULT
CREATE TABLE `wb_frags` (
`frag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_name` varchar(50) NOT NULL,
  `frag_title` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql27 = <<<MULT
CREATE TABLE `wb_frags_arguments` (
 `frag_argument_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `argument_value` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_argument_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql28 = <<<MULT
CREATE TABLE `wb_frags_content` (
  `frag_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `frag_content` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql29 = <<<MULT
CREATE TABLE `wb_frags_pos` (
  `frag_pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_pos_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql30 = <<<MULT
CREATE TABLE `wb_frags_status` (
  `frag_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `frag_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql31 = <<<MULT
CREATE TABLE `wb_frags_types` (
  `frag_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_type_name` varchar(255) NOT NULL,
  `frag_type_desc` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql32 = <<<MULT
CREATE TABLE `wb_frags_types_rel` (
  `frag_type_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `frag_type_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_type_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql33 = <<<MULT
CREATE TABLE `wb_menuitems` (
  `menuitem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_name` varchar(100) NOT NULL,
  `menuitem_link` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql34 = <<<MULT
CREATE TABLE `wb_menuitems_menus` (
  `menuitem_menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql35 = <<<MULT
CREATE TABLE `wb_menuitems_parents` (
  `menuitem_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `parent_menuitem_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql36 = <<<MULT
CREATE TABLE `wb_menuitems_templates` (
  `menuitem_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_template_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql37 = <<<MULT
CREATE TABLE `wb_menuitems_views` (
  `menuitem_view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `view_id` int(11) NOT NULL,
  `argument_value` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_view_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql38 = <<<MULT
CREATE TABLE `wb_menus` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql39 = <<<MULT
CREATE TABLE `wb_menus_pos` (
  `menu_pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_pos_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql40 = <<<MULT
CREATE TABLE `wb_menus_status` (
  `menu_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `menu_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql41 = <<<MULT
CREATE TABLE `wb_pos` (
  `pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pos_name` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql42 = <<<MULT
CREATE TABLE `wb_seo` (
  `seo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seo_title` varchar(50) NOT NULL,
  `seo_keywords` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql43 = <<<MULT
CREATE TABLE `wb_users_roles_rel` (
  `user_role_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_role_rel_id`,user_id,user_role_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

// data to users roles rel
$sql44 = <<<MULT
INSERT INTO wb_users_roles_rel VALUES('1', '100001', '10001', NOW()), ('2', '100002', '10002', NOW());
MULT;

$sql45 = <<<MULT
CREATE TABLE `wb_views` (
  `view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `view_name` varchar(50) NOT NULL,
  `argument` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`view_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql46 = <<<MULT
CREATE TABLE wb_views_arguments (
  `view_argument_id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `argument_table` varchar(50) DEFAULT NULL,
  `argument_id` varchar(50) NOT NULL,
  `argument_name` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`view_argument_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

$sql47 = <<<MULT
CREATE TABLE `wb_visitors` (
  `visitor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_ip` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

// positions data
$sql48 = <<<MULT
INSERT INTO wb_pos VALUES('1', 'top', NOW()), ('2', 'side', NOW()), ('3', 'bottom', NOW());
MULT;

// menuitems status
$sql49 = <<<MULT
CREATE TABLE wb_menuitems_status(
menuitem_id INT UNSIGNED NOT NULL,
menuitem_status BOOL NOT NULL,
timestamp DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY(menuitem_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
MULT;

// frag types data
$sql50 = <<<MULT
INSERT INTO wb_frags_types VALUES('1','latestarticles','Frag displaying latest articles from a particular category',NOW()),
('2','content','Frag displaying text and HTML content',NOW()),
('3','menu','Frag displaying a menu',NOW()),
('4','login','User Login',NOW()),
('5','search','Website Search',NOW());
MULT;

/**
* Change/Append your keys which have no effect to executing
*
*/
$sqlarray = array( 
			"controllers" => $sql1, // Rename key to the purpose of query
			"controllers-data" => $sql2,
			"templates" => $sql3,
			"templates-data" => $sql4,
			"users" => $sql5,
			"users-data" => $sql6,
			"users-roles" => $sql7,
			"users-roles-data" => $sql8,
			"ar-categories" => $sql9,
			"ar-comments" => $sql10,
			"ar-comments_status" => $sql11,
			"articles" => $sql12,
			"articles-categories-rel" => $sql13,
			"articles-comments-rel" => $sql14,
			"articles-ratings" => $sql15,
			"articles-seo" => $sql16,
			"articles-status" => $sql17,
			"contact" => $sql18,
			"controllers-templates" => $sql19,
			"controllers-views" => $sql20,
			"forums" => $sql21,
			"forums-categories" => $sql22,
			"forums-categories-rel" => $sql23,
			"forums-posts" => $sql24,
			"forums-users-rel" => $sql25,
			"frags" => $sql26,
			"frags-arguments" => $sql27,
			"frags-content" => $sql28,
			"frags-pos" => $sql29,
			"frags-status" => $sql30,
			"frags-type" => $sql31,
			"frags-type-rel" => $sql32,
			"menuitems" => $sql33,
			"menuitems-menus" => $sql34,
			"menuitems-parents" => $sql35,
			"menuitems-templates" => $sql36,
			"menuitems-views" => $sql37,
			"menus" => $sql38,
			"menus-pos" => $sql39,
			"menus-status" => $sql40,
			"pos" => $sql41,
			"seo" => $sql42,
			"users-roles-rel" => $sql43,
			"roles-rel-data" => $sql44,
			"views" => $sql45,
			"views-arguments" => $sql46,
			"visitors" => $sql47,
			"pos-data" => $sql48,
			"menuitems-status" => $sql49,
			"frags-types-data" => $sql50
		);


if(__EXEC == true) {
	date_default_timezone_set( 'UTC' ); // TimeZone 
	log_file(); // Logging when the script executed..
	$link = connect_db($dbserver);
	$key="null";
	if($link) {
		$num = 1;
		switch($schema['curq']) {
			case 'tip':
				log_file( "Tip query selected" );
				break; // Add you cases to run each query
			case 'all':
				if(isset($sqlarray) && is_array($sqlarray)) {
					foreach($sqlarray as $q) {
						if(!empty($q)) {
							if(valid($q, $link)) {
								execute($q, $link, $num);
								++$num;
							}
							else log_err( $q.": is invalid" );
						} else log_err( $q.": is empty" );
					}
				}
				break;
			default: // Invoke command line arguments
				$argc = count($argv); $num=1;
				if($argc>1) { // Some key was defined..
					for($i=1;$i<$argc;$i++) {
						$key=$argv[$i];
						if(array_key_exists($key,$sqlarray)) {
							$q = $sqlarray[$key];
							if(!empty($q))
								if(valid($q,$link)) {
									execute($q,$link,$num); ++$num;
								} else log_err( $q.": is invalid");
							else log_err( $q.": is empty" );
						} else log_err( $q.": is invalid" );
					}
				} else if($argc>2) { // We have a range declared..
					/** Simply execute blindly using keys */

					for($i=$argv[1];$i<$argv[2]+1;$i++) {
						$key=$i;
						if(array_key_exists($sqlarray,$key)) {
							$q = $sqlarray[$key];
							if(!empty($q))
								if(valid($q,$link)) {
									execute($q,$link,$num); ++$num;
								} else log_err($q.": is invalid");
							else log_err($q.": is empty");
						} else log_err( $q.": is invalid" );
					}
				}
				break;
		}
		mysqli_close($link);
	} else log_err( "MYSQL identifier error" );
} else log_err( "EXEC constant not defined" );
