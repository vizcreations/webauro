<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Articles SEO Class to dynamically change meta information of the HTML document for each article, category
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ArticlesSEO extends SEO {
	public function index() {
		$sitename = $this->registry->sitename;
		$this->registry->template->title = "Articles";
		$this->registry->template->keywords = $sitename." articles";
		$this->registry->template->description = $sitename." articles";
		$this->registry->template->showseo( 'seo' );
	}
	
	public function full_article($id=0) {
		$amodel = new ArticlesModel();
		try {
			$seo = $amodel->getArticleSEO($id);
		} catch(Exception $e) {
			$seo = array('seo_title' => 'Error', 'seo_keywords' => 'error', 'seo_description' => 'error');
			$this->registry->template->message = $e->getMessage();
		}
		if($amodel->getCount() < 1) {
			$seo = array('seo_title' => $this->registry->seo->title, 
							'seo_keywords' => $this->registry->seo->keywords, 
							'seo_description' => $this->registry->seo->description);
		}
		$this->registry->template->title = $seo['seo_title'];
		$this->registry->template->keywords = $seo['seo_keywords'];
		$this->registry->template->description = $seo['seo_description'];
		$this->registry->template->showseo( 'seo' );
	}		
	
	public function latest_category_articles($id=0) {
		$amodel = new ArticlesModel();
		$sitename = $this->registry->sitename;
		try {
			$arcat = $amodel->getArticleCategory($id);
		} catch(Exception $e) {
			$arcat = array("ar_category_name" => $sitename." article category");
		}
		$this->registry->template->title = $sitename.' | '.$arcat['ar_category_name'];
		$this->registry->template->keywords = $sitename." blog, ".$sitename." article, ".$sitename." tech";
		$this->registry->template->description = $arcat['ar_category_name'].' articles';
		$this->registry->template->showseo( 'seo' );
	}
	
	public function page_latest_category_articles($id=0) {		
		$amodel = new ArticlesModel();
		$page = $this->httpvars['page'];
		$sitename = $this->registry->sitename;	
		try {			
			$arcat = $amodel->getArticleCategory($id);		
		} catch(Exception $e) {			
			$arcat = array("ar_category_name" => $sitename." article category");		
		}		
		$this->registry->template->title = $sitename.' | '.$arcat['ar_category_name'].' | Page - '.$page;
		$this->registry->template->keywords = $sitename. "blog, ".$sitename." article, ".$sitename." tech";
		$this->registry->template->description = $arcat['ar_category_name'].' articles';
		$this->registry->template->showseo( 'seo' );
	}
	
	public function latest_articles() {
		$sitename = $this->registry->sitename;
		$this->registry->template->title = $sitename;
		$this->registry->template->keywords = "";
		$this->registry->template->description = "";
		$this->registry->template->showseo( 'seo' );
	}
};

