<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users SEO Class for dynamic meta information for different pages
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class UsersSEO extends SEO {
	public function index() {
		$sitename = $this->registry->sitename;
		if($this->registry->isLogged) {
			$title = $sitename.' - '.$this->registry->username;
			$keywords = "";
			$description = "";
		} else {
			$title = $sitename;
			$keywords = "";
			$description = "";
		}
		$this->registry->template->title = $title;
		$this->registry->template->keywords = $keywords;
		$this->registry->template->description = $description;
		$this->registry->template->showseo( 'seo' );
	}
	
	public function profile($userid=0) {
		$model = new UsersModel();
		$sitename = $this->registry->sitename;
		try {
			$user = $model->getIdUser($userid);
		} catch(Exception $e) {
			$user = 'none';
		}
		$this->registry->template->title = $sitename.' - '.$user['user_name'].'\'s profile';
		$this->registry->template->keywords = $sitename.' user profile, india user profile, indian user profile';
		$this->registry->template->description = 'Profile of '.$user['user_name'].' in '.$sitename;
		$this->registry->template->showseo( 'seo' );
	}
	
	public function user($userid=0) {
		$this->tweets($userid);
	}
	
	public function tweets($userid=0) {
		$model = new UsersModel();
		$sitename = $this->registry->sitename;
		try {
			$user = $model->getIdUser($userid);
		} catch(Exception $e) {
			$user = 0;
		}
		$this->registry->template->title = $sitename.' - '.$user['username'].' '.$user['lname'];
		$this->registry->template->keywords = '';
		$this->registry->template->description = 'Personal messages page of '.$user['username'].' '.$user['lname'].' in '.$sitename;
		$this->registry->template->showseo( 'seo' );
	}
	
	public function following($userid=0) {
		$model = new UsersModel();
		$sitename = $this->registry->sitename;
		try {
			$user = $model->getIdUser($userid);
		} catch(Exception $e) {
			$user = 0;
		}
		$this->registry->template->title = $sitename.' - '.$user['username'].' '.$user['lname'].' following';
		$this->registry->template->keywords = '';
		$this->registry->template->description = '';
		$this->registry->template->showseo( 'seo' );
	}
	
	public function followers($userid=0) {
		$model = new UsersModel();
		$sitename = $this->registry->sitename;
		try {
			$user = $model->getIdUser($userid);
		} catch(Exception $e) {
			$user = 0;
		}
		$this->registry->template->title = $sitename.' '.$user['username'].' '.$user['lname'].'\'s followers';
		$this->registry->template->keywords = '';
		$this->registry->template->description = '';
		$this->registry->template->showseo( 'seo' );
	}
	
	public function myspace() {
		$model = new UsersModel();
		$this->registry->template->title = "";
		$this->registry->template->keywords = '';
		$this->registry->template->description = '';
		$this->registry->template->showseo( 'seo' );
	}
	
	public function myspaced() {
		$this->registry->template->title = "";
		$this->registry->template->keywords = '';
		$this->registry->template->description = '';
		$this->registry->template->showseo( 'seo' );
	}
	
	public function show_pm($userid=0) {
		$this->registry->template->title = "";
		$this->registry->template->keywords = '';
		$this->registry->template->description = '';
		$this->registry->template->showseo( 'seo' );
	}
};

