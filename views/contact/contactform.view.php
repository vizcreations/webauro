<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Contact - Contact Form View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

$site = $this->registry->sitename;
$conform_token = uniqid();
$_SESSION[$site]['conform_token'] = $conform_token;
 
?>

<h4><?php echo $title; ?></h4>

<table class="contactinfo" cellpadding="0" cellspacing="0">
<tr>
	<td>
		<b>Name: </b>
	</td>
	<td>
		<?php echo $name; ?>
	</td>
</tr>
<tr>
	<td>
		<b>Telephone: </b>
	</td>
	<td>
		<?php echo $telephone; ?>
	</td>
</tr>
<tr>
	<td>
		<b>Email: </b>
	</td>
	<td>
		<?php echo $email; ?>
	</td>
</tr>
<tr>
	<td>
		<b>Address: </b>
	</td>
	<td>
		<?php echo $address; ?>
	</td>
</tr>
</table>

<?php echo isset($message) ? '<div class="message">'.$message.'</div>' : ""; ?>
<form name="contactform" id="contactform" method="post" action="index.php" style="padding: 0;">
	<table class="commentform" cellpadding="2" cellspacing="4">
	<tr>
		<td>
			<label for="contactname">Name</label><br />
			<input type="text" name="contactname" id="contactname" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="contactemail">Email</label><br />
			<input type="text" name="contactemail" id="contactemail" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="contactsubject">Subject</label><br />
			<input type="text" name="contactsubject" id="contactsubject" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="contactmessage">Message</label><br />
			<textarea name="contactmessage" id="contactmessage" rows="3" cols="50" class="txtarea"></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<label for="imgv">Security code</label><br />
			<input type="text" name="imgv" id="imgv" value="" onclick="this.value='';" size="25" class="inputtext" /><br />
			<img src="index.php?route=graphics/render_imagev" border="0" alt="" class="imagev" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="contact/submit" />
			<input type="hidden" name="conform_token" value="<?php echo $conform_token; ?>" />
			<input type="submit" name="submitcontact" id="submitcontact" value="Submit" class="" style="width: 126px; height: 34px; font-family: Arial, Sans; font-size: 0.95em; color: #FFFFFF; border: 0; background: url('templates/<?php echo $this->webTemplate; ?>/images/sbutton.png');" />
		</td>
	</tr>
	</table>
</form>
<p></p>
