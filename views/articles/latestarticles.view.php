<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Articles - Latest Articles View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
?>

<h4><?php echo $title; ?></h4>
<?php echo isset($message) ? '<div class="message">'.$message.'</div>' : ""; ?>

<?php 
if(!$articles) {
?>
<p class="paratop parabottom"><span class="red">No articles created yet for this category!</span></p>
<?php 
} else {
?>
<table class="articles" cellpadding="0" cellspacing="0">
<?php
	for($i=0; $i<count($articles); $i++) {
		$article = $articles[$i];
?>
<tr>
	<td>
		<h5><a href="index.php?route=articles/full_article/<?php echo $article['article_id']; ?>"><?php echo $article['article_title']; ?></a></h5>
		<?php 
			echo substr(nl2br($article['article_content']), 0, 190).'..<a href="index.php?route=articles/full_article/'.$article['article_id'].'">more..</a>';
			$amodel = new ArticlesModel();
			try {
				$commentcount = $amodel->getArticleCommentsCount($article['article_id']);
			} catch(Exception $e) {
				$commentcount = 0;
				echo '<span class="red">'.$e->getMessage().'</span>';
			}
		?>
		<br />
		<small class="gray">By ~<b><?php echo $article['article_author']; ?></b> - <?php echo $commentcount.' comments'?></small>
	</td>
</tr>
<?php 
	}
?>
</table>
<?php
	$nav = '';
	$link = 'index.php?route=articles/page_latest_category_articles/'.$catid.'&page=';
	for($pagenum=1; $pagenum<=$maxpage; $pagenum++) {
		if($page == $pagenum) {
			if($maxpage == 1) $nav .= '';
			else
			$nav .= ' '.$pagenum.' ';
		} else $nav .= ' <a href="'.$link.$pagenum.'">'.$pagenum.'</a> ';
	}
	if($page > 1) {
		$pagenum = $page - 1;
		$prev = ' <a href="'.$link.$pagenum.'">[ Prev ]</a> ';
		$first = ' <a href="'.$link.'1">[ First ]</a> ';
	} else {
		$prev = '';
		$first = '';
	}
	if($page < $maxpage) {
		$pagenum = $page + 1;
		$next = ' <a href="'.$link.$pagenum.'">[ Next ]</a> ';
		$last = ' <a href="'.$link.$maxpage.'">[ Last ]</a> ';
	} else {
		$next = '';
		$last = '';
	}
	echo '<table class="" cellpadding="0" cellspacing="0"><tr><td><p class="paratop parabottom"><small>'.$first.$prev.'Page <b>'.$page.'</b> of '.$maxpage.$next.$last.'</small></p></td></tr></table>';
?>
<?php
}
?>
