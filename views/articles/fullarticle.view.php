<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Articles - Front End Full Article View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

$comform_token = uniqid();$_SESSION['comform_token'] = $comform_token;
 
?>

<h4><?php echo $title; ?></h4>
<small class="gray">By <b>~<?php echo $article['article_author']; ?></b>, updated on <?php echo date("F jS, Y", strtotime($article['timestamp'])); ?></small><br /><br /> 
<?php echo $articlecontent; ?>
<br /><br />
<small>More articles from <a href="index.php?route=articles/latest_category_articles/<?php echo $article['ar_category_id']; ?>"><?php echo $article['ar_category_name']; ?></a></small><br /><br /><br />
<?php
$model = new ArticlesModel();
$commentstatus = $model->getCStatus($article['article_id']);
//$commentstatus = false;
if($commentstatus) {
?>
<h5>Comments</h5>
<?php if(!$comments) { ?>
<p class="none" style="padding: 0 1px;"><span class="red"><small>No comments yet!</small></span></p>
<?php } else { ?>
<table class="comments" cellspacing="1" cellpadding="0" style="width: 95%;">
<?php 	
	for($i=0; $i<count($comments); $i++) {		
		$comment = $comments[$i];		
		if(($i%2) == 0) $bgcolor = "#F8F8FF";		
		else $bgcolor = "#F5F5F5";		
		$time = date("F jS, Y", strtotime($comment['timestamp']));
?>
<tr style="background-color: <?php echo $bgcolor;?>;">	
	<td>
	<?php echo strip_tags(nl2br($comment['ar_comment_content']), "<br>"); ?><br />		
	~<a target="_blank" href="<?php echo htmlspecialchars($comment['ar_comment_uri']); ?>"><?php echo htmlspecialchars($comment['ar_comment_name']); ?></a> &nbsp; <?php echo $time; ?>
	<?php if($this->registry->superadminIsLogged) { ?>
	<p class="para"><a href="index.php?route=articles/delete_comment/<?php echo $comment['ar_comment_id'].'&article='.$article['article_id'].'">'; ?>">Delete</a></p>
	<?php } ?>	
	</td>
</tr>
<?php } ?>
</table>
<?php } ?>
<br />
<?php echo isset($message) ? '<div class="message"><span class="red">'.$message.'</span></div>' : ""; ?>
<form name="commentform" id="commentform" method="post" action="index.php" style="padding: 0;">
	<table class="commentform" cellpadding="0" cellspacing="0">
	<tr>	
		<td>		
			<label for="articlecommentname">Your name</label><br />		
			<input type="text" name="articlecommentname" id="articlecommentname" class="inputtext" />	
		</td>
	</tr>
	<tr>	
		<td>		
			<label for="articlecommentemail">Your email</label><br />
			<input type="text" name="articlecommentemail" id="articlecommentemail" class="inputtext" />
		</td>
	</tr>
	<tr>	
		<td>		
			<label for="articlecommenturi">Your website URI</label><br />
			<input type="text" name="articlecommenturi" id="articlecommenturi" class="inputtext" />
		</td>
	</tr>
	<tr>	
		<td>		
			<label for="articlecommentcontent">Your comment</label><br />		
			<textarea name="articlecommentcontent" id="articlecommentcontent" rows="3" cols="50" class="txtarea"></textarea>	
		</td>
	</tr>
	<tr>
		<td>
			<label for="imgv">Security code</label><br />
			<input type="text" name="imgv" id="imgv" value="" onclick="this.value='';" class="inputtext" /><br />
			<img src="index.php?route=graphics/render_imagev" border="0" alt="" class="imagev" />
		</td>
	</tr>
	<tr>	
		<td>		
			<input type="hidden" name="route" value="articles/save_comment/<?php echo $article['article_id']; ?>" />		
			<input type="hidden" name="comform_token" value="<?php echo $comform_token; ?>" />		
			<input type="submit" name="savecomment" id="savecomment" value="Comment" class="" style="width: 126px; height: 34px; font-family: Arial, Sans; font-size: 0.95em; color: #FFFFFF; border: 0; background: url('templates/<?php echo $this->webTemplate; ?>/images/sbutton.png');" />	
		</td>
	</tr>
	</table>
</form>
<p></p>

<?php 
}
?>

<span class='st_googleplus_large' displayText='Google +'></span>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_linkedin_large' displayText='LinkedIn'></span>
<span class='st_sharethis_large' displayText='ShareThis'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
<span class='st_email_large' displayText='Email'></span>
<!-- <div class="fb-comments" data-href="http://vizcreations.com/index.php?route=articles/full_article/<?php //echo $article['article_id']; ?>" data-width="535" data-num-posts="10"></div> -->
<p>&nbsp;</p>
