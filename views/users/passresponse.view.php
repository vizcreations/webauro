<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Password Response View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
?>
<div id="left">
<div class="inside" style="padding-top: 0;">
<div id="cityajax">
<h3 class="msg"><?php echo $title; ?></h3>
<p class="para"><?php echo isset($message) ? $message : $intro; ?></p>
</div>
</div>
</div>

<div id="right">
	<?php require_once( __VIEWS.DS.'rightpanel.view.php' ); ?>
</div>
