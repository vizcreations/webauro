<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Login View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

$loginmessage = "";
if(isset($_REQUEST['message'])) {
	switch($_REQUEST['message']) {
		case 'empty':
			$loginmessage = "Please enter fields properly!";
			break;
		case 'auth':
			$loginmessage = "Invalid login";
			break;
		default:
			$loginmessage = "";
			break;
	}
}

$site = $this->registry->sitename;
$lform_token = uniqid();
$_SESSION[$site]['form_token'] = $lform_token;

?>
<div id="logindiv">
<h4><?php echo $title; ?></h4>
<p class="paratop parabottom"><?php echo isset($message) ? $message : $intro; ?></p>
<?php echo empty($loginmessage) ? "" : '<p class="paratop parabottom red">'.$loginmessage.'</p>'; ?>

<form name="loginform" id="loginform" action="index.php" method="post">
	<fieldset>
		<table class="form" style="width: 300px;" align="center">
		<tr>
			<td>
				<label for="username">Username:</label>
			</td>
			<td>
				<input type="text" name="username" id="username" size="35" class="select" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="password">Password:</label>
			</td>
			<td>
				<input type="password" name="password" id="password" size="35" class="select" />
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="form_token" id="form_token" value="<?php echo $form_token; ?>" />
				<input type="hidden" name="route" id="route" value="users/do_login" />
			</td>
			<td>
				<input type="submit" name="login" id="login" value="Login" class="loginbutton" />
			</td>
		</tr>
		</table>
	</fieldset>
	<p>Not registered? <a href="index.php?route=users/registration">Sign up</a></p>
</form>
</div>
