<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Thank You View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

?>
<h4 class="msg"><?php echo $title; ?></h4>
<p class="paratop parabottom"><?php echo isset($message) ? $message : $intro; ?></p>
<?php // require_once( __API.DS.'openinviter'.DS.'example.php' ); ?>
<!--<p class="para brown">Please login from the login form on the right panel after activation!</p>
--><?php 

$email = $postvars['email'];
$pos = strpos($email, "@");
// if($pos) {
	// $substr = substr($email, $pos);
	// if($substr != '@gmail.com' && $substr != '@yahoo.com' && $substr != '@msn.com') {
		// echo '<p class="para brown">Congratulations, please Login from the login form on the right panel!</p>';
	// } else {
?>
<!-- <fieldset>
<legend>Grab your friends and invite</legend>
<table class="form" style="width: 90%;">
<tr>
	<td>
		<label for="uname">Username</label><br />
		<input type="text" name="uname" id="uname" size="35" />
	</td>
</tr>
<tr>
	<td>
		<label for="pwd">Password</label><br />
		<input type="text" name="pwd" id="pwd" size="35" />
	</td>
</tr>
<tr>
	<td>
		<label for="e-mail">Email</label><br />
		<span><?php echo $email; ?></span>
	</td>
</tr>
<tr>
	<td>
		<input type="button" name="grabcontacts" id="grabcontacts" class="loginbutton" value="Continue" onclick="return;" />
	</td>
</tr>
</table>
</fieldset> -->
<?php 
	// }
// }
?>
