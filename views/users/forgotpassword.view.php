<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Forgot Password View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

$sitename = $this->registry->sitename;
$fform_token = uniqid();
$_SESSION[$sitename]['fform_token'] = $fform_token;
?>

<h4 class="msg"><?php echo $title; ?></h4>
<?php echo isset($message) ? '<p class="para red">'.$message.'</p>' : '<br />'; ?>
<form name="passwordform" id="passwordform" method="post" action="index.php" style="padding: 0;">
	<fieldset class="container" style="padding: 0; margin: 2px;">
	<div class="bbar"></div>
	<h3>Enter your Email to send request</h3>
	<table class="form">
	<tr>
		<td align="right">
			<label for="uemail">Email</label>
		</td>
		<td>
			<input type="text" name="uemail" id="uemail" size="35" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="users/password_form" />
			<input type="hidden" name="fform_token" value="<?php echo $fform_token; ?>" />
		</td>
		<td>
			<input type="submit" name="sendpassreq" id="sendpassreq" class="loginbutton" value="Send" />
		</td>
	</tr>
	</table>
	<p class="para"></p>
	<div class="ybar" style="margin: 0;"></div>
	</fieldset>
</form>
