<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Change Password View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

?>

<script type="text/javascript">
<!--
function checkPassChange() {
	var snewpass, snewpass2;
	with(window.document.changepassform) {
		snewpass = newpassword;
		snewpass2 = newpassword2;
	}
	if(snewpass.value.trim() == '') {
		window.alert("Please enter new password!");
		snewpass.focus();
		return false;
	} else if(snewpass.value.trim().length > 10) {
		window.alert("Password length must not exceed 10");
		snewpass.focus();
		return false;
	} else if(snewpass.value.trim().length < 5) {
		window.alert("Password length must not be less than 5!");
		snewpass.focus();
		return false;
	} else if(snewpass2.value.trim() == '') {
		window.alert("Please enter password again!");
		snewpass2.focus();
		return false;
	} else if(snewpass.value.trim() != snewpass2.value.trim()) {
		snewpass2.focus();
		window.alert("Passwords do not match!");
		return false;
	}
	return true;
}
//-->
</script>

<h4 class="msg"><?php echo $title; ?></h4>
<p class="para"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="changepassform" id="changepassform" method="post" action="index.php" onsubmit="return checkPassChange();" style="padding: 0;">
	<fieldset class="container" style="padding: 0; margin: 2px;">
	<div class="bbar"></div>
	<table class="form">
	<tr>
		<td align="right">
			<label for="newpassword">New password</label>
		</td>
		<td>
			<input type="password" name="newpassword" id="newpassword" size="35" />
		</td>
	</tr>
	<tr>
		<td align="right">
			<label for="newpassword2">Type again</label>
		</td>
		<td>
			<input type="password" name="newpassword2" id="newpassword2" size="35" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="users/change_password/<?php echo $password; ?>" />
		</td>
		<td>
			<input type="submit" class="loginbutton" name="changepass" id="changepass" value="Change" />
		</td>
	</tr>
	</table>
	<div class="ybar" style="margin: 0; margin-top: 10px;"></div>
	</fieldset>
</form>
