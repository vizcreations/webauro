<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Profile View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
$message = '';
if(isset($_GET['message'])) {
	$msg = $_GET['message'];
	switch($msg) {
		case 'uexists':
			$message = "The first name and last name combination already exists. Try another!";
			break;
		case 'success':
			$message = "Profile updated successfully!";
			break;
	}
}

$sitename = $this->registry->sitename;
$prform_token = uniqid();
$_SESSION[$sitename]['prform_token'] = $prform_token;

?>

<h4 class="msg"><?php echo $title; ?>&nbsp;&nbsp;</h4>
<p class="paratop parabottom"><?php echo isset($message) ? $message : $intro; ?></p>

	<!-- User Profile -->
	<table class="container" cellpadding="3" cellspacing="3" style="border-bottom: 1px #DCDCDC solid; margin: 10px 0 10px; width: 97.7%;">
	<tr>
		<td colspan="2" style="padding: 0; vertical-align: top;">
			<div class="bbar"></div>
		</td>
	</tr>
	<tr>
		<td style="width: 10%; vertical-align: top; padding: 10px;" align="center">
			<div id="thediv001" onclick="this.style.display='none';" style="display: none; background: #FFFFFF;"></div>
			<img width="60px" src="images/profiles/thumbs/<?php echo (empty($user['user_image'])) ? 'profile.jpg' : $user['user_image']; ?>" alt="profile" border="0" onclick="zoomImage('<?php echo $user['user_image']; ?>', 'thediv001');" />
		</td>
		<td>
		<?php
			if(!isset($edit)) {
		?>
			<table cellpadding="0" cellspacing="0" class="form5" width="75%">
				<tr>
					<td width="30%"><b><label for="fname">Username</label></b></td><td><?php echo $user['user_name']; ?></td>
				</tr>
				<tr>
					<td><b><label for="location">Location</label></b></td><td><?php echo $user['user_location']; ?></td>
				</tr>
				<tr>
					<td><b><label for="joined">Joined</label></b></td><td><?php echo date("j M Y", strtotime($user['user_joined'])); ?></td>
				</tr>
				<?php
					if($user['user_id'] == $this->registry->user->userid) {
				?>
				<tr>
					<td>&nbsp;</td>
					<td>
					<small class="small">
						<a href="index.php?route=users/edit_profile/<?php echo $user['user_id']; ?>">Edit profile</a>&nbsp;&nbsp;
						<a href="index.php?route=users/change_userpassword/<?php echo $this->registry->user->userid; ?>">Change password</a>
					</small>
					</td>
				</tr>
				<?php
					}
				?>
			</table>
			<?php 
			} else {
			?>
			<form name="profileform" id="bookingform" action="index.php" method="post" id="profileform" enctype="multipart/form-data" onsubmit="return checkImage();">
			<table class="form" style="width: 97%;">
			<tr>
				<td>
					<label for="fname">Username</label>
				</td>
				<td>
					<input type="text" class="select" name="uname" id="username" value="<?php echo $user['user_name']; ?>" onkeyup="checkUser(this.value);" />&nbsp;&nbsp;<span id="userload"></span>
					<br /><span id="uspan"></span>
				</td>
			</tr>
			<tr>
				<td>
					<label for="email">Email</label>
				</td>
				<td>
					<input type="text" class="select" name="email" id="email" value="<?php echo $user['user_email']; ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<label for="location">Location:</label>
				</td>
				<td id="cityselect2">
					<input type="text" name="location" id="location" value="<?php echo $user['user_location']; ?>" />
				</td>
			</tr>
			<tr>
				<td style="border: 0;">Change image</td>
				<td style="border: 0;">
					<input type="file" name="image" /><br /> .jpg (or) .png (or) .bmp only<br />
					<input type="hidden" name="route" value="users/update_profile" />
					<input type="hidden" name="prform_token" value="<?php echo $prform_token; ?>" /><br />
					<input type="hidden" name="oimg" value="<?php echo $user['user_image']; ?>" />
					<input type="submit" class="loginbutton" name="update" id="update" value="Update" />
				</td>
			</tr>
			</table>
			</form>
			<?php
					}
			?>
		</td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 0; vertical-align: top;">
			<div class="ybar" style="margin: 0; margin-top: 10px;"></div>
		</td>
	</tr>
	</table>
