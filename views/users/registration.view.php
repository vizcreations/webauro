<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users - Registration View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

extract( $_REQUEST );
$regform_token = uniqid();
$sitename = $this->registry->sitename;
$_SESSION[$sitename]['regform_token'] = $regform_token;
?>

<?php
	if(isset($_REQUEST['regmessage'])) {
		$msg = $_GET['regmessage'];
		switch($msg) {
			case 'empty':
				$message = "Please enter/select all required fields!";
				break;
			case 'pass':
				$message = "The two passwords do not match!";
				break;
			case 'uexists':
				$message = "Username chosen already exists. Choose another..";
				break;
			case 'emexists':
				$message = "The email is already taken! Please choose another..";
				break;
			default:
				$message = "Technical problem!";
				break;
		}
		$regmessage = $message;
	} else {
		$regmessage = "";
	}
?>
<div id="regdiv">
	<h4><?php echo $title; ?></h4>
    <?php if(!empty($regmessage)) echo '<p class="para red">'.$regmessage.'</p>'; else echo isset($message) ? '<p class="para">'.$message.'</p>' : '<br />';  ?>
	<div class="form">
	<div class="inside">
	<form name="regform" id="regform" method="post" action="index.php" class="none" enctype="multipart/form-data" onsubmit="return validateRegForm();">
	<fieldset class="form none auto">
	<table class="form cent" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<label for="email">Email:</label>
		</td>
		<td>
			<input type="text" name="email" id="email" class="inputtext" onkeyup="checkEmail(this.value);" /><span id="eload"></span>
			<br /><span id="espan"></span>
		</td>
	</tr>
	<tr>
		<td>
			<label for="uname">Username:</label>
		</td>
		<td>
			<input type="text" name="uname" class="inputtext" />
			<br /><span id="uspan"></span>
		</td>
	</tr>
	<tr>
		<td>
			<label for="password">Password:</label>
		</td>
		<td>
			<input type="password" name="password" id="password" class="inputtext" />
			<br /><span id="pspan"></span>
		</td>
	</tr>
	<tr>
		<td>
			<label for="password2">Retype password:</label>
		</td>
		<td>
			<input type="password" name="password2" id="password2" class="inputtext" />
			<br /><span id="p2span"></span>
		</td>
	</tr>
	<tr>
		<td>
			<label for="location">Location</label>
		</td>
		<td>
			<input type="text" name="location" id="location" class="inputtext" />
			<br /><span id="locspan"></span>
		</td>
	</tr>
	<tr>
		<td style="border: 0;">&nbsp;</td>
		<td>
			<input type="text" name="imgv" id="imgv" value="enter code here.." onclick="this.value='';" class="inputtext" /><br />
			<!-- Whenever this page gets loaded a numerical code is saved in session and displayed below -->
			<img src="index.php?route=graphics/render_imagev" border="0" alt="" class="imagev" />
			<br /><span id="imgvspan"></span>
		</td>
	</tr>
	<tr>
		<td align="right" valign="top" style="border: 0;">
			<input style="border: none;" type="checkbox" name="agree" id="agree" value="yes" />
		</td>
		<td valign="top" style="border: 0;">
			I agree to <a href="javascript:void(0);">terms &amp; conditions</a>
			<br /><span id="agspan"></span>
		</td>
	</tr>
	<tr>
		<td style="border: 0;">
			<input type="hidden" name="regform_token" value="<?php echo $regform_token; ?>" />
			<input type="hidden" name="route" value="users/save_reg" />
		</td>
		<td style="border: 0;">
			<input type="submit" name="save" id="save" value="Submit" class="button" />
		</td>
	</tr>
	</table>
	</fieldset>
	</form>
	</div>
	</div>
</div>
