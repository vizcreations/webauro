<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="homet">
<?php 
if(!$articles) {
?>
<tr><td><span class="red">No articles created yet!</span></td></tr>
<?php
} else {
?>
<tr>
	<th>ID</th>
	<th>Article name</th>
	<th>Article author</th>
	<th>Category</th>
	<th>Article Status</th>
	<th>Comment Status</th>
</tr>
<?php
	$model = new ArticlesModel();
	for($i=0; $i<count($articles); $i++) {
		$article = $articles[$i];
		if(!$article['article_status']) {
			$style = 'style="color: #CCCCCC;"';
			$option = '<a href="index.php?route=articles/enable_article/'.$article['article_id'].'&page='.$page.'">Enable</a>';
		} else {
			$style = '';
			$option = '<a href="index.php?route=articles/disable_article/'.$article['article_id'].'&page='.$page.'">Disable</a>';
		}
		$commentstatus = $model->getCommentsStatus($article['article_id']);
		if(!$commentstatus) {
			$cop = '<a href="index.php?route=articles/enable_c_status/'.$article['article_id'].'&page='.$page.'">Enable</a>';
		} else {
			$cop = '<a href="index.php?route=articles/disable_c_status/'.$article['article_id'].'&page='.$page.'">Disable</a>';
		}
?>
<tr>
	<td style="width: 5%;" <?php echo $style; ?>><?php echo $article['article_id']; ?></td>
	<td><a <?php echo $style; ?> href="index.php?route=articles/edit_article/<?php echo $article['article_id']; ?>"><?php echo $article['article_title']; ?></a></td>
	<td <?php echo $style; ?>><?php echo $article['article_author']; ?></td>
	<!--<td><?php echo date("F jS Y", strtotime($article['timestamp'])); ?></td>
	-->
	<td <?php echo $style; ?>><?php echo $article['ar_category_name']; ?></td>
	<td><?php echo $option; ?></td>
	<td><?php echo $cop; ?></td>
</tr>
<?php
	}
}
?>
</table>
<?php 
	$nav = '';
	$link = 'index.php?route=articles/page_articles/';
	for($pagenum=1; $pagenum<=$maxpage; $pagenum++) {
		if($page == $pagenum) {
			if($maxpage == 1) $nav .= '';
			else $nav .= ' '.$pagenum.' ';
		} else $nav .= ' <a href="'.$link.$pagenum.'">'.$pagenum.'</a> ';
	}
	if($page > 1) {
		$pagenum = $page - 1;
		$prev = ' <a href="'.$link.$pagenum.'">[ Prev ]</a> ';
		$first = ' <a href="'.$link.'1">[ First ]</a> ';
	} else {
		$prev = '';
		$first = '';
	}
	if($page < $maxpage) {
		$pagenum = $page + 1;
		$next = ' <a href="'.$link.$pagenum.'">[ Next ]</a> ';
		$last = ' <a href="'.$link.$maxpage.'">[ Last ]</a> ';
	} else {
		$next = '';
		$last = '';
	}
	echo '<table class="homet"><tr><td>'.$first.$prev.$nav.$next.$last.'</td></tr></table>';
?>
<p class="para">
	<a href="index.php?route=articles/add_article"><img src="images/png/icons/addf.png" alt="add" border="0" /></a>
</p>
<p class="intro para"><a href="index.php?route=articles/categories"><b>Categories</b></a></p>
</div>
