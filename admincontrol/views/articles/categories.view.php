<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Categories View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="home">
<tr>
<?php 
if(!$categories) {
?>
<td><p class="para red">No categories created yet!</p></td>
<?php 
} else {
	for($i=0; $i<count($categories); $i++) {
		$category = $categories[$i];
?>
<td>
	<h5><a href="index.php?route=articles/edit_category/<?php echo $category['ar_category_id']; ?>"><?php echo $category['ar_category_name']; ?></a></h5>
	<ul class="list">
	<?php 
		$amodel = new ArticlesModel();
		try {
			$articles = $amodel->getCategoryArticles($category['ar_category_id']);
		} catch(Exception $e) {
			$articles = 0;
			echo '<li class="red">'.$e->getMessage().'</li>';
		}
		if(!$articles) {
	?>
	<li class="red">No articles created yet!</li>
	<?php
		} else {
			for($a=0; $a<count($articles); $a++) {
				$article = $articles[$a];
	?>
	<li><a href="index.php?route=articles/edit_article/<?php echo $article['article_id']; ?>"><?php echo $article['article_title']; ?></a></li>
	<?php
			}
		}
	?>
	</ul>
</td>
<?php
		if((($i+1)%4) == 0) echo '</tr><tr>';
	}
}
?>
</tr>
</table>
<p class="para"><a href="index.php?route=articles/add_category"><img src="images/png/icons/addf.png" alt="add" border="0" /></a></p>
</div>
