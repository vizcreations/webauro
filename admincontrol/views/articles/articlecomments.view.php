<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Article comments View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
 ?>
 
 <h2><?php echo $title; ?></h2>
 <p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>
 
 <table class="home">
 <?php 
 if(!$comments) {
 ?>
 <tr><td><p class="para red">No comments for this article yet!</p></td></tr>
 <?php 
 } else {
 	for($i=0; $i<count($comments); $i++) {
 		$comment = $comments[$i];
 ?>
 <tr>
 	<td>
 		<h5><?php echo $comment['ar_comment_name']; ?></h5>
 		<p class="para"><?php echo $comment['ar_comment_content']; ?></p>
 		<p class="small"><small><?php echo date("F jS, Y", strtotime($comment['timestamp'])); ?></small></p>
 		<p class="para"><a href="index.php?route=articles/remove_comment/<?php echo $comment['ar_comment_id']; ?>&aid=<?php echo $article['article_id']; ?>">Remove comment</a></p>
 	</td>
 </tr>
 <?php
 	}
 ?>
 
 <?php 
 }
 ?>
 </table>
 </div>
