<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Add article View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

$sitename = $this->registry->sitename;
$arform_token = uniqid();
$_SESSION[$sitename]['arform_token'] = $arform_token;
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="articleform" id="articleform" method="post" action="index.php">
<fieldset>
<legend>Article details</legend>
<table class="form">
<tr>
	<td>
		<label for="articletitle">Title</label><br />
		<input type="text" name="articletitle" id="articletitle" size="35" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="articleauthor">Author</label><br />
		<input type="text" name="articleauthor" id="articleauthor" size="35" value="<?php echo $this->registry->username; ?>" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="articlecat">Category</label><br />
		<?php 
		if($categories) {
		?>
		<select name="articlecategory" id="articlecategory">
		<?php
			for($cat=0; $cat<count($categories); $cat++) {
				$category = $categories[$cat];
		?>
			<option value="<?php echo $category['ar_category_id']; ?>"><?php echo $category['ar_category_name']; ?></option>
		<?php 
			}
		?>
		</select>
		<?php
		} else {
		?>
		<p class="red">No categories created yet!</p>
		<?php 
		}
		?>
	</td>
</tr>
<tr>
	<td>
		<label for="articlecontent">Content</label><br />
		<textarea name="articlecontent" id="articlecontent" rows="25" style="width: 100%;"></textarea>
	</td>
</tr>
<tr>
	<td>
		<label for="seotitle">SEO Title</label><br />
		<input type="text" name="seotitle" id="seotitle" size="35" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="seokeywords">SEO Keywords</label><br />
		<textarea name="seokeywords" rows="2" style="width: 100%;"></textarea>
	</td>
</tr>
<tr>
	<td>
		<label for="seodescription">SEO Description</label><br />
		<textarea name="seodescription" rows="2" style="width: 100%;"></textarea>
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="route" value="articles/add_article" />
		<input type="hidden" name="arform_token" value="<?php echo $arform_token; ?>" />
		<?php if($categories) { ?>
		<input type="submit" name="addarticle" id="addarticle" value="Add" />
		<?php } ?>
	</td>
</tr>
</table>
</fieldset>
</form>

</div>
