<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Edit category View 
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="arcategoryform" id="arcategoryform" action="index.php" method="post">
<fieldset>
<legend>Category details</legend>
<table class="form">
<tr>
	<td>
		<label for="categoryname">Category name</label><br />
		<input type="text" name="categoryname" id="categoryname" size="35" class="inputtext" value="<?php echo $category['ar_category_name']; ?>" />
	</td>
</tr>
<tr>
	<td>
		<label for="categorydesc">Category description</label><br />
		<textarea name="categorydesc" id="categorydesc" rows="3" style="width: 100%"><?php echo $category['ar_category_description']; ?></textarea>
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="route" value="articles/edit_category/<?php echo $category['ar_category_id']; ?>" />
		<input type="submit" name="editcategory" id="editcategory" value="Edit" />
	</td>
</tr>
</table>
</fieldset>
</form>
</div>
