<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol FragsMenu Edit frag menu View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="fragform" id="fragform" method="post" action="index.php">
<fieldset>
	<legend>Details</legend>
	<table class="form">
	<tr>
		<td>
			<label for="fragname">Frag name</label><br />
			<input type="text" name="fragname" id="fragname" value="<?php echo $frag['frag_name']; ?>" size="37" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="fragtitle">Frag title</label><br />
			<input type="text" name="fragtitle" id="fragtitle" value="<?php echo $frag['frag_title']; ?>" size="37" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="fragpos">Frag position</label><br />
			<?php
			if(!$positions) {
			?>
			<p class="para red">No positions created yet!</p>
			<?php 
			} else {
			?>
			<select name="fragpos" id="fragpos">
			<?php 
				for($p=0; $p<count($positions); $p++) {
					$position = $positions[$p];
					if($position['pos_id'] == $frag['pos_id']) $selected = "selected=\"selected\"";
					else $selected = "";
			?>
			<option value="<?php echo $position['pos_id']; ?>" <?php echo $selected; ?>><?php echo $position['pos_name']; ?></option>
			<?php 
				}
			?>
			</select>
			<?php 
			}
			?>
		</td>
	</tr>
	<tr>
		<td>
			<label for="fragargument">Menu</label><br />
			<select name="fragargument" id="fragargument">
			<?php
			if(!$menus) {
			?>
			<option value="0">No menus yet!</option>
			<?php 
			} else {
				for($c=0; $c<count($menus); $c++) {
					$menu = $menus[$c];
					if($menu['menu_id'] == $argument) $selected = "selected=\"selected\"";
					else $selected = "";
			?>
			<option value="<?php echo $menu['menu_id']; ?>" <?php echo $selected; ?>><?php echo $menu['menu_name']; ?></option>
			<?php
				}
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="frags/edit_frag_step3/<?php echo $frag['frag_id']; ?>" />
			<input type="hidden" name="fragtypeid" value="<?php echo $fragtypeid; ?>" />
			<input type="hidden" name="fragtype" value="<?php echo $fragtype; ?>" />
			<input type="submit" name="editfrag" id="editfrag" value="Edit" />
		</td>
	</tr>
	</table>
</fieldset>
</form>

</div>
