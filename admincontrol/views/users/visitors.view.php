<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Visitors View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>
<table class="home">
<tr>
<td>
<?php 
if(!$visitors) {
?>
<p class="para red">No visitors for today!</p>
<?php
	$unique = 0; 
} else {
?>
<ol class="list">
<?php
	for($i=0; $i<count($visitors); $i++) {
		$avisitors[] = $visitors[$i]['visitor_ip'];
	}
	$unqvisitors = array_unique($avisitors);
	$unique = count($unqvisitors);
	for($v=0; $v<count($visitors); $v++) {
		$visitor = $visitors[$v];
		//$tags = get_meta_tags('http://www.geobytes.com/IpLocator.htm?GetLocation&template=php3.txt&IpAddress='.$visitor['visitor_ip']);
?>
<li><b><?php echo $visitor['visitor_ip']; ?></b> <small class="small">at <?php echo date("g:i A", strtotime($visitor['timestamp'])); ?> minus 12 hours from <i><?php echo isset($tags['city']) ? $tags['city']: 'Unknown'; ?></i></small></li>
<?php
	}
?>
</ol>
<?php 
}
?>
</td>
</tr>
</table>
<p class="intro para"><b><?php echo ($unique) ? $unique : 0; ?></b> Unique Vistors Today!</p>
</div>
