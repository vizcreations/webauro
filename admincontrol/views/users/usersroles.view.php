<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Users roles View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="home">
<tr>
<?php 
if(!$usersroles) {
?>
<td><p class="para red">No users roles created yet!</p></td>
<?php 
} else {
	for($i=0; $i<count($usersroles); $i++) {
		$usersrole = $usersroles[$i];
?>
<td>
	<h5><?php echo $usersrole['user_role_name']; ?></h5>
	<ul class="list">
	<?php 
	$model = new UsersModel();
	try {
		$users = $model->getRoleUsers($usersrole['user_role_id']);
	} catch(Exception $e) {
		$users = 0;
		echo '<li class="red">'.$e->getMessage().'</li>';
	}
	if(!$users) {
	?>
	<li class="red">No users created yet for this role!</li>
	<?php 
	} else {
		for($u=0; $u<count($users); $u++) {
			$user = $users[$u];
	?>
	<li><a href="index.php?route=users/user/<?php echo $user['user_id']; ?>"><?php echo $user['user_name']; ?></a></li>
	<?php
		}
	}
	?>
	</ul>
</td>
<?php
		if((($i+1) % 4) == 0) echo '</tr><tr>';
	}
?>

<?php
}
?>
</tr>
</table>
<p class="para"><a href="index.php?route=users/add_usersrole"><img src="images/png/icons/addf.png" alt="" border="0" /></a></p>
</div>
