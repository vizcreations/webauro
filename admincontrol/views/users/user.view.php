<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users User full View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<script type="text/javascript">
<!--
function delUser(userid) {
	if(confirm("Are you sure you want to delete this user?")) {
		window.location.href = 'index.php?route=users/delete_user/' + userid;
	}
}
//-->
</script>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="userform" id="userform" method="post" action="index.php">
<fieldset>
	<legend>Edit details</legend>
	<table class="form">
		<tr>
			<td>
				<label for="username">Username</label><br />
				<input type="text" name="username" id="username" size="37" value="<?php echo $user['user_name']; ?>" class="inputtext" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="useremail">Email</label><br />
				<input type="text" name="useremail" id="useremail" size="37" value="<?php echo $user['user_email']; ?>" class="inputtext" />
			</td>
		</tr>		
		<tr>			
			<td>				
				<label for="userstatus">Status</label><br />				
				<?php 				
					if($user['user_status']) {					
						$val1 = "1";					
						$op1 = "Active";					
						$val2 = "0";					
						$op2 = "Inactive";				
					} else {					
						$val1 = "0";					
						$op1 = "Inactive";					
						$val2 = "1";					
						$op2 = "Active";				
					}				
				?>				
				<select name="userstatus" id="userstatus">					
					<option value="<?php echo $val1; ?>"><?php echo $op1; ?></option>					
					<option value="<?php echo $val2; ?>"><?php echo $op2; ?></option>				
				</select>			
			</td>		
		</tr>				
		<tr>			
			<td>				
				<label for="userlocation">Location</label><br />				
				<input type="text" name="userlocation" id="userlocation" size="37" value="<?php echo $user['user_location']; ?>" class="inputtext" />			
			</td>		
		</tr>				
		<tr>			
			<td>				
				<label for="userrole">User Role</label><br />				
				<?php 				
					if(!$usersroles) {				
				?>				
				<p class="para red">No users roles created yet!</p>				
				<?php 				
					} else {				
				?>				
				<select name="userrole" id="userrole">				
				<?php					
					for($i=0; $i<count($usersroles); $i++) {						
						$usersrole = $usersroles[$i];						
						if($usersrole['user_role_id'] == $user['user_role_id']) $selected = "selected=\"selected\"";						
						else $selected = "";				
				?>				
				<option value="<?php echo $usersrole['user_role_id']; ?>" <?php echo $selected; ?>><?php echo $usersrole['user_role_name']; ?></option>				
				<?php					
					}				
				?>				
				</select>				
				<?php 				
					} 				
				?>			
			</td>		
		</tr>
		<tr>
			<td>
				<label for="newpassword">New password</label><br />
				<input type="password" name="newpassword" id="newpassword" size="37" class="inputtext" />
			</td>
		</tr>
		<tr>
			<td>
				<label for="newpassword2">Type password again</label><br />
				<input type="password" name="newpassword2" id="newpassword2" size="37" class="inputtext" />
			</td>
		</tr>
		<tr>
			<td>
				<input type="hidden" name="route" value="users/user/<?php echo $user['user_id']; ?>" />
				<input type="submit" name="edituser" id="edituser" value="Edit user" />
			</td>
		</tr>
	</table>
</fieldset>
</form>
<!-- <p class="intro"><a href="javascript:delUser(<?php echo $user['user_id']; ?>);">Delete user</a></p> -->
</div>
