<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Add user View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

$sitename = $this->registry->sitename;

$userform_token = uniqid();
$_SESSION[$sitename]['userform_token'] = $userform_token;
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="userform" id="userform" method="post" action="index.php">
<fieldset>
<legend>User details</legend>
	<table class="form">
	<tr>
		<td>
			<label for="username">Username</label><br />
			<input type="text" name="username" id="username" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="password">Password</label><br />
			<input type="password" name="password" id="password" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="password2">Enter Password again</label><br />
			<input type="password" name="password2" id="password2" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="email">Email</label><br />
			<input type="text" name="email" id="email" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="location">Location</label><br />
			<textarea name="location" id="location" rows="2" cols="70"></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<label for="userrole">Role</label><br />
			<select name="userrole" id="userrole">
			<?php 
			if(!$roles) {
			?>
			<option value="0">No roles</option>
			<?php 
			} else {
				for($i=0; $i<count($roles); $i++) {
					$role = $roles[$i];
					if($this->registry->adminrole != 'superadmin') {
						if($role['user_role_name'] == 'superadmin') continue;
					}
					
					if($this->registry->adminrole == 'user') {
						if($role['user_role_name'] == 'admin' || $role['user_role_name'] == 'superadmin') continue;
					}
			?>
			<option value="<?php echo $role['user_role_id']; ?>"><?php echo $role['user_role_name']; ?></option>
			<?php
				}
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="users/add_user" />
			<input type="hidden" name="userform_token" value="<?php echo $userform_token; ?>" />
			<input type="submit" name="adduser" id="adduser" value="Save" />
		</td>
	</tr>
	</table>
</fieldset>
</form>
</div>
