<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Add users role View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

$roleform_token = uniqid();
$_SESSION[$sitename]['roleform_token'] = $roleform_token;
 
?>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="usersroleform" id="usersroleform" action="index.php" method="post">
<fieldset>
	<legend>Role details</legend>
	<table class="form">
	<tr>
		<td>
			<label for="rolename">Role name</label><br />
			<input type="text" name="rolename" id="rolename" size="40" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="roledesc">Role description</label><br />
			<textarea name="roledesc" id="roledesc" cols="70" rows="2"></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="users/add_usersrole" />
			<input type="hidden" name="roleform_token" value="<?php echo $roleform_token; ?>" />
			<input type="submit" name="addrole" id="addrole" value="Add" />
		</td>
	</tr>
	</table>
</fieldset>
</form>

</div>
