<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

if(isset($_REQUEST['message'])) $msg = $_REQUEST['message'];
else $msg = "";

?>
<script type="text/javascript">
<!--
function delUser(userid) {
	if(confirm("Are you sure you want to delete this user?")) {
		window.location.href = 'index.php?route=users/delete_user/' + userid;
	}
}
//-->
</script>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="homet">
<?php if(!$users) { ?>
<tr>
	<td class="para red">No users created yet!</td>
</tr>
<?php } else { ?>
<tr>
	<th>ID</th>
	<th>Username</th>
	<th>Email</th>
	<th>Status</th>
	<th>Joined</th>
	<th>Role</th>
</tr>
<?php 
		for($i=0; $i<count($users); $i++) {		
			$user = $users[$i];
?>
<tr>
	<td style="width: 5%;"><?php echo $user['user_id']; ?></td>
	<td><a href="index.php?route=users/user/<?php echo $user['user_id']; ?>"><?php echo $user['user_name']; ?></a></td>
	<td><?php echo $user['user_email']; ?></td>
	<td><?php echo ($user['user_status']) ? 'Active - (<a href="index.php?route=users/deactivate_user/'.$user['user_id'].'">Deactivate</a>)' : '<span style="color: #CCCCCC;">Inactive</span> - (<a href="index.php?route=users/activate_user/'.$user['user_id'].'">Activate</a>)'; ?></td>
	<td><?php echo date("F jS Y", strtotime($user['user_joined'])); ?></td>	<td><?php echo $user['user_role_name']; ?></td>
</tr>
<?php
		}
}
?>
</table>
<?php if($this->registry->adminrole != 'user') { ?>
<p class="intro"><a href="index.php?route=users/add_user"><img src="images/png/icons/addf.png" alt="add" border="0" /></a></p>
<?php } ?>

<p class="intro">
<?php if($this->registry->adminrole == 'superadmin') { ?>
<a href="index.php?route=users/users_roles"><b>Users Roles</b></a> &nbsp; 
<?php } ?>
<a href="index.php?route=users/visitors">Visitors</a></p>

</div>
