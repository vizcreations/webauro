<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Menus Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="home">
<tr>
<?php 
if(!$menus) {
?>
<td><p class="para red">No menus created yet!</p></td>
<?php 
} else {
	for($i=0; $i<count($menus); $i++) {
		$menu = $menus[$i];
		if(!$menu['menu_status']) {
			$style = 'style="color: #CCCCCC;"';
			$option = '<p class="para">(<a href="index.php?route=menus/enable_menu/'.$menu['menu_id'].'">Enable</a>)</p>';
		} else {
			$style = '';
			$option = '<p class="para">(<a href="index.php?route=menus/disable_menu/'.$menu['menu_id'].'">Disable</a>)</p>';
		}
?>
<td>
	<h5><a <?php echo $style; ?> href="index.php?route=menus/edit_menu/<?php echo $menu['menu_id']; ?>"><?php echo $menu['menu_name']; ?></a> - <small class="blue"><?php echo $menu['pos_name']; ?></small></h5>
	<ul class="list">
	<?php
		$mmodel = new MenusModel();
		try {
			$menuitems = $mmodel->getMenuitems($menu['menu_id']);
		} catch(Exception $e) {
			$menuitems = 0;
			echo '<li class="red">'.$e->getMessage().'</li>';
		}
		if(!$menuitems) {
	?>
	<li class="red" <?php echo $style; ?>>No menuitems for this menu</li>
	<?php
		} else {
			for($m=0; $m<count($menuitems); $m++) {
				$menuitem = $menuitems[$m];
				if(!$menuitem['menuitem_status']) {
					$style2 = 'style="color: #CCCCCC";';
					$option2 = '<small class="small">(<a href="index.php?route=menus/enable_menuitem/'.$menuitem['menuitem_id'].'">Enable<a>)</small>';
				} else {
					$style2 = '';
					$option2 = '<small class="small">(<a href="index.php?route=menus/disable_menuitem/'.$menuitem['menuitem_id'].'">Disable</a>)</small>';
				}
	?>
	<li><a <?php echo $style; ?> <?php echo $style2; ?> href="index.php?route=menus/edit_menuitem/<?php echo $menuitem['menuitem_id']; ?>"><?php echo $menuitem['menuitem_name']; ?></a>&nbsp;&nbsp; - &nbsp;&nbsp;<?php echo $option2; ?></li>
	<?php
			}
		}
	?>
	</ul>
	<?php echo $option; ?>
	<p class="intro"><a href="index.php?route=menus/add_menuitem/<?php echo $menu['menu_id']; ?>"><img src="images/png/icons/addf.png" alt="add" border="0" /></a></p>
</td>
<?php
		if((($i+1)%4) == 0) echo '</tr><tr>';
	}
}
?>
</tr>
</table>

<p class="para intro"><a href="index.php?route=menus/add_menu"><b>Add Menu</b></a></p>
</div>
