<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Menus Edit menu View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="menuform" id="menuform" action="index.php" method="post">
<fieldset>
	<legend>Menu details</legend>
	<table class="form">
	<tr>
		<td>
			<label for="menuname">Menu name</label><br />
			<input type="text" name="menuname" id="menuname" size="37" value="<?php echo $menu['menu_name']; ?>" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="menupos">Position</label><br />
			<?php 
			if(!$positions) {
			?>
			<p class="para red">No positions created yet!</p>
			<?php 
			} else {
			?>
			<select name="menupos" id="menupos">
			<?php 
				for($i=0; $i<count($positions); $i++) {
					$position = $positions[$i];
					if($position['pos_id'] == $menu['pos_id']) $selected = "selected=\"selected\"";
					else $selected = "";
			?>
			<option value="<?php echo $position['pos_id']; ?>" <?php echo $selected;?>><?php echo $position['pos_name']; ?></option>
			<?php 
				}
			?>
			</select>
			<?php 
			}
			?>
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="menus/edit_menu/<?php echo $menu['menu_id']; ?>" />
			<input type="submit" name="editmenu" id="editmenu" value="Edit Menu" />
		</td>
	</tr>
	</table>
</fieldset>
</form>

</div>
