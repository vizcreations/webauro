<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Menus Add menu item View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<script type="text/javascript">
<!--
function getViews(controllerid) {
	uri = 'async.php';
	uri += '?route=ajax/get_controller_views/' + controllerid;
	uri += '&sid=' + Math.random();
	div = 'views';
	sendAJAXRequest(div, uri);
}
//-->
</script>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<?php if(!$step) { ?>
<form name="menuitemform" id="menuitemform" action="index.php" method="post">
<fieldset>
<legend>Menuitem details</legend>
<table class="form">
<tr>
	<td>
		<label for="menuitemname">Menuitem name</label><br />
		<input type="text" name="menuitemname" id="menuitemname" size="35" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="menuitemcontroller">Controller</label><br />
		<?php 
		if(!$controllers) {
		?>
		<span class="red">No controllers created yet!</span>
		<?php 
		} else {
		?>
		<select name="menuitemcontroller" id="menuitemcontroller" onchange="getViews(this.value);">
		<?php
			for($c=0; $c<count($controllers); $c++) {
				$controller = $controllers[$c];
		?>
		<option value="<?php echo $controller['controller_id']; ?>"><?php echo $controller['controller_name']; ?></option>
		<?php
			}
		?>
		</select>
		<?php
		}
		?>
	</td>
</tr>
<tr>
	<td>
		<label for="menuitemview">View</label><br />
		<?php 
		if(!$views) {
		?>
		<p class="red">No views created yet!</p>
		<?php 
		} else {
		?>
		<div id="views">
		<select name="menuitemview" id="menuitemview">
		<?php 
			for($v=0; $v<count($views); $v++) {
				$view = $views[$v];
		?>
		<option value="<?php echo $view['view_id']; ?>"><?php echo $view['view_name']; ?></option>
		<?php
			}
		?>
		</select>
		</div>
		<?php 
		}
		?>
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="route" value="menus/add_menuitem_step2/<?php echo $menu['menu_id']; ?>" />
		<input type="submit" name="step2" id="step2" value="Next" />
	</td>
</tr>
</table>
</fieldset>
</form>

<?php } else { ?>

<form name="menuitemform" id="menuitemform" action="index.php" method="post">
<fieldset>
<legend>Proceed to creation</legend>

<table class="form">
<tr>
	<td>
		<label for="menuitemargument">Argument</label><br />
		<div id="arguments">
		<?php 
		if(!$arguments) {
		?>
		<p class="para red">No arguments</p>
		<?php 
		} else {
		?>
		<select name="menuitemargument" id="menuitemargument">
			<option value="0">No arguments</option>
		<?php
			for($i=0; $i<count($arguments); $i++) {
				$theargument = $arguments[$i];
		?>
		<option value="<?php echo $theargument[0]; ?>"><?php echo $theargument[1]; ?></option>
		<?php 
			}
		?>
		</select>
		<?php
		}
		?>
		</div>
	</td>
</tr>
<tr>
	<td>
		<label for="menuitemparent">Parent</label><br />
		<?php 		
		if(!$menuitems) {		
		?>		
		<input type="hidden" name="menuitemparent" value="0" />		
		<p class="para red">No menuitems created yet!</p>		
		<?php 	} else {	?>		
		<select name="menuitemparent" id="menuitemparent">		
			<option value="0">No parent</option>		
		<?php 	
			for($mp=0; $mp<count($menuitems); $mp++) {				
				$themenuitem = $menuitems[$mp];		
		?>		
			<option value="<?php echo $themenuitem['menuitem_id']; ?>"><?php echo $themenuitem['menuitem_name']; ?></option>		
		<?php 	}	?>
		</select>		
		<?php 	}	?>	
	</td>
</tr>
<tr>
	<td>
		<label for="menuitemtemplate">Template</label><br />
		<?php 
		if(!$templates) {
		?>
		<p class="para red">No templates</p>
		<?php 
		} else {
		?>
		<select name="menuitemtemplate" id="menuitemtemplate">
		<?php 
			for($t=0; $t<count($templates); $t++) {
				$thetemplate = $templates[$t];
		?>
		<option value="<?php echo $thetemplate['template_id']; ?>"><?php echo $thetemplate['template_name']; ?></option>
		<?php
			}
		?>
		</select>
		<?php 
		}
		?>
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="menuitemname" value="<?php echo $args['menuitemname']; ?>" />
		<input type="hidden" name="menuitemcontroller" value="<?php echo $args['menuitemcontroller']; ?>" />
		<input type="hidden" name="menuitemview" value="<?php echo $args['menuitemview']; ?>" />
		<input type="hidden" name="route" value="menus/add_menuitem_step3/<?php echo $args['menuid']; ?>" />
		<input type="submit" name="addmenuitem" id="addmenuitem" value="Add" /> &nbsp;&nbsp; <input type="button" value="Back" onclick="history.go(-1);" />
	</td>
</tr>
</table>
</fieldset>
</form>

<?php } ?>
</div>
