<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol default view
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
?>

<div class="container">
	<h2><?php echo $heading; ?></h2>
	<p class="intro"><?php echo $intro; ?></p>
	<table class="home">
	<tr>
		<td>
			<h5>New Articles</h5>
			<ul class="list">
			<?php 
			if(!$articles) {
			?>
			<li class="red">No new articles!</li>
			<?php
			} else {
				for($a=0; $a<count($articles); $a++) {
					$article = $articles[$a];
			?>
				<li><a href="index.php?route=articles/edit_article/<?php echo $article['article_id']; ?>"><?php echo $article['article_title']; ?></a></li>
			<?php	
				}
			}
			?>
			</ul>
			<p class="para intro"><a href="index.php?route=articles/add_article"><img src="images/png/icons/addf.png" border="0" alt="add" /></a></p>
		</td>
		<td>
			<h5>New Frags</h5>
			<ul class="list">
			<?php 
			if(!$frags) {
			?>
			<li class="red">No new frags!</li>
			<?php 
			} else {
				for($f=0; $f<count($frags); $f++) {
					$frag = $frags[$f];
			?>
				<li><a href="index.php?route=frags/edit_frag/<?php echo $frag['frag_id']; ?>"><?php echo $frag['frag_title']; ?></a></li>
			<?php
				}
			}
			?>
			</ul>
			<p class="para intro"><a href="index.php?route=frags/create_frag"><img src="images/png/icons/addf.png" border="0" alt="add" /></a></p>
		</td>
	</tr>
	<tr>
		<td>
			<h5>New Components</h5>
			<ul class="list">
			<?php 
			if(!$components) {
			?>
			<li class="red">No new components!</li>
			<?php 
			} else {
				for($c=0; $c<count($components); $c++) {
					$component = $components[$c];
			?>
				<li><a href="index.php?route=<?php echo $component['controller_name']; ?>"><?php echo $component['controller_name']; ?></a></li>
			<?php
				}
			}
			?>
			</ul>
			<p class="para intro"><a href="index.php?route=components/add_component"><img src="images/png/icons/addf.png" border="0" alt="add" /></a></p>
		</td>
		<td>
			<h5>New Menus</h5>
			<ul class="list">
			<?php 
			if(!$menus) {
			?>
			<li class="red">No new menus!</li>
			<?php
			} else {
				for($m=0; $m<count($menus); $m++) {
					$menu = $menus[$m];
			?>
				<li><a href="index.php?route=menus/edit_menu/<?php echo $menu['menu_id']; ?>"><?php echo $menu['menu_name']; ?></a></li>
			<?php
				}
			}
			?>
			</ul>
			<p class="para intro"><a href="index.php?route=menus/add_menu"><img src="images/png/icons/addf.png" border="0" alt="add" /></a></p>
		</td>
	</tr>
	</table>
</div>

