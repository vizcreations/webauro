<div class="logincontainer">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol default view
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
$admform_token = uniqid();
$_SESSION['admform_token'] = $admform_token; 
?>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table id="logindiv">
<tr>
	<td align="center">
		<form name="loginform" id="loginform" action="index.php" method="post">
			<fieldset>
				<table class="form">
				<tr>
					<td width="10%">
						<label for="ausername">Username</label>
					</td>
					<td>
						<input type="text" name="ausername" id="ausername" size="60" class="inputtext" />
					</td>
				</tr>
				<tr>
					<td>
						<label for="apassword">Password</label>
					</td>
					<td>
						<input type="password" name="apassword" id="apassword" size="60" class="inputtext" />
					</td>
				</tr>
				<tr>
					<td>
						<input type="hidden" name="route" value="index/do_login" />
						<input type="hidden" name="admform_token" value="<?php echo $admform_token; ?>" />			
					</td>
					<td>
						<input type="image" src="images/png/icons/login.png" name="login" id="login" value="Login" />
					</td>
				</tr>
				</table>
			</fieldset>
		</form>
	</td>
</tr>
</table>
</div>
