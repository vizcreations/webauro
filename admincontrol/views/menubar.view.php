<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Menubar View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>
<ul class="hmenu">
	<li><a href="index.php">Home</a></li>
	<li><a href="index.php?route=menus">Menus</a></li>
	<li><a href="index.php?route=articles">Articles</a></li>
	<li><a href="index.php?route=users">Users</a></li>
	<li><a href="index.php?route=components">Components</a></li>
	<li><a href="index.php?route=frags">Frags</a></li>
	<li><a href="index.php?route=positions">Positions</a></li>
	<li><a href="index.php?route=templates">Templates</a></li>
	<li><a href="index.php?route=contact">Contact</a></li>
	<li><a target="_blank" href="<?php echo $registry->url; ?>">Launch website</a></li>
	<li><a href="index.php?route=users/logout"><span class="normal red">Logout</span></a></li>
</ul>
