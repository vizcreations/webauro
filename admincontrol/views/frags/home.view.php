<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Frags Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="home">
<tr>
<?php 
if(!$frags) {
?>
<td><p class="para red">No frags created yet!</p></td>
<?php 
} else {
	for($i=0; $i<count($frags); $i++) {
		$frag = $frags[$i];
		if(!$frag['frag_status']) {
			$style = "style=\"color: #CCCCCC;\"";
			$option = '<p class="para">(<a href="index.php?route=frags/enable_frag/'.$frag['frag_id'].'">Enable</a>)</p>';
		} else {
			$style = "";
			$option = '<p class="para">(<a href="index.php?route=frags/disable_frag/'.$frag['frag_id'].'">Disable</a>)</p>';
		}
?>
<td>
	<h5><a <?php echo $style; ?> href="index.php?route=frags/edit_frag/<?php echo $frag['frag_id']; ?>"><?php echo $frag['frag_title']; ?></a></h5>
	<ul class="list">
		<li <?php echo $style; ?>>Name - <small><?php echo $frag['frag_name']; ?></small></li>
		<li <?php echo $style; ?>>Position - <small><?php echo $frag['pos_name']; ?></small></li>
	</ul>
	<?php echo $option; ?>
</td>
<?php
		if((($i+1)%4) == 0) echo '</tr><tr>';
	}
}
?>
</tr>
</table>

<p class="intro"><a href="index.php?route=frags/create_frag"><img src="images/png/icons/addf.png" alt="add" border="0" /></a></p>
</div>
