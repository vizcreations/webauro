<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Frags Edit frag View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="fragform" id="fragform" method="post" action="index.php">
<table class="home">
<tr>
<?php 
if(!$fragtypes) {
?>
<td><p class="para red">No frag types defined yet!</p></td>
<?php 
} else {
	for($i=0; $i<count($fragtypes); $i++) {
		$fragtype = $fragtypes[$i];
		if($frag['frag_type_id'] == $fragtype['frag_type_id']) {
			$selected = '<small><img src="images/png/icons/enable.png" alt="" border="0" height="10" /></small>';
			$checked = "checked=\"checked\"";
		} else {
			$selected = '';
			$checked = "";
		}
?>
<td>
	<input type="radio" name="fragtype" value="<?php echo $fragtype['frag_type_name']; ?>" <?php echo $checked; ?> />&nbsp;<?php echo $fragtype['frag_type_name']; ?>&nbsp;
	<?php echo $selected; ?>
</td>
<?php
		if((($i+1) % 4) == 0) echo '</tr><tr>';
	}
?>

<?php
}
?>
</tr>
</table>

<table class="form">
<tr>
	<td>
		<input type="hidden" name="route" value="frags/edit_frag_step2/<?php echo $frag['frag_id']; ?>" />
		<input type="submit" name="editfragstep2" id="editfragstep2" value="Next" />
	</td>
</tr>
</table>
</form>

</div>
