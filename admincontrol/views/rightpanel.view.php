<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Right panel View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

?>

<?php if($registry->isLogged) { ?>
<div class="container">
	<h2>
	<!--<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			Menu
		</td>
		<td style="padding-left: 3px;">
			<img src="images/png/icons/manage.png" alt="welcome" border="0" />
		</td>
	</tr>
	</table>
	-->Menu</h2>
	<p class="intro">Control features</p>
	<ul class="menu">
		<li><a href="index.php">Home</a></li>
		<li><a href="index.php?route=menus">Menus</a>
		<li><a href="index.php?route=articles">Articles</a>
		<li><a href="index.php?route=users">Users</a></li>
		<li><a href="index.php?route=groups">Groups</a></li>
		<li><a href="index.php?route=components">Components</a></li>
		<li><a href="index.php?route=frags">Frags</a></li>
		<li><a href="index.php?route=positions">Positions</a>
		<li><a href="index.php?route=templates">Templates</a></li>
		<li><a href="index.php?route=contact">Contact</a></li>
	</ul>
</div>

<div class="container">
	<p class="intro">Logout!</p>
	<ul class="menu">
		<li><a href="index.php?route=users/logout"><img src="images/png/icons/logout.png" alt="logout" border="0" /></a></li>
	</ul>
</div>
<?php } else { ?>
<div class="container">
	<h2>Links</h2>
	<p class="intro">Navigate</p>
	<ul class="menu">
		<li><a href="../index.php">Website preview</a></li>
		<li><a href="javascript:void(0);">Production website</a></li>
		<li><a href="javascript:(void);">Admin rules</a></li>
		<li><a href="#">Development</a></li>
	</ul>
</div>
<?php } ?>
