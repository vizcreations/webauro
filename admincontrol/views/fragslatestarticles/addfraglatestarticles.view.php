<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol FragsLatestArticles Add frag latest articles View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="fragform" id="fragform" method="post" action="index.php">
<fieldset>
	<legend>Details</legend>
	<table class="form">
	<tr>
		<td>
			<label for="fragname">Frag name</label><br />
			<input type="text" name="fragname" id="fragname" size="37" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="fragtitle">Frag title</label><br />
			<input type="text" name="fragtitle" id="fragtitle" size="37" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="fragpos">Frag position</label><br />
			<?php
			if(!$positions) {
			?>
			<p class="para red">No positions created yet!</p>
			<?php 
			} else {
			?>
			<select name="fragpos" id="fragpos">
			<?php 
				for($p=0; $p<count($positions); $p++) {
					$position = $positions[$p];
			?>
			<option value="<?php echo $position['pos_id']; ?>"><?php echo $position['pos_name']; ?></option>
			<?php 
				}
			?>
			</select>
			<?php 
			}
			?>
		</td>
	</tr>
	<tr>
		<td>
			<label for="fragargument">Category</label><br />
			<select name="fragargument" id="fragargument">
			<?php 
			if(!$categories) {
			?>
			<option value="0">No categories yet!</option>
			<?php 
			} else {
				for($c=0; $c<count($categories); $c++) {
					$category = $categories[$c];
			?>
			<option value="<?php echo $category['ar_category_id']; ?>"><?php echo $category['ar_category_name']; ?></option>
			<?php
				}
			}
			?>
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="frags/add_frag_step3" />
			<input type="hidden" name="fragtypeid" value="<?php echo $fragtypeid; ?>" />
			<input type="hidden" name="fragtype" value="<?php echo $fragtype; ?>" />
			<input type="submit" name="addfrag" id="addfrag" value="Add" />
		</td>
	</tr>
	</table>
</fieldset>
</form>

</div>
