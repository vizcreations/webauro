<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Contact Contact form View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="contactform" id="contactform" method="post" action="index.php">
<fieldset>
	<legend>Contact email</legend>
	<table class="form">
	<tr>
		<td>
			<label for="contactname">Name</label><br />
			<input type="text" name="contactname" id="contactname" size="35" value="<?php echo $contactname; ?>" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="contacttelephone">Telephone</label><br />
			<input type="text" name="contacttelephone" id="contacttelephone" value="<?php echo $contacttelephone; ?>" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<label for="contactaddress">Address</label><br />
			<textarea name="contactaddress" id="contactaddress" rows="3" style="width: 100%;"><?php echo $contactaddress; ?></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<label for="contactemail">Email</label><br />
			<input type="text" name="contactemail" id="contactemail" size="35" value="<?php echo $contactemail; ?>" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="contact/edit_contact" />
			<input type="submit" name="editcontact" id="editcontact" value="Edit" />
		</td>
	</tr>
	</table>
</fieldset>
</form>
</div>
