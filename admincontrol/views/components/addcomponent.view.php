<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Components Add component View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

$sitename = $this->registry->sitename;
$comform_token = uniqid();
$_SESSION[$sitename]['comform_token'] = $comform_token;
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="componentform" id="componentform" action="index.php" method="post">
<fieldset>
<legend>Component details</legend>
<table class="form">
	<tr>
		<td>
			<label for="controller">Controller</label><br />
			<input type="text" name="controller" id="controller" size="35" class="inputtext" />
		</td>
	</tr>
	<tr>
		<td>
			<input type="hidden" name="route" value="components/add_component" />
			<input type="hidden" name="comform_token" value="<?php echo $comform_token; ?>" />
			<input type="submit" name="addcomponent" id="addcomponent" value="Add" />
		</td>
	</tr>
</table>
</fieldset>
</form>

</div>
