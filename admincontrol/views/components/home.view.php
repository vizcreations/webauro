<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Components Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>
<table class="home">
<tr>
<?php 
if($controllers) {
	for($c=0; $c<count($controllers); $c++) {
		$controller = $controllers[$c];
?>
	<td>
		<h5><a href="index.php?route=<?php echo $controller['controller_name']; ?>"><?php echo $controller['controller_name']; ?></a></h5>
		<ul class="list">
		<?php 
			$cmodel = new ComponentsModel();
			try {
				$views = $cmodel->getViews($controller['controller_id']);
			} catch(Exception $e) {
				$views = 0;
			}
			if($views) {
				for($v=0; $v<count($views); $v++) {
					$view = $views[$v];
		?>
			<li><?php echo $view['view_name']; ?></li>
		<?php
				}
			} else {
		?>
			<li class="red">No views yet!</li>
		<?php
			}
		?>
		</ul>
		<p class="intro"><a href="index.php?route=components/add_view/<?php echo $controller['controller_id']; ?>"><img src="images/png/icons/addf.png" alt="add" border="0" /></a></p>
	</td>
<?php
		if((($c+1)%4) == 0) echo '</tr><tr>';
	}
} else {
?>
<tr><td><p class="para red">No controllers created yet!</p></td></tr>
<?php 
}
?>
</tr>
</table>
<p class="para intro"><b><a href="index.php?route=components/add_component">Add component</a></b></p>
</div>
