<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Components Add view View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

$sitename = $this->registry->sitename;
$viewform_token = uniqid();
$_SESSION[$sitename]['viewform_token'] = $viewform_token;
 
?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="viewform" id="viewform" action="index.php" method="post">
<fieldset>
<legend>View details</legend>
<table class="form">
<tr>
	<td>
		<label for="view">View name</label><br />
		<input type="text" name="view" id="view" size="35" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="argument">Argument</label><br />
		<select name="argument" id="argument">
			<option value="0">No</option>
			<option value="1">Yes</option>
		</select>
	</td>
</tr>
<tr>
	<td>
		<label for="argumenttable">Argument table</label><br />
		<input type="text" name="argumenttable" id="argumenttable" size="35" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="argumentid">Argument ID field</label><br />
		<input type="text" name="argumentid" id="argumentid" size="35" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="argumentname">Argument name field</label><br />
		<input type="text" name="argumentname" id="argumentname" size="35" class="inputtext" />
	</td>
</tr>
</table>
<table width="100%" class="form">
<tr>
	<td>
		<input type="hidden" name="route" value="components/add_view/<?php echo $componentid; ?>" />
		<input type="hidden" name="viewform_token" value="<?php echo $viewform_token; ?>" />
		<input type="submit" name="addview" id="addview" value="Add" />
	</td>
</tr>
</table>
</fieldset>
</form>
</div>
