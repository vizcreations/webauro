<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Positions Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="home">
<tr>
<?php 
if(!$positions) {
?>
<td><p class="red">No positions created yet!</p></td>
<?php 
} else {
	for($i=0; $i<count($positions); $i++) {
		$position = $positions[$i];
?>
	<td>
		<h5><?php echo $position['pos_name']; ?></h5>
		<h6><u>Menus</u></h6>
		<ul class="list">
		<?php 
			$pmodel = new PositionsModel();
			try {
				$menus = $pmodel->getPositionMenus($position['pos_id']);
			} catch(Exception $e) {
				$menus = 0;
				echo '<li class="red">'.$e->getMessage().'</li>';
			}
			if(!$menus) {
		?>
		<li class="red">No menus associated.</li>
		<?php 
			} else {
				for($m=0; $m<count($menus); $m++) {
					$menu = $menus[$m];
		?>
		<li><a href="index.php?route=menus/menu/<?php echo $menu['menu_id']; ?>"><?php echo $menu['menu_name']; ?></a></li>
		<?php
				}
			}
		?>
		</ul>
		<h6><u>Frags</u></h6>
		<ul class="list">
		<?php
		try {
			$frags = $pmodel->getPositionFrags($position['pos_id']);
		} catch(Exception $e) {
			$frags = 0;
			echo '<li class="red">'.$e->getMessage().'</li>';
		}
		if(!$frags) {
		?>
		<li class="red">No frags associated..</li>
		<?php 
		} else {
			for($k=0; $k<count($frags); $k++) {
				$frag = $frags[$k];
		?>
		<li><a href="index.php?route=frags/edit_frag/<?php echo $frag['frag_id']; ?>"><?php echo $frag['frag_name']; ?></a></li>
		<?php
			}
		}
		?>
		</ul>
	</td>
<?php
		if((($i+1)%4) == 0) echo '</tr><tr>';
	}
}
?>
</tr>
</table>
</div>
