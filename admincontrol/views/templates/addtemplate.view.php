<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Templates Add template View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
$sitename = $this->registry->sitename;
$templateform_token = uniqid();
$_SESSION[$sitename]['templateform_token'] = $templateform_token;

?>

<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<form name="templateform" id="templateform" action="index.php" method="post">
<fieldset>
<legend>New template details</legend>
<table class="form">
<tr>
	<td>
		<label for="templatename">Template name</label><br />
		<input type="text" name="templatename" id="templatename" size="30" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<label for="templateauthor">Template author</label><br />
		<input type="text" name="templateauthor" id="templateauthor" size="30" class="inputtext" />
	</td>
</tr>
<tr>
	<td>
		<input type="hidden" name="route" value="templates/add_template" />
		<input type="hidden" name="templateform_token" id="templateform_token" value="<?php echo $templateform_token; ?>" />
		<input type="submit" name="addtemplate" id="addtemplate" value="Add" />
	</td>
</tr>
</table>
</fieldset>
</form>

</div>
