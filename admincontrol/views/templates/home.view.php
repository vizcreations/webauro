<div class="container">
<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Templates Home View
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
?>
<script type="text/javascript">
<!--
function delTemplate(tmplid) {
	if(confirm("Are you sure you want to delete this template?")) {
		window.location.href = 'index.php?route=templates/delete_template/' + tmplid;
	}
}
//-->
</script>
<h2><?php echo $title; ?></h2>
<p class="intro"><?php echo isset($message) ? $message : $intro; ?></p>

<table class="homet">
<?php 
if($templates) {
?>
<tr>
	<th>ID</th>
	<th>Template Name</th>
	<th>Default</th>
	<th>Selection</th>
	<th>Delete</th>
</tr>
<?php
	for($t=0; $t<count($templates); $t++) {
		$template = $templates[$t];
		if($selected == $template['template_id']) {
			$selection = true;
			$del = 'Void';
		} else {
			$selection = false;
			$del = '<a href="javascript:void(0);" onclick="javascript:delTemplate('.$template['template_id'].');">Delete</a>';
		}
?>
<tr>
	<td style="width: 5%;"><?php echo $template['template_id']; ?></td>
	<td><?php echo $template['template_name']; ?></td>
	<td><?php if($selection) echo "<small class=\"blue\">Selected</small>"; else echo "<small class=\"red\">No</small>";?></td>
	<td><a href="index.php?route=templates/select_template/<?php echo $template['template_id']; ?>">Select</a></td>
	<td><?php echo $del; ?></td>
</tr>
<?php
	}
?>

<?php 
} else {
?>
<tr><td><p class="para red">No templates created yet!</p></td></tr>
<?php 
}
?>
</table>
<p class="para"><a href="index.php?route=templates/add_template"><img src="images/png/icons/addf.png" alt="add" border="0" /></a></p>
</div>
