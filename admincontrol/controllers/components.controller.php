<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Components Controller to handle large extensions
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ComponentsController extends Controller {
	public function index() {
		$cmodel = new ComponentsModel();
		try {
			$controllers = $cmodel->getComponents();
		} catch(Exception $e) {
			$controllers = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->controllers = $controllers;
		$this->registry->template->title = "Components";
		$this->registry->template->intro = "Add or edit components";
		$this->registry->template->show( 'home' );
	}
	
	public function component($cid) {
		$cmodel = new ComponentsModel();
		try {
			$component = $cmodel->getComponentName($cid);
		} catch(Exception $e) {
			$component = "Error";
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->title = "$component";
		$this->registry->template->intro = "Edit component details";
		$this->registry->template->show( $component );
	}
	
	public function add_component() {
		if(isset($this->httpvars['addcomponent'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['comform_token']) && isset($_SESSION[$sitename]['comform_token'])) {
				if($this->httpvars['comform_token'] == $_SESSION[$sitename]['comform_token']) {
					
					if(!empty($this->httpvars['controller'])) {
						$cmodel = new ComponentsModel();
						try {
							$cmodel->save($this->httpvars, 'components', 'add');
							unset($_SESSION[$sitename]['comform_token']);
							$this->registry->router->redirect( 'index.php?route=components', '' );
							exit();
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}
						unset($_SESSION[$sitename]['comform_token']);
					} else {
						$this->registry->template->message = "Please enter controller name!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry but you cannot submit twice!";
			}
		}
		$this->registry->template->title = "Add component";
		$this->registry->template->intro = "Add new component to website";
		$this->registry->template->show( 'addcomponent' );
	}
	
	public function add_view($componentid) {
		$cmodel = new ComponentsModel();
		if(isset($this->httpvars['addview'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['viewform_token']) && isset($_SESSION[$sitename]['viewform_token'])) {
				if($this->httpvars['viewform_token'] == $_SESSION[$sitename]['viewform_token']) {
					
					if(!empty($this->httpvars['view'])) {
						try {
							$cmodel->save($this->httpvars, 'views', 'add');
							$this->httpvars['controllerid'] = $componentid;
							$cmodel->save($this->httpvars, 'controller_view', 'add');
							$this->httpvars['viewid'] = $cmodel->getInsertId();
							if($this->httpvars['argument'])
								$cmodel->save($this->httpvars, 'views_arguments', 'add');
							
							unset($_SESSION[$sitename]['viewform_token']);
							$this->registry->router->redirect( 'index.php?route=components', '' );
							exit();
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}

						unset($_SESSION[$sitename]['viewform_token']);
					} else {
						$this->registry->template->message = "Please enter view name";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		try {
			$componentname = $cmodel->getComponentName($componentid);
		} catch(Exception $e) {
			$componentname = "Error";
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->componentid = $componentid;
		$this->registry->template->title = "$componentname";
		$this->registry->template->intro = "Add view to component";
		$this->registry->template->show( 'addview' );
	}
}
