<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Layout Background Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class BackgroundController extends Controller {
	public function index() {
		$xml = __ROOT.DS.'images'.DS.'background.xml';
		if(file_exists($xml)) {
			$xmlDoc = new DOMDocument();
			$xmlDoc->load($xml);
			$bgimage = $xmlDoc->getElementsByTagName( 'image' )->item(0)->nodeValue;
			if(file_exists(__ROOT.DS.'images'.DS.$bgimage)) {
				$background = 'images'.DS.$bgimage;
			} else {
				$background = 0;
			}
		} else {
			$background = 0;
		}
		$this->registry->template->background = $background;
		$this->registry->template->title = "Background image";
		$this->registry->template->intro = "Change the default background for the website";
		$this->registry->template->show( 'home' );
	}
	
	public function add_bgimage() {
		if(isset($this->httpvars['changebg'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['bgform_token']) && isset($_SESSION[$sitename]['bgform_token'])) {
				if($this->httpvars['bgform_token'] == $_SESSION[$sitename]['bgform_token']) {
					
					$bgtile = $this->httpvars['tile'];
					if(isset($bgtile) && $bgtile == 'yes') $thetile = '1';
					else $thetile = '0';
					
					if(isset($_FILES['bgimage']['name']) && !empty($_FILES['bgimage']['name'])) {
						$bgname = $_FILES['bgimage']['name'];
						$tmpname = $_FILES['bgimage']['tmp_name'];
						$ext = substr(strrchr($bgname, '.'), 1);
						$randname = md5(rand() * time());
						$uploaddir = __ROOT.DS.'images'.DS;
						$bgname = $randname.'.'.$ext;
						$bgpath = $uploaddir.$bgname;
						move_uploaded_file($tmpname, $bgpath);
						$xmlDoc = new DOMDocument();
						$xmlDoc->formatOutput = true;
						$root = $xmlDoc->createElement('background');
						$xmlDoc->appendChild($root);
						$bg = $xmlDoc->createElement('image');
						$bg->appendChild($xmlDoc->createTextNode($bgname));
						$root->appendChild($bg);
						$tile = $xmlDoc->createElement('tile');
						$tile->appendChild($xmlDoc->createTextNode($thetile));
						$root->appendChild($tile);
						if(file_exists(__ROOT.DS.'images'.DS.'background.xml')) {
							$xmlDoc2 = new DOMDocument();
							$xmlDoc2->load(__ROOT.DS.'images'.DS.'background.xml');
							$bgimage = $xmlDoc2->getElementsByTagName('image')->item(0)->nodeValue;
							if(file_exists(__ROOT.DS.'images'.DS.$bgimage)) unlink(__ROOT.DS.'images'.DS.$bgimage);
							unlink(__ROOT.DS.'images'.DS.'background.xml');
						}
						$xmlDoc->save(__ROOT.DS.'images'.DS.'background.xml');
						$this->registry->template->message = "Background saved successfully!";
					}
					unset($_SESSION[$sitename]['bgform_token']);
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice";
				}
			}
		}
		$this->index();
	}
	
	public function default_background() {
		$xml = __ROOT.DS.'images'.DS.'background.xml';
		if(file_exists($xml)) {
			$xmlDoc = new DOMDocument();
			$xmlDoc->load($xml);
			$image = $xmlDoc->getElementsByTagName('image')->item(0)->nodeValue;
			$imagefile = __ROOT.DS.'images'.DS.$image;
			if(file_exists($imagefile)) unlink($imagefile);
			unlink($xml);
		}
		$this->registry->template->message = "Background changed to default successfully!";
		$this->index();
	}
}
