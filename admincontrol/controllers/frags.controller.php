<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Frags Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class FragsController extends Controller {
	public function index() {
		$fmodel = new FragsModel();
		try {
			$frags = $fmodel->getFrags();
		} catch(Exception $e) {
			$frags = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->frags = $frags;
		$this->registry->template->title = "Frags";
		$this->registry->template->intro = "Add or edit frags";
		$this->registry->template->show( 'home' );
	}
	
	public function create_frag_content() {
		$fmodel = new FragsModel();
		if(isset($this->httpvars['addfrag'])) {
			if(isset($this->httpvars['frform_token']) && isset($_SESSION['frform_token'])) {
				if($this->httpvars['frform_token'] == $_SESSION['frform_token']) {
					// TODO CODE
					$fmodel->save($this->httpvars, 'frag', 'add');
					$this->httpvars['fragid'] = $fmodel->getInsertId();
					$fmodel->save($this->httpvars, 'frag_pos', 'add');
					/*if(!empty($this->httpvars['fragargument']))
						$fmodel->save($this->httpvars, 'frag_argument', 'add');*/
					if(!empty($this->httpvars['fragcontent']))
						$fmodel->save($this->httpvars, 'frag_content', 'add');
					unset($_SESSION['frform_token']);
					$this->registry->router->redirect( 'index.php?route=frags', '' );
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		$pmodel = new PositionsModel();
		try {
			$positions = $pmodel->getPositions();
		} catch(Exception $e) {
			$positions = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->positions = $positions;
		$this->registry->template->title = "Create";
		$this->registry->template->intro = "Add a new frag to website";
		$this->registry->template->show( 'addfrag' );
	}
	
	public function create_frag() {
		$model = new FragsModel();
		try {
			$fragtypes = $model->getFragTypes();
		} catch(Exception $e) {
			$fragtypes = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->fragtypes = $fragtypes;
		$this->registry->template->title = "Add";
		$this->registry->template->intro = "Select Frag type";
		$this->registry->template->show( 'addfrag' );
	}
	
	public function edit_frag($fragid) {
		$model = new FragsModel();
		try {
			$frag = $model->getFrag($fragid);
		} catch(Exception $e) {
			$frag = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$fragtypes = $model->getFragTypes();
		} catch(Exception $e) {
			$fragtypes = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->frag = $frag;
		$this->registry->template->fragtypes = $fragtypes;
		$this->registry->template->title = "Edit {$frag['frag_title']}";
		$this->registry->template->intro = "Choose frag type";
		$this->registry->template->show( 'editfrag' );
	}
	
	public function add_frag_step2() {
		$model = new FragsModel();
		$pmodel = new PositionsModel();
		if(isset($this->httpvars['addfragstep2'])) {
			if(!empty($this->httpvars['fragtype'])) {
				$controllerfile = __CONTROLLERS.DS.'frags'.strtolower($this->httpvars['fragtype']).'.controller.php';
				if(file_exists($controllerfile)) {
					require_once($controllerfile);
					$controllerclass = 'Frags'.$this->httpvars['fragtype'].'Controller';
					$controller = new $controllerclass($this->registry);
					$controller->add_frag_step2();
				} else {
					$this->create_frag();
				}
			} else {
				$this->registry->template->message = "Please select a frag type!";
				$this->create_frag();
			}
		} else {
			$this->create_frag();
		}
	}
	
	public function edit_frag_step2($fragid) {
		$model = new FragsModel();
		$pmodel = new PositionsModel();
		if(isset($this->httpvars['editfragstep2'])) {
			if(!empty($this->httpvars['fragtype'])) {
				$controllerfile = __CONTROLLERS.DS.'frags'.strtolower($this->httpvars['fragtype']).'.controller.php';
				if(file_exists($controllerfile)) {
					require_once($controllerfile);
					$controllerclass = 'Frags'.$this->httpvars['fragtype'].'Controller';
					$controller = new $controllerclass($this->registry);
					$controller->edit_frag_step2($fragid);
				} else {
					$this->edit_frag($fragid);
				}
			} else {
				$this->registry->template->message = "Please select a frag type!";
				$this->edit_frag($fragid);
			}
		} else {
			$this->edit_frag($fragid);
		}
	}
	
	public function add_frag_step3() {
		$fmodel = new FragsModel();
		$pmodel = new PositionsModel();
		$modelname = 'Frags'.$this->httpvars['fragtype'].'Model';
		$typemodel = new $modelname();
		if(isset($this->httpvars['addfrag'])) {
			if(!empty($this->httpvars['fragname'])) {
				try {
					$fmodel->save($this->httpvars, 'frag', 'add');
					$this->httpvars['fragid'] = $fmodel->getInsertId();
					$this->httpvars['fragstatus'] = 1;
					$fmodel->save($this->httpvars, 'frag_status', 'add');
					$fmodel->save($this->httpvars, 'frag_type_rel', 'add');
					$fmodel->save($this->httpvars, 'frag_pos', 'add');
					if(class_exists($modelname)) {
						$typemodel->save($this->httpvars, 'frag_'.$this->httpvars['fragtype'], 'add');
					}
					$this->registry->template->message = "Frag added successfully!";
				} catch(Exception $e) {
					$this->registry->template->message = $e->getMessage();
				}
			} else {
				$this->registry->template->message = "Please fill all details";
			}
		}
		$this->index();
	}
	
	public function edit_frag_step3($fragid) {
		$fmodel = new FragsModel();
		$pmodel = new PositionsModel();
		$modelname = 'Frags'.$this->httpvars['fragtype'].'Model';
		$typemodel = new $modelname();
		if(isset($this->httpvars['editfrag'])) {
			if(!empty($this->httpvars['fragname'])) {
				$this->httpvars['fragid'] = $fragid;
				try {
					$fmodel->save($this->httpvars, 'frag', 'edit');
					$fmodel->save($this->httpvars, 'frag_type_rel', 'edit');
					$fmodel->save($this->httpvars, 'frag_pos', 'edit');
					if(class_exists($modelname)) {
						if(method_exists($typemodel, 'check_exists')) {
							try {
								$exists = $typemodel->check_exists($fragid);
							} catch(Exception $e) {
								$exists = true;
							}
							if($exists)
								$typemodel->save($this->httpvars, 'frag_'.$this->httpvars['fragtype'], 'edit');
							else
								$typemodel->save($this->httpvars, 'frag_'.$this->httpvars['fragtype'], 'add');
						}
					}
					$this->registry->template->message = "Frag saved successfully!";
				} catch(Exception $e) {
					$this->registry->template->message = $e->getMessage();
				}
			} else {
				$this->registry->template->message = "Please fill all details";
			}
		}
		$this->index();
	}
	
	public function enable_frag($fragid) {
		$model = new FragsModel();
		try {
			$model->enableFrag($fragid);
			$this->registry->template->message = "Frag successfully enabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->index();
	}
	
	public function disable_frag($fragid) {
		$model = new FragsModel();
		try {
			$model->disableFrag($fragid);
			$this->registry->template->message = "Frag successfully disabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->index();
	}
};
