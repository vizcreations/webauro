<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Controller to handle Users
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class UsersController extends Controller {
	public function index() {		
            $model = new UsersModel();		
            try {			
                $users = $model->getUsers();		
                
            } catch(Exception $e) {			
                $users = 0;			
                $this->registry->template->message = $e->getMessage();
            }		
            $this->registry->template->users = $users;
            $this->registry->template->title = "Members";
            $this->registry->template->intro = "Welcome to the Users section";
            $this->registry->template->show( 'home' );
	}
	
	public function login() {
		$this->registry->template->title = "Login";
		$this->registry->template->intro = "This is a secure area. Please fill out the following form before proceeding.";
		$this->registry->template->show( 'login' );
	}		
	
	public function deactivate_user($userid) {		
		$model = new UsersModel();		
		try {			
			$model->deactivateUser($userid);			
			$this->registry->template->message = "User successfully deactivated!";		
		} catch(Exception $e) {			
			$this->registry->template->message = $e->getMessage();		
		}		
		$this->index();	
	}		
	
	public function activate_user($userid) {		
		$model = new UsersModel();		
		try {			
			$model->activateUser($userid);			
			$this->registry->template->message = "User activated successfully";		
		} catch(Exception $e) {			
			$this->registry->template->message = $e->getMessage();		
		}		
		$this->index();	
	}		
	
	public function users_roles() {
		if($this->registry->isLogged && $this->registry->adminrole == 'superadmin') {		
			$model = new UsersModel();
			try {
				$usersroles = $model->getUsersRoles();		
			} catch(Exception $e) {			
				$usersroles = 0;			
				$this->registry->template->message = $e->getMessage();		
			}		
			$this->registry->template->usersroles = $usersroles;		
			$this->registry->template->title = "Users Roles";		
			$this->registry->template->intro = "Add or edit User Role";		
			$this->registry->template->show( 'usersroles' );
		} else {
			$this->index();
		}
	}
	
	public function logout() {
		global $app;
		$site = $app['sitename'];
		if(isset($_SESSION[$site.'admin_is_logged_in'])) {
			unset($_SESSION[$site.'admin_is_logged_in']);
			$this->registry->isLogged = false;
		}
		if(isset($_COOKIE[$site.'acookie'])) {
			unset($_COOKIE[$site.'acookie']);	
		}
		$this->registry->router->redirect( 'index.php', '' );
	}
	
	public function user($userid) {
		$model = new UsersModel();
		if($this->registry->isLogged && $this->registry->adminrole != 'user') {
			if(isset($this->httpvars['edituser'])) {
				if(!empty($this->httpvars['username']) && !empty($this->httpvars['useremail']) && isset($this->httpvars['userrole'])) {
					$this->httpvars['userid'] = $userid;
					$message = '';	
					try {
						$model->save($this->httpvars, 'users', 'edit');
						$model->save($this->httpvars, 'users_roles_rel', 'edit');
						if(!empty($this->httpvars['newpassword']) && !empty($this->httpvars['newpassword2'])) {
							if($this->httpvars['newpassword'] == $this->httpvars['newpassword2']) {
								$model->save($this->httpvars, 'users_pass', 'edit');
								try {
									$user = $model->getUser($userid);
									$this->registry->template->user = $user;
								} catch(Exception $e) {
									$this->registry->template->user = array();
									$this->registry->template->message = $e->getMessage();
								}
								$_SESSION['vcadmin_username'] = $user['user_name'];
								$_SESSION['vcadmin_role'] = $user['user_role_name'];
							} else {
								$message = "Password not changed! New passwords don't match! ";
							}
						}
						$message .= "User information saved successfully!";
						$this->registry->template->message = $message;
					} catch(Exception $e) {	
						$this->registry->template->message = $e->getMessage();	
					}			
				} else {				
					$this->registry->template->message = "Please enter all required fields!";			
				}
			}
			try {
				$user = $model->getUser($userid);
				$this->registry->template->user = $user;
			} catch(Exception $e) {
				$this->registry->template->user = array();
				$this->registry->template->message = $e->getMessage();
			}
			try {
				$usersroles = $model->getUsersRoles();		
			} catch(Exception $e) {
				$usersroles = 0;			
				$this->registry->template->message = $e->getMessage();		
			}
			$access = false;
			if($this->registry->adminrole == 'superadmin') $access = true;
			if($this->registry->adminrole == 'admin') $access = true;
			if($user['user_role_name'] == 'superadmin' && $this->registry->adminrole == 'admin') $access = false;
			if($access == true) {
				$this->registry->template->usersroles = $usersroles;
				$this->registry->template->title = $user['user_name'];
				$this->registry->template->intro = "Edit user details";
				$this->registry->template->show( 'user' );
			} else {
				$this->index();
			}
		} else {
			$this->index();
		}
	}
	
	public function add_user() {
		$model = new UsersModel();
		if(isset($this->httpvars['adduser'])) {
			$sitename = $this->registry->sitename;
			if($this->registry->isLogged && $this->registry->adminrole != 'user') {
				if(isset($this->httpvars['userform_token']) && isset($_SESSION[$sitename]['userform_token'])) {
					if($this->httpvars['userform_token'] == $_SESSION[$sitename]['userform_token']) {
						if(!empty($this->httpvars['username']) && !empty($this->httpvars['password']) && !empty($this->httpvars['password2']) && !empty($this->httpvars['email']) && $this->httpvars['userrole'] != '0') {
							if($this->httpvars['password'] == $this->httpvars['password2']) {
								if(!$model->checkExists($this->httpvars['username'])) {
									if(!$model->checkExists($this->httpvars['email'])) {
										try {
											$model->save($this->httpvars, 'users', 'add');
											$userid = $model->getInsertId();
											$this->httpvars['userid'] = $userid;
											$model->save($this->httpvars, 'users_roles_rel', 'add');
											$message = "User successfully saved!";
										} catch(Exception $e) {
											$message = $e->getMessage();
										}
										unset($_SESSION[$sitename]['userform_token']);
									} else {
										$message = "Email already exists! Try another.";
									}
								} else {
									$message = "Username already exists! Try another.";
								}
							} else {
								$message = "User creation failed! Passwords don't match!";
							}
						} else {
							$message = "Please enter all important fields!";
						}
					} else {
						$message = "Sorry, but you cannot submit twice!";
					}
				} else {
					$message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$message = "Unauthorized access!";
			}
			$this->registry->template->message = $message;
		}
		try {
			$roles = $model->getUsersRoles();
		} catch(Exception $e) {
			$roles = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->roles = $roles;
		$this->registry->template->title = "Add User";
		$this->registry->template->intro = "Add new user or admin to site";
		$this->registry->template->show( 'adduser' );
	}
	
	public function delete_user($userid) {
		$model = new UsersModel();
		try {
			$model->deleteUser($userid);
			$this->registry->router->redirect( 'index.php?route=users', '&message=success' );
		} catch(Exception $e) {
			$this->registry->router->redirect( 'index.php?route=users', '&message=mysql' );
		}
	}
	
	public function add_usersrole() {
		$model = new UsersModel();
		if(isset($this->httpvars['addrole'])) {
			$sitename = $this->registry->sitename;
			if($this->registry->isLogged && $this->registry->adminrole == 'superadmin') {
				if(isset($this->httpvars['roleform_token']) && isset($_SESSION[$sitename]['roleform_token'])) {
					if($this->httpvars['roleform_token'] == $_SESSION[$sitename]['roleform_token']) {
	
						if(!empty($this->httpvars['rolename'])) {
	
							try {
								$model->save($this->httpvars, 'users_roles', 'add');
								$this->registry->router->redirect( 'index.php?route=users/users_roles', '' );
	
							} catch(Exception $e) {
								$this->registry->template->message = $e->getMessage();
							}
							unset($_SESSION[$sitename]['roleform_token']);
						} else {
							$this->registry->template->message = "Please enter role name!";
						}
					} else {
						$this->registry->template->message = "Sorry, but you cannot submit twice!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Unauthorized access!";
			}
		}
		$this->registry->template->title = "Add";
		$this->registry->template->intro = "Add a new users role";
		$this->registry->template->show( 'addusersrole' );
	}

	public function visitors() {
		$model = new UsersModel();
		$date = getdate();
		if($date['hours'] < 10) $date['hours'] = '0'.$date['hours'];
		if($date['minutes'] < 10) $date['minutes'] = '0'.$date['minutes'];
		$curhours = $date['hours'];
		$curminutes = $date['minutes'];
		$curwday = $date['mday'];
		$months = array("0" => "0", "January" => "1", "February" => "2", "March" => "3", "April" => "4", "May" => "5",
						"June" => "6", "July" => "7", "August" => "8", "September" => "9", "October" => "10",
						"November" => "11", "December" => "12");
		$curmonth = $months[$date['month']];
		$curyear = $date['year'];
		$curtime = $date['hours'].$date['minutes'];
		$unixtime = $curyear.'-'.$curmonth.'-'.$curwday;
		try {
			$visitors = $model->getVisitors($unixtime);
		} catch(Exception $e) {
			$visitors = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->visitors = $visitors;
		$this->registry->template->title = "Visitors";
		$this->registry->template->intro = "Visitors of this day";
		$this->registry->template->show( 'visitors' );
	}
}
