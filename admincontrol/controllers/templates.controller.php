<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Templates Controller to assign templates
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class TemplatesController extends Controller {
	public function index() {
		$tmodel = new TemplatesModel();
		try {
			$templates = $tmodel->getWebTemplates();
		} catch(Exception $e) {
			$templates = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$xmlDoc = new DOMDocument();
		$xmlfile = __ROOT.DS.'webTemplate.xml';
		if(file_exists($xmlfile)) {
			$xmlDoc = new DOMDocument();
			$xmlDoc->load( __ROOT.DS.'webTemplate.xml' );
			$selected = $xmlDoc->getElementsByTagName('selected')->item(0)->nodeValue;
		} else {
			$selected = 1;
		}
		$this->registry->template->selected = $selected;
		$this->registry->template->templates = $templates;
		$this->registry->template->title = "Templates";
		$this->registry->template->intro = "Select the template for your website";
		$this->registry->template->show( 'home' );
	}
	
	public function add_template() {
		if(isset($this->httpvars['addtemplate'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['templateform_token']) && isset($_SESSION[$sitename]['templateform_token'])) {
				if($this->httpvars['templateform_token'] == $_SESSION[$sitename]['templateform_token']) {
					$tmodel = new TemplatesModel();
					if(!empty($this->httpvars['templatename']) && !empty($this->httpvars['templateauthor'])) {
						try {
							$exists = $tmodel->checkExists($this->httpvars['templatename']);
							if($exists) {
								$this->registry->template->message = "Template name already exists!";
							} else {
								try {
									$tmodel->save($this->httpvars, 'templates', 'add');
									$this->registry->router->redirect( 'index.php?route=templates', '' );
								} catch(Exception $e) {
									$this->registry->template->message = $e->getMessage();
								}
								unset($_SESSION[$sitename]['templateform_token']);
							}
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}
					} else {
						$this->registry->template->message = "Please enter all fields!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		$this->registry->template->title = "Add template";
		$this->registry->template->intro = "Add new templates to your website";
		$this->registry->template->show( 'addtemplate' );
	}
	
	public function delete_template($templateid) {
		// TODO CODE
		$tmodel = new TemplatesModel();
		try {
			$templatename = $tmodel->getTemplateName($templateid);
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$tmodel->deleteTemplate($templateid);
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		// if(is_dir( __AROOT.DS.'templates'.DS.$templatename )) unlink( __AROOT.DS.'templates'.DS.$templatename );
		$this->index();
	}
	
	public function select_template($templateid) {
		$xmlDoc = new DOMDocument();
		$xmlDoc->formatOutput = true;
		$root = $xmlDoc->createElement('webTemplate');
		$xmlDoc->appendChild($root);
		$selected = $xmlDoc->createElement( 'selected' );
		$tmpid = $xmlDoc->createTextNode($templateid);
		$selected->appendChild($tmpid);
		$root->appendChild($selected);
		//if(file_exists( __ROOT.DS.'webTemplate.xml') ) unlink( __ROOT.DS.'webTemplate.xml' );

		/**
		* XML file simply gets truncated and written
		*/
		$file = __ROOT.DS."webTemplate.xml";
		if(is_writable($file))
			$xmlDoc->save( __ROOT.DS.'webTemplate.xml' );
		else //throw new Exception("Action failed: Template descriptor '$file' unwritable!");
			$this->registry->template->message = "Action failed: Template descriptor '$file' unwritable!";
		$this->index();
	}
};
