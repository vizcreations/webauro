<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ArticlesController extends Controller {
	private $page = 1;
	private $rowsperpage = 6;
	
	public function index() {
		$amodel = new ArticlesModel();
		$offset = ($this->page - 1) * $this->rowsperpage;
		try {
			$articles = $amodel->getArticles($offset, $this->rowsperpage);
			$total = $amodel->getArticlesCount();
			$maxpage = ceil($total / $this->rowsperpage);
		} catch(Exception $e) {
			$articles = 0;
			$total = 0;
			$maxpage = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->articles = $articles;
		$this->registry->template->page = $this->page;
		$this->registry->template->maxpage = $maxpage;
		$this->registry->template->title = "Articles";
		$this->registry->template->intro = "Content for the website";
		$this->registry->template->show( 'home' );
	}
	
	public function page_articles($page) {
		$this->page = $page;
		$amodel = new ArticlesModel();
		$offset = ($this->page - 1) * $this->rowsperpage;
		try {
			$articles = $amodel->getArticles($offset, $this->rowsperpage);
			$total = $amodel->getArticlesCount();
			$maxpage = ceil($total / $this->rowsperpage);
		} catch(Exception $e) {
			$articles = 0;
			$total = 0;
			$maxpage = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->articles = $articles;
		$this->registry->template->page = $page;
		$this->registry->template->maxpage = $maxpage;
		$this->registry->template->title = "Articles";
		$this->registry->template->intro = "Content for the website";
		$this->registry->template->show( 'home' );
	}
	
	public function add_article() {
		$amodel = new ArticlesModel();
		if(isset($this->httpvars['addarticle'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['arform_token']) && isset($_SESSION[$sitename]['arform_token'])) {
				if($this->httpvars['arform_token'] == $_SESSION[$sitename]['arform_token']) {
					if(!empty($this->httpvars['articletitle']) && !empty($this->httpvars['articleauthor']) && !empty($this->httpvars['articlecontent'])) {
						/*$this->httpvars['articlecontent'] = str_replace("<", "&lt;", $this->httpvars['articlecontent']);
						$this->httpvars['articlecontent'] = str_replace(">", "&gt;", $this->httpvars['articlecontent']);*/
						try {
							$amodel->save($this->httpvars, 'articles', 'add');
							$this->httpvars['articleid'] = $amodel->getInsertId();
							$this->httpvars['articlestatus'] = 1;
							$amodel->save($this->httpvars, 'articles_status', 'add');
							$amodel->save($this->httpvars, 'ar_category_rel', 'add');
							$seomodel = new SEOModel();
							$seomodel->save($this->httpvars, 'seo', 'add');
							$this->httpvars['seoid'] = $seomodel->getInsertId();
							$amodel->save($this->httpvars, 'article_seo', 'add');
							unset($_SESSION[$sitename]['arform_token']);
							$this->registry->router->redirect( 'index.php?route=articles', '' );
							exit();
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}
						unset($_SESSION[$sitename]['arform_token']);
					} else {
						$this->registry->template->message = "Please enter all fields!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		try {
			$categories = $amodel->getArticleCategories();
		} catch(Exception $e) {
			$categories = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->categories = $categories;
		$this->registry->template->title = "Add Article";
		$this->registry->template->intro = "Add new article to website";
		$this->registry->template->show( 'addarticle' );
	}
	
	public function edit_article($articleid) {
		$amodel = new ArticlesModel();
		if(isset($this->httpvars['editarticle'])) {
			if(isset($this->httpvars['arform_token']) && isset($_SESSION['arform_token'])) {
				if($this->httpvars['arform_token'] == $_SESSION['arform_token']) {
					if(!empty($this->httpvars['articletitle']) && !empty($this->httpvars['articleauthor']) && !empty($this->httpvars['articlecontent'])) {
						/*$this->httpvars['articlecontent'] = str_replace("<", "&lt;", $this->httpvars['articlecontent']);
						$this->httpvars['articlecontent'] = str_replace(">", "&gt;", $this->httpvars['articlecontent']);*/
						$this->httpvars['articleid'] = $articleid;
						try {
							$amodel->save($this->httpvars, 'articles', 'edit');
							try {
								$seomodel = new SEOModel();
								$amodel->save($this->httpvars, 'ar_category_rel', 'edit');
								if(empty($this->httpvars['seotitle']))
									$seomodel->save($this->httpvars, 'seo', 'add');
								else
									$seomodel->save($this->httpvars, 'seo', 'edit');
								unset($_SESSION['arform_token']);
								$this->registry->template->message = "Article saved successfully!";
							} catch(Exception $e) {
								$this->registry->template->message = $e->getMessage();
							}
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}
					} else {
						$this->registry->template->message = "Please enter all fields!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		try {
			$article = $amodel->getArticle($articleid);
		} catch(Exception $e) {
			$article = array("article_id" => 0,
								"article_title" => "Error",
								"article_author" => "Error",
								"article_content" => "Error");
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$categories = $amodel->getArticleCategories();
		} catch(Exception $e) {
			$categories = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$seo = $amodel->getArticleSEO($articleid);
		} catch(Exception $e) {
			$seo = array('seo_title' => 'empty', 'seo_keywords' => 'empty', 'seo_description' => 'empty');
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->seo = $seo;
		$this->registry->template->categories = $categories;
		$this->registry->template->article = $article;
		$this->registry->template->title = $article['article_title'];
		$this->registry->template->intro = "Edit article";
		$this->registry->template->show( 'editarticle' );
	}
	
	public function article_comments($id) {
		$model = new ArticlesModel();
		try {
			$article = $model->getArticle($id);
		} catch(Exception $e) {
			$article = array("article_title" => "Error");
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$comments = $model->getArticleComments($id);
		} catch(Exception $e) {
			$comments = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->article = $article;
		$this->registry->template->comments = $comments;
		$this->registry->template->title = "'{$article['article_title']}'".' comments';
		$this->registry->template->intro = "Comments for this article";
		$this->registry->template->show( 'articlecomments' );
	}
	
	public function remove_comment($cid) {
		$model = new ArticlesModel();
		$aid = $this->httpvars['aid'];
		try {
			$model->removeComment($cid);
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->article_comments($aid);
	}
	
	public function categories() {
		$amodel = new ArticlesModel();
		try {
			$categories = $amodel->getArticleCategories();
		} catch(Exception $e) {
			$categories = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->categories = $categories;
		$this->registry->template->title = "Article categories";
		$this->registry->template->intro = "Add or edit categories for articles";
		$this->registry->template->show( 'categories' );
	}
	
	public function add_category() {
		if(isset($this->httpvars['addcategory'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['arcatform_token']) && isset($_SESSION[$sitename]['arcatform_token'])) {
				if($this->httpvars['arcatform_token'] == $_SESSION[$sitename]['arcatform_token']) {
					if(!empty($this->httpvars['categoryname']) && !empty($this->httpvars['categorydesc'])) {
						// TODO CODE
						$amodel = new ArticlesModel();
						try {
							$amodel->save($this->httpvars, 'ar_category', 'add');
							unset($_SESSION[$sitename]['arcatform_token']);
							$this->registry->router->redirect( 'index.php?route=articles/categories', '' );
							exit();
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}
						unset($_SESSION[$sitename]['arcatform_token']);
					} else {
						$this->registry->template->message = "Please enter all fields!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		$this->registry->template->title = "Add Category";
		$this->registry->template->intro = "Add new category for articles";
		$this->registry->template->show( 'addcategory' );
	}
	
	public function edit_category($catid) {
		$model = new ArticlesModel();
		if(isset($this->httpvars['editcategory'])) {
			if(!empty($this->httpvars['categoryname'])) {
				$this->httpvars['categoryid'] = $catid;
				try {
					$model->save($this->httpvars, 'ar_category', 'edit');
					$message = "Category successfully edited!";
				} catch(Exception $e) {
					$message = $e->getMessage();
				}
				$this->registry->template->message = $message;
			} else {
				$this->registry->template->message = "Please enter category name!";
			}
		}
		try {
			$category = $model->getCategory($catid);
		} catch(Exception $e) {
			$category = array("ar_category_name" => "error", "ar_category_description" => "error");
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->category = $category;
		$this->registry->template->title = $category['ar_category_name'];
		$this->registry->template->intro = "Edit category";
		$this->registry->template->show( 'editcategory' );
	}
	
	public function enable_article($id) {
		$model = new ArticlesModel();
		$page = isset($this->httpvars['page']) ? $this->httpvars['page'] : 1;
		try {
			$model->enableArticle($id);
			$this->registry->template->message = "Article successfully enabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		//$this->index();
		$this->page_articles($page);
	}
	
	public function disable_article($id) {
		$model = new ArticlesModel();
		$page = isset($this->httpvars['page']) ? $this->httpvars['page'] : 1;
		try {
			$model->disableArticle($id);
			$this->registry->template->message = "Article successfully disabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		//$this->index();
		$this->page_articles($page);
	}
	
	public function enable_c_status($id) {
		$model = new ArticlesModel();
		$page = $this->httpvars['page'];
		try {
			$model->enableCStatus($id);
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->page_articles($page);
	}
	
	public function disable_c_status($id) {
		$model = new ArticlesModel();
		$page = $this->httpvars['page'];
		try {
			$model->disableCStatus($id);
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->page_articles($page);
	}
};
