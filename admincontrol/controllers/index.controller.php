<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Index Controller for entry point
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class definition for Index Controller */

class IndexController extends Controller {
	
	public function index() {
		if($this->registry->isLogged) {
			$amodel = new ArticlesModel();
			$fmodel = new FragsModel();
			$cmodel = new ComponentsModel();
			$mmodel = new MenusModel();
			try {
				$articles = $amodel->getHomeArticles();
			} catch(Exception $e) {
				$articles = 0;
				$this->registry->template->message = $e->getMessage();
			}
			try {
				$frags = $fmodel->getHomeFrags();
			} catch(Exception $e) {
				$frags = 0;
				$this->registry->template->message = $e->getMessage();
			}
			try {
				$components = $cmodel->getHomeComponents();
			} catch(Exception $e) {
				$components = 0;
				$this->registry->template->message = $e->getMessage();
			}
			try {
				$menus = $mmodel->getHomeMenus();
			} catch(Exception $e) {
				$menus = 0;
				$this->registry->template->message = $e->getMessage();
			}
			$this->registry->template->articles = $articles;
			$this->registry->template->frags = $frags;
			$this->registry->template->components = $components;
			$this->registry->template->menus = $menus;
			$this->registry->template->heading = "Welcome to Webauro!";
			$this->registry->template->intro = "Hi ".$this->registry->adminrole."! Administer away..";
			$this->registry->template->show( 'home' );
		} else {
			$this->registry->template->title = "Login";
			$this->registry->template->intro = "This is a secure area. Please fill out the following form before proceeding.";
			$this->registry->template->show( 'login' );
		}
	}
	
	public function do_login() {
		$model = new UsersModel();
		if(isset($this->httpvars['login']) || isset($this->httpvars['login_x'])) {
			if(isset($this->httpvars['admform_token']) && isset($_SESSION['admform_token'])) {
				// TODO CODE
				if($this->httpvars['admform_token'] == $_SESSION['admform_token']) {
					// TODO CODE
					try {
						$exists = $model->check_login($this->httpvars['ausername'], $this->httpvars['apassword']);
						if($exists) {
							try {
								$user = $model->getUser($this->httpvars['ausername']);
							} catch(Exception $e) {
								$user = 0;
								$this->registry->template->message = $e->getMessage();
							}
							global $app;
							$site = $app['sitename'];
							$_SESSION[$site.'admin_is_logged_in'] = true;
							$_SESSION[$site.'admin_username'] = $user['user_name'];
							$_SESSION[$site.'admin_role'] = $user['user_role_name'];
							$this->registry->isLogged = true;
							$this->registry->router->redirect( 'index.php', '' );
							unset($_SESSION['admform_token']);
						} else {
							$this->registry->template->title = "Login";
							$this->registry->template->message = "ACCESS DENIED!";
							$this->registry->template->show( 'login' );
						}
					} catch(Exception $e) {
						$this->registry->template->title = "Error";
						$this->registry->template->message = "Technical problem during database transaction! ";
						$this->registry->template->show( 'login' );
					}
				} else {
					$this->registry->template->title = "Login";
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
					$this->registry->template->show( 'login' );
				}
			} else {
				$this->registry->router->redirect( 'index.php', '' );
			}
		} else {
			$this->registry->router->redirect( 'index.php', '' );
		}
	}
	
};
