<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Ajax Controller to handle AJAX requests
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */ 
class AJAXController extends Controller {
	public function index() {
		$this->registry->router->redirect( "index.php" );
	}
	
	public function get_galleries($cityid) {
		$model = new ImageGalleryModel();
		try {
			$galleries = $model->getGalleries($cityid);
			$this->registry->template->galleries = $galleries;
		} catch(Exception $e) {
			$this->registry->template->galleries = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->show( 'galleries' );
	}
	
	public function get_controller_views($controllerid) {
		$cmodel = new ComponentsModel();
		try {
			$views = $cmodel->getViews($controllerid);
		} catch(Exception $e) {
			$views = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->views = $views;
		$this->registry->template->show( 'views' );
	}
	
	public function get_view_arguments($viewid) {
		$cmodel = new ComponentsModel();
		try {
			$argument = $cmodel->checkArgument($viewid);
		} catch(Exception $e) {
			$argument = 0;
			$this->registry->template->message = $e->getMessage();
		}
		if(!$argument) {
			$arguments = 0;
		}
		else {
			try {
				$args = $cmodel->getArguments($viewid);
				$arguments = $cmodel->getArgsList($args);
			} catch(Exception $e) {
				$args = array("argument_table" => "error", "argument_name" => "error");
				$arguments = 0;
				$this->registry->template->message = $e->getMessage();
			}
		}
		$this->registry->template->arguments = $arguments;
		$this->registry->template->show( 'arguments' );
	}
}
