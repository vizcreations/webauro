<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Positions Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class PositionsController extends Controller {
	public function index() {
		$pmodel = new PositionsModel();
		try {
			$positions = $pmodel->getPositions();
		} catch(Exception $e) {
			$positions = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->positions = $positions;
		$this->registry->template->title = "Positions";
		$this->registry->template->intro = "Position your menu or frag";
		$this->registry->template->show( 'home' );
	}
}
