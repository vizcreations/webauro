<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Frags Menu Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class FragsMenuController extends Controller {
	public function index() {
		// TODO CODE
	}
	
	public function add_frag_step2() {
		$model = new FragsModel();
		$mmodel = new MenusModel();
		$pmodel = new PositionsModel();
		try {
			$menus = $mmodel->getMenus();
		} catch(Exception $e) {
			$categories = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$positions = $pmodel->getPositions();
		} catch(Exception $e) {
			$positions = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$fragtypeid = $model->getFragTypeId('menu');
		} catch(Exception $e) {
			$fragtypeid = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->menus = $menus;
		$this->registry->template->positions = $positions;
		$this->registry->template->fragtypeid = $fragtypeid;
		$this->registry->template->fragtype = 'argument';
		$this->registry->router->controller = 'FragsMenu';
		$this->registry->template->title = "Add";
		$this->registry->template->intro = "Add Menu Frag";
		$this->registry->template->show( 'addfragmenu' );
	}
	
	public function edit_frag_step2($fragid) {
		$model = new FragsModel();
		$mmodel = new MenusModel();
		$pmodel = new PositionsModel();
		try {
			$frag = $model->getFrag($fragid);
		} catch(Exception $e) {
			$frag = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$menus = $mmodel->getMenus();
		} catch(Exception $e) {
			$categories = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$positions = $pmodel->getPositions();
		} catch(Exception $e) {
			$positions = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$arg = $model->getFragArgument($fragid);
		} catch(Exception $e) {
			$arg = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$fragtypeid = $model->getFragTypeId('menu');
		} catch(Exception $e) {
			$fragtypeid = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->frag = $frag;
		$this->registry->template->menus = $menus;
		$this->registry->template->positions = $positions;
		$this->registry->template->argument = $arg['argument_value'];
		$this->registry->template->fragtypeid = $fragtypeid;
		$this->registry->template->fragtype = 'argument';
		$this->registry->router->controller = 'FragsMenu';
		$this->registry->template->title = "{$frag['frag_name']}";
		$this->registry->template->intro = "Edit frag details";
		$this->registry->template->show( 'editfragmenu' );
	}
}
