<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Menus Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class MenusController extends Controller {
	public function index() {
		$mmodel = new MenusModel();
		try {
			$menus = $mmodel->getMenus();
		} catch(Exception $e) {
			$menus = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->menus = $menus;
		$this->registry->template->title = "Menus";
		$this->registry->template->intro = "Manage Menus";
		$this->registry->template->show( 'home' );
	}
	
	public function add_menu() {
		if(isset($this->httpvars['addmenu'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['menuform_token']) && isset($_SESSION[$sitename]['menuform_token'])) {
				if($this->httpvars['menuform_token'] == $_SESSION[$sitename]['menuform_token']) {
					if(!empty($this->httpvars['menuname'])) {
						try {
							$mmodel = new MenusModel();
							$mmodel->save($this->httpvars, 'menu', 'add');
							$this->httpvars['menuid'] = $mmodel->getInsertId();
							$this->httpvars['menustatus'] = 1;
							$mmodel->save($this->httpvars, 'menu_pos', 'add');
							$mmodel->save($this->httpvars, 'menu_status', 'add');
							unset($_SESSION[$sitename]['menuform_token']);
							$this->registry->router->__redirect( 'index.php', '?route=menus' );
							exit();
						} catch(Exception $e) {
							$this->registry->template->message = $e->getMessage();
						}
					} else {
						$this->registry->template->message = "Please enter menu name";
					}
					unset($_SESSION[$sitename]['menuform_token']);
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		$pmodel = new PositionsModel();
		try {
			$positions = $pmodel->getPositions();
		} catch(Exception $e) {
			$positions = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->positions = $positions;
		$this->registry->template->title = "Add";
		$this->registry->template->intro = "Add new Menu";
		$this->registry->template->show( 'addmenu' );
	}
	
	public function edit_menu($menuid) {
		$mmodel = new MenusModel();
		if(isset($this->httpvars['editmenu'])) {
			if(!empty($this->httpvars['menuname'])) {
				$this->httpvars['menuid'] = $menuid;
				try {
					$mmodel->save($this->httpvars, 'menu', 'edit');
					$mmodel->save($this->httpvars, 'menu_pos', 'edit');
					$this->registry->template->message = "Menu details saved successfully!";
				} catch(Exception $e) {
					$this->registry->template->message = $e->getMessage();
				}
			} else {
				$this->registry->template->message = "Please enter menu name";
			}
		}
		$pmodel = new PositionsModel();
		try {
			$menu = $mmodel->getMenu($menuid);
		} catch(Exception $e) {
			$menu = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$positions = $pmodel->getPositions();
		} catch(Exception $e) {
			$positions = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->positions = $positions;
		$this->registry->template->menu = $menu;
		$this->registry->template->title = "Edit '{$menu['menu_name']}'";
		$this->registry->template->intro = "Edit menu details";
		$this->registry->template->show( 'editmenu' );
	}
	
	public function add_menuitem($menuid) {
		$mmodel = new MenusModel();
		$cmodel = new ComponentsModel();
		$tmodel = new TemplatesModel();
		try {
			$controllers = $cmodel->getComponents();
		} catch(Exception $e) {
			$controllers = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$views = $cmodel->getViews($controllers[0]['controller_id']);
		} catch(Exception $e) {
			$views = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$menu = $mmodel->getMenu($menuid);
		} catch(Exception $e) {
			$menu = array("menu_id" => 0, "menu_name" => "error");
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->controllers = $controllers;
		$this->registry->template->views = $views;
		$this->registry->template->step = false;
		$this->registry->template->menu = $menu;
		$this->registry->template->title = $menu['menu_name'];
		$this->registry->template->intro = "Add menuitem for menu";
		$this->registry->template->show( 'addmenuitem' );
	}
	
	public function add_menuitem_step2($menuid) {
		if(isset($this->httpvars['step2'])) {
			if(!empty($this->httpvars['menuitemname'])) {
				$mmodel = new MenusModel();
				$tmodel = new TemplatesModel();
				$cmodel = new ComponentsModel();
				$this->registry->template->step = true;
				$this->httpvars['menuid'] = $menuid;
				$this->registry->template->args = $this->httpvars;
				$mmodel = new MenusModel();
				try {
					$menu = $mmodel->getMenu($menuid);
				} catch(Exception $e) {
					$menu = array("menu_id" => 0, "menu_name" => "error");
					$this->registry->template->message = $e->getMessage();
				}
				try {
					$templates = $tmodel->getWebTemplates();
				} catch(Exception $e) {
					$templates = 0;
					$this->registry->template->message = $e->getMessage();
				}
				$viewid = $this->httpvars['menuitemview'];
				try {
					$argument = $cmodel->checkArgument($viewid);
				} catch(Exception $e) {
					$argument = 0;
					$this->registry->template->message = $e->getMessage();
				}
				if(!$argument) {
					$arguments = 0;
				} else {
					try {
						$args = $cmodel->getArguments($viewid);
						$arguments = $cmodel->getArgsList($args);
					} catch(Exception $e) {
						$args = array("argument_table" => "error", "argument_id" => "error", "argument_name" => "error");
						$arguments = 0;
						$this->registry->template->message = $e->getMessage();
					}
				}						
				try {				
					$menuitems = $mmodel->getMenuitems($menuid);			
				} catch(Exception $e) {				
					$menuitems = 0;				
					$this->registry->template->message = $e->getMessage();			
				}			
				$this->registry->template->menuitems = $menuitems;
				$this->registry->template->arguments = $arguments;
				$this->registry->template->templates = $templates;
				$this->registry->template->title = $menu['menu_name'];
				$this->registry->template->intro = "Step 2";
				$this->registry->template->show( 'addmenuitem' );
			} else {
				$this->registry->template->message = "Please enter menuitem name";
				$this->add_menuitem($menuid);
			}
		} else {
			$this->add_menuitem($menuid);
		}
	}
	
	public function add_menuitem_step3($menuid) {
		if(isset($this->httpvars['addmenuitem'])) {
			$mmodel = new MenusModel();
			$cmodel = new ComponentsModel();
			$this->httpvars['menuid'] = $menuid;
			$this->httpvars['menuitemargument'] = isset($this->httpvars['menuitemargument']) ? $this->httpvars['menuitemargument'] : "";
			try {
				$this->httpvars['menuitemcontrollername'] = $cmodel->getControllerName($this->httpvars['menuitemcontroller']);
				$this->httpvars['menuitemviewname'] = $cmodel->getViewName($this->httpvars['menuitemview']);
			} catch(Exception $e) {
				$this->httpvars['menuitemcontrollername'] = 'index';
				$this->httpvars['menuitemviewname'] = 'index';
				$this->registry->template->message = $e->getMessage();
			}
			try {
				$mmodel->save($this->httpvars, 'menuitem', 'add');
				$this->httpvars['menuitemid'] = $mmodel->getInsertId();
				$this->httpvars['menuitemstatus'] = 1;
				$mmodel->save($this->httpvars, 'menuitem_status', 'add');
				$mmodel->save($this->httpvars, 'menuitem_menu', 'add');
				$mmodel->save($this->httpvars, 'menuitem_view', 'add');
				$mmodel->save($this->httpvars, 'menuitem_template', 'add');				
				$mmodel->save($this->httpvars, 'menuitem_parent', 'add');
			} catch(Exception $e) {
				$this->registry->template->message = $e->getMessage();
			}
		}
		$this->index();
	}
	
	public function edit_menuitem($id) {
		$mmodel = new MenusModel();
		$cmodel = new ComponentsModel();
		$tmodel = new TemplatesModel();
		try {
			$menuitem = $mmodel->getMenuitem($id);
		} catch(Exception $e) {
			$menuitem = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$template = $mmodel->getMenuitemTemplate($id);
		} catch(Exception $e) {
			$template = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$controllers = $cmodel->getComponents();
		} catch(Exception $e) {
			$controllers = 0;
			$this->registry->template->message = $e->getMessage();
		}
		try {
			$views = $cmodel->getViews($menuitem['controller_id']);
		} catch(Exception $e) {
			$views = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->menuitem = $menuitem;
		$this->registry->template->template = $template;
		$this->registry->template->controllers = $controllers;
		$this->registry->template->views = $views;
		$this->registry->template->step = false;
		$this->registry->template->title = $menuitem['menuitem_name'];
		$this->registry->template->intro = "Edit menuitem details";
		$this->registry->template->show( 'editmenuitem' );
	}
	
	public function edit_menuitem_step2($id) {
		if(isset($this->httpvars['step2'])) {
			if(!empty($this->httpvars['menuitemname'])) {
				$mmodel = new MenusModel();
				$tmodel = new TemplatesModel();
				$cmodel = new ComponentsModel();
				$this->registry->template->step = true;
				$this->httpvars['menuitemid'] = $id;
				$this->registry->template->args = $this->httpvars;
				$mmodel = new MenusModel();
				try {
					$menuitem = $mmodel->getMenuitem($id);
				} catch(Exception $e) {
					$menuitem = 0;
					$this->registry->template->message = $e->getMessage();
				}
				try {
					$template = $mmodel->getMenuitemTemplate($id);
				} catch(Exception $e) {
					$template = 0;
					$this->registry->template->message = $e->getMessage();
				}
				try {
					$templates = $tmodel->getWebTemplates();
				} catch(Exception $e) {
					$templates = 0;
					$this->registry->template->message = $e->getMessage();
				}
				$viewid = $this->httpvars['menuitemview'];
				try {
					$argument = $cmodel->checkArgument($viewid);
				} catch(Exception $e) {
					$argument = 0;
					$this->registry->template->message = $e->getMessage();
				}
				$selectedarg = "";
				if(!$argument) {
					$arguments = 0;
				}
				else {
					try {
						$selectedarg = $cmodel->getSelectedArg($viewid, $menuitem['menuitem_id']);
					} catch(Exception $e) {
						$selectedarg = "";
						$this->registry->template->message = $e->getMessage();
					}
					try {
						$args = $cmodel->getArguments($viewid);
						$arguments = $cmodel->getArgsList($args);
					} catch(Exception $e) {
						$args = array("argument_table" => "error", "argument_id" => "error", "argument_name" => "error");
						$arguments = 0;
						$this->registry->template->message = $e->getMessage();
					}
				}
				$this->registry->template->arguments = $arguments;
				if(!$template) {
					$template['template_id'] = 0;
					$template['template_name'] = 'vizcreations';
				}						try {				$menuitems = $mmodel->getMenuitems($menuitem['menu_id']);			} catch(Exception $e) {				$menuitems = 0;				$this->registry->template->message = $e->getMessage();			}			$this->registry->template->menuitem = $menuitem;			$this->registry->template->menuitems = $menuitems;
				$this->registry->template->selectedarg = $selectedarg;
				$this->registry->template->template = $template;
				$this->registry->template->templates = $templates;
				$this->registry->template->title = $menuitem['menuitem_name'];
				$this->registry->template->intro = "Step 2";
				$this->registry->template->show( 'editmenuitem' );
			} else {
				$this->registry->template->message = "Please fill menuitem field properly";
				$this->edit_menuitem($id);
			}
		} else {
			$this->edit_menuitem($id);
		}
	}
	
	public function edit_menuitem_step3($id) {
		if(isset($this->httpvars['editmenuitem'])) {
			$mmodel = new MenusModel();
			$cmodel = new ComponentsModel();
			$this->httpvars['menuitemid'] = $id;
			$this->httpvars['menuitemargument'] = isset($this->httpvars['menuitemargument']) ? $this->httpvars['menuitemargument'] : "";
			try {
				$this->httpvars['menuitemcontrollername'] = $cmodel->getControllerName($this->httpvars['menuitemcontroller']);
				$this->httpvars['menuitemviewname'] = $cmodel->getViewName($this->httpvars['menuitemview']);
			} catch(Exception $e) {
				$this->httpvars['menuitemcontrollername'] = 'index';
				$this->httpvars['menuitemviewname'] = 'index';
				$this->registry->template->message = $e->getMessage();
			}
			try {
				$mmodel->save($this->httpvars, 'menuitem', 'edit');
				$mmodel->save($this->httpvars, 'menuitem_view', 'edit');
				$mmodel->save($this->httpvars, 'menuitem_template', 'edit');				$mmodel->save($this->httpvars, 'menuitem_parent', 'edit');
			} catch(Exception $e) {
				$this->registry->template->message = $e->getMessage();
			}
		}
		$this->index();
	}
	
	public function enable_menu($menuid) {
		$model = new MenusModel();
		try {
			$model->enableMenu($menuid);
			$this->registry->template->message = "Menu successfully enabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->index();
	}
	
	public function disable_menu($menuid) {
		$model = new MenusModel();
		try {
			$model->disableMenu($menuid);
			$this->registry->template->message = "Menu successfully disabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->index();
	}
	
	public function enable_menuitem($menuitemid) {
		$model = new MenusModel();
		try {
			$model->enableMenuitem($menuitemid);
			$this->registry->template->message = "Menuitem successfully enabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->index();
	}
	
	public function disable_menuitem($menuitemid) {
		$model = new MenusModel();
		try {
			$model->disableMenuitem($menuitemid);
			$this->registry->template->message = "Menuitem successfully disabled!";
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->index();
	}
};
