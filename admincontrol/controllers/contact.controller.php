<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Contact Controller
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ContactController extends Controller {
	public function index() {
		$xmlDoc = new DOMDocument();
		$xmlFile = __ROOT.DS.'contact.xml';
		if(file_exists($xmlFile)) {
			$xmlDoc->load($xmlFile);
			$contactname = $xmlDoc->getElementsByTagName('name')->item(0)->nodeValue;
			$contacttelephone = $xmlDoc->getElementsByTagName('telephone')->item(0)->nodeValue;
			$contactaddress = $xmlDoc->getElementsByTagName('address')->item(0)->nodeValue;
			$contactemail = $xmlDoc->getElementsByTagName('email')->item(0)->nodeValue;
		} else {
			$contactname = "Vijay Kanta";
			$contacttelephone = "9966401737";
			$contactaddress = "Hyderabad";
			$contactemail = 'viju.kantah@gmail.com';
		}
		$this->registry->template->contactname = $contactname;
		$this->registry->template->contacttelephone = $contacttelephone;
		$this->registry->template->contactaddress = $contactaddress;
		$this->registry->template->contactemail = $contactemail;
		$this->registry->template->title = "Contact";
		$this->registry->template->intro = "Change contact details of your website";
		$this->registry->template->show( 'contactform' );
	}
	
	public function edit_contact() {
		if(isset($this->httpvars['editcontact'])) {
			$xmlDoc = new DOMDocument();
			$xmlDoc->formatOutput = true;
			$root = $xmlDoc->createElement('contact');
			$xmlDoc->appendChild($root);
			$contactname = $xmlDoc->createElement('name');
			$contactname->appendChild($xmlDoc->createTextNode($this->httpvars['contactname']));
			$root->appendChild($contactname);
			$contacttelephone = $xmlDoc->createElement('telephone');
			$contacttelephone->appendChild($xmlDoc->createTextNode($this->httpvars['contacttelephone']));
			$root->appendChild($contacttelephone);
			$contactaddress = $xmlDoc->createElement('address');
			$contactaddress->appendChild($xmlDoc->createTextNode($this->httpvars['contactaddress']));
			$root->appendChild($contactaddress);
			$contactemail = $xmlDoc->createElement('email');
			$contactemail->appendChild($xmlDoc->createTextNode($this->httpvars['contactemail']));
			$root->appendChild($contactemail);
			$file=__ROOT.DS."contact.xml";
			$save = TRUE;
			$this->registry->template->message = "Contact details successfully changed!";
			if(file_exists($file)) {
				if(!is_writable($file)) {
					$this->registry->template->message = "Action failed: XML descriptor '$file' isn't writable!";
					$save=FALSE;
				}
			}
			if($save === TRUE)
				$xmlDoc->save(__ROOT.DS.'contact.xml');
		}
		$this->index();
	}
}
