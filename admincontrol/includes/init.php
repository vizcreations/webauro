<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Engine - Initialize all objects and data ready for processing
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */


/* Constants Definitions */
define( "DS", DIRECTORY_SEPARATOR );
define( "__AROOT", "../" );
define( "__API", __AROOT.DS."includes".DS."library" );
define( "__SELF", $_SERVER['PHP_SELF'] );
define( "__INC", __ROOT.DS."includes" );
define( "__APP", __ROOT.DS."application" );
define( "__CONTROLLERS", __ROOT.DS."controllers" );
define( "__MODELS", __ROOT.DS."models" );
define( "__VIEWS", __ROOT.DS."views" );


/* Include application core classes */
require_once(__APP.DS.'registry.class.php');
require_once(__APP.DS.'database.class.php');
require_once(__APP.DS.'router.class.php');
require_once(__APP.DS.'controller.class.php');
require_once(__APP.DS.'template.class.php');
require_once(__AROOT.DS.'configuration.php');

$registry = new Registry();
$registry->db = DB::getInstance();
if(DB::$error != null || !empty(DB::$error)) die( DB::$error );
$registry->isLogged = false;

$site = $app['sitename'];
$registry->sitename = $registry->websitename = $site;
$registry->url = $webserver['url'];

$name = $site."acookie";
$value = "true";
$time = 3600;
$dir = "/";
setcookie($name, $value, time()+$time, $dir);

if(isset($_SESSION[$site.'admin_is_logged_in'])) {
	if($_SESSION[$site.'admin_is_logged_in'] == true) { 
		$registry->isLogged = true;
	}
}

$registry->superadminIsLogged = false;

if(!isset($_COOKIE[$site.'acookie'])) {
	//$registry->router->unsetTheCookie();
	if(isset($_SESSION[$site.'admin_is_logged_in'])) {
		unset($_SESSION[$site.'admin_is_logged_in']);
		$registry->isLogged = false;
		$registry->userid = 0;
	}
}

//if(isset($_SESSION['vc_amemberid'])) $registry->userid = $_SESSION['vcadmin_userid']; else $registry->userid = 0;
if(isset($_SESSION[$site.'admin_username'])) $registry->username = $_SESSION[$site.'admin_username']; else $registry->username = null;
if(isset($_SESSION[$site.'admin_role'])) $registry->adminrole = $_SESSION[$site.'admin_role']; else $registry->adminrole = null;
if($registry->adminrole == 'superadmin') $registry->superadminIsLogged = true;

$registry->router = new Router($registry);
$registry->router->setControllerPath(__CONTROLLERS);
$registry->router->setTheCookie();

$registry->template = new Template($registry);
if(!empty($registry->router->message)) die( $registry->router->message );

/* Default timezone to UTC */
date_default_timezone_set('UTC');

/** Auto load model classes */
function __autoload($class) {
	$file = str_replace('model','',strtolower($class));
	$model = __ROOT.DS.'models'.DS.strtolower($file).'.model.php';
	if(file_exists($model) && is_readable($model)) {
		require_once $model;
	}
}


