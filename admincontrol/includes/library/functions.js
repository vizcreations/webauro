/**
 * @abstract Admincontrol JavaScript Functions
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */ 

/* JavaScript functions for AdminControl */

var uri, div;
function changeGalleries(cityid) {
	uri = 'async.php';
	uri = uri + '?route=ajax/get_galleries/' + cityid;
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest('changegallery', uri);
}

function sendAJAXRequest(div, uri) {
	xmlHttp = getXmlHttpObject();
	if(xmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
			if(xmlHttp.status == 200) {
				thediv = document.getElementById(div);
				thediv.innerHTML = xmlHttp.responseText;
			}
		}
	}
	xmlHttp.open("GET", uri, true);
	xmlHttp.send(null);
}

function defaultBG() {
	if(confirm("Are you sure you want to change background to default?")) {
		window.location.href = "index.php?route=background/default_background";
	}
}
