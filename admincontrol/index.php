<?php
/* Begin a Session */
session_start();

/**
 * @abstract Admincontrol - Single point entry - Index 
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */

/* During development mode change setting for errors to appear */
error_reporting(E_ALL); // E_ALL to show
ini_set( "display_errors", "On" ); // On to show

/* Set a root path variable for security */
$path = realpath(dirname(__FILE__));
define( "__ROOT", $path );

/* Declarations and Object initializations */
require_once( 'includes'.DIRECTORY_SEPARATOR.'init.php' );
?>


<!-- HTML -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Webauro - <?php echo $registry->sitename; ?></title>
<link rel="stylesheet" href="stylesheets/styles.css" media="all" type="text/css" />
<script type="text/javascript" src="includes/library/ajax.js"></script>
<script type="text/javascript" src="includes/library/functions.js"></script>
</head>

<body>
<div id="wrapper">
<!-- HEADER -->
<div id="header">
	<div id="headertop">
		<div id="logo">
			<h1>
				<table cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<img src="images/png/icons/conf.png" alt="acontrol" border="0" />
					</td>
					<td style="padding-left: 3px;">
						<a href="index.php">Webauro <small>Admin Control</small></a>
					</td>
				</tr>
				</table>
			</h1>
		</div>		
		<div id="flashad">
			<h4><?php echo $registry->websitename; ?></h4>
		</div>
	</div>
	<?php if($registry->isLogged) { ?>
	<div id="headerbottom">
		<div id="menubar">
			<?php require_once( __VIEWS.DS."menubar.view.php" ); ?>
		</div>
	</div>
	<?php } ?>
</div>
<!-- HEADER -->

<!-- SSI -->
<div id="mid">
	<?php $registry->router->loader(); ?>
</div>
<!-- SSI -->

<!-- FOOTER -->
<div id="footer">
	<p>2007 - 2010 &copy; www.vizcreations.com. All rights reserved on graphics and design.</p>
</div>
<!-- FOOTER -->
</div>
</body>
</html>

<!-- HTML -->


<?php DB::closeInstance(); ?>
