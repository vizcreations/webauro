<?php
/* Begin a Session */
session_start();
/**
 * @abstract Admincontrol Async entry point
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 

/* Development mode so we enable errors to appear */
error_reporting(E_ALL);

/* Set a root path variable for security */
$path = realpath(dirname(__FILE__));
define( "__ROOT", $path );

/* Declarations and Object initializations */
require_once( 'includes/engine.php' );

$registry->router->loader();

?>
<?php DB::closeInstance(); ?>
