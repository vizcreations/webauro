<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/* Class definition for the Registry */

/**
 * @abstract Admincontrol Registry Class to supply global information and objects
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
Class Registry {
	
	/**
	 * @var type array
	 */
	public $vars = array();
	
	/**
	 * 
	 * @param $index
	 * @param $value
	 * @return void
	 */
	public function __set($index, $value) {
		$this->vars[$index] = $value;
	}
	
	/**
	 * 
	 * @param $value
	 * @return value
	 */
	public function __get($index) {
		return $this->vars[$index];
	}
	
}
