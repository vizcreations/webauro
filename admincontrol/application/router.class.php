<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );
/**
 * @abstract Admincontrol Router Class to load Controller and run it's action/method
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
/* Class definition for the Router */

Class Router {
	
	/**
	 * 
	 * @var null
	 */
	private $registry = null;
	
	private static $path = null;
	public $controller = 'index';
	private $action = 'index';
	public $id;
	public $message;
	public $ck = null;
	
	/**
	 * 
	 * @param $registry
	 * @return void
	 */
	public function __construct($registry) {
		$this->registry = $registry;
	}
	
	public function setControllerPath($thepath) {
		self::$path = $thepath;
	}
	
	public function loader() {
		$this->getController();
		
		if(!$this->registry->isLogged && $this->controller != 'index') {
			$this->controller = 'index';
			$this->action = 'index';
		}
		
		$controllerclassfile = self::$path.DS.$this->controller.'.controller.php';
		if(!$controllerclassfile || !file_exists($controllerclassfile)) {
			die( '404 file not found <br />'.$controllerclassfile );
		}
		
		require_once($controllerclassfile);
		
		$controllerclass = strtolower($this->controller).'Controller';
		$controller = new $controllerclass($this->registry);
		if(method_exists($controller, $this->action)) {
			$action = $this->action;
		} else {
			$action = 'index';			
		}		
		
		if(empty($this->id) && $this->id == 0) {
			$controller->$action();
		} else {
			$id = $this->id;
			$controller->$action($id);
		}
	}
	
	/**
	 * 
	 * @return void
	 */
	public function getController() {
		
		$route = isset($_REQUEST['route']) ? $_REQUEST['route'] : '';
		if(!empty($route)) {
			$parts = explode('/', $route);
		} else {
			$parts = null;
		}
		
		if(!empty($parts[0])) {
			$this->controller = $parts[0];
		} else {
			$this->controller = 'index';
		}
		
		if(!empty($parts[1])) {
			$this->action = $parts[1];
		} else {
			$this->action = 'index';
		}
		
		if(!empty($parts[2])) {
			$this->id = $parts[2];
		} else {
			$this->id = '';
		}
		
		if(!empty($parts[3])) {
			$this->message = $parts[3];
		}
		
	}
	
	public function redirect($uri, $msg) {
		$this->registry->template->message = $msg;
		?>
		<script type="text/javascript">
		<!--
		window.location.href='<?php echo $uri; ?>';
		//-->
		</script>
		<?php
	}
	
	public function header($url) {
		header("Location: $url");
	}
	
	public function loadFrag($frag) {
		$fragfile = __FRAGS.DS.$frag.'.frag.php';
		if(file_exists($fragfile))
			require_once( __FRAGS.DS.$frag.'.frag.php' );
		else
			echo "404 Frag template $fragfile not found!";
	}
	
	public function setTheCookie() {
		$this->ck = $_COOKIE;
	}
	
	public function unsetTheCookie() {
		$this->ck = null;
	}
	
}
