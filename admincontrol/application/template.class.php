<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );
/**
 * @abstract Admincontrol Template Class to supply data to views
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
/* Class definition for the Router */

/* Class definition for the Template */

Class Template {
	
	/**
	 * 
	 * @var object
	 */
	private $registry;
	
	/**
	 * 
	 * @var array
	 */
	public $vars = array();
	
	public function template($registry) {
		$this->registry = $registry;
	}
	
	/**
	 * 
	 * @param $index
	 * @param $value
	 * @return void
	 */
	public function __set($index, $value) {
		$this->vars[$index] = $value;
	}
	
	public function show( $template ) {
		// $path = __VIEWS.DS.$template.'.view.php';
		$feature = $this->registry->router->controller;
		/*if($feature == 'index')
			$path = __VIEWS.DS.$template.'.view.php';
		else*/
		
		$path = __VIEWS.DS.strtolower($feature).DS.$template.'.view.php';
		foreach($this->vars as $key => $value) {
			$$key = $value;
		}
		if(file_exists($path)) {
			require_once($path);
		} else {
			echo '404 Template Not found in '.$path;
		}
	}
	
}
