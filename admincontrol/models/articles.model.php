<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Articles Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
class ArticlesModel extends DB {
	public function save($vars, $entity, $option) {
		// TODO CODE
/* 		if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		
		$vars = $this->escapeSQL($vars);
		switch($entity) {
			case 'articles':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__articles(article_title, article_author, article_content, timestamp) ".
									" VALUES('{$vars['articletitle']}', '{$vars['articleauthor']}', '{$vars['articlecontent']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__articles ".
									" SET article_title='{$vars['articletitle']}', ".
									" article_author='{$vars['articleauthor']}', ".
									" article_content='{$vars['articlecontent']}', ".
									" timestamp=NOW() ".
									" WHERE article_id='{$vars['articleid']}' ";
						break;
				}
				break;
			case 'ar_category_rel':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__articles_categories_rel(article_id, ar_category_id, timestamp) ".
									" VALUES('".self::getInsertId()."', '{$vars['articlecategory']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__articles_categories_rel ".
									" SET ar_category_id='{$vars['articlecategory']}' ".
									" WHERE article_id='{$vars['articleid']}' ";
				}
				break;
			case 'ar_category':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__ar_categories(ar_category_name, ar_category_description, timestamp) ".
									" VALUES('{$vars['categoryname']}', '{$vars['categorydesc']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__ar_categories ".
									" SET ar_category_name='{$vars['categoryname']}', ".
									" ar_category_description='{$vars['categorydesc']}', ".
									" timestamp=NOW() ".
									" WHERE ar_category_id='{$vars['categoryid']}' ";
						break;
				}
				break;
			case 'article_seo':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__articles_seo(article_id, seo_id, timestamp) ".
									" VALUES('{$vars['articleid']}', '{$vars['seoid']}', NOW()) ";
						break;
				}
				break;
			case 'articles_status':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__articles_status(article_id, article_status, timestamp) ".
									" VALUES('{$vars['articleid']}', '{$vars['articlestatus']}', NOW()) ";
						break;
				}
				break;
		}
		$this->setQuery($query);
		$this->execute();
	}
	
	public function getArticles($offset, $rowsperpage) {
		$query = " SELECT * ".
					" FROM #__articles, #__ar_categories, #__articles_categories_rel, #__articles_status ".
					" WHERE #__articles_categories_rel.article_id=#__articles.article_id ".
					" AND #__ar_categories.ar_category_id=#__articles_categories_rel.ar_category_id ".
					" AND #__articles_status.article_id=#__articles.article_id ".
					" ORDER BY #__articles.article_id ".
					" DESC ".
					" LIMIT $offset, $rowsperpage ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getArticlesCount() {
		$query = " SELECT COUNT(*) AS count ".
					" FROM #__articles, #__ar_categories, #__articles_categories_rel, #__articles_status ".
					" WHERE #__articles_categories_rel.article_id=#__articles.article_id ".
					" AND #__ar_categories.ar_category_id=#__articles_categories_rel.ar_category_id ".
					" AND #__articles_status.article_id=#__articles.article_id ".
					" ORDER BY #__articles.article_id ".
					" DESC ";
		$this->setQuery($query);
		$count = $this->getResult();
		return $count;
	}
	
	public function getHomeArticles() {
		$query = " SELECT * ".
					" FROM #__articles ".
					" ORDER BY article_id ".
					" DESC ".
					" LIMIT 0, 3 ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function getArticleComments($id) {
		$query = " SELECT * ".
					" FROM #__articles, #__ar_comments, #__articles_comments_rel ".
					" WHERE #__articles.article_id='$id' ".
					" AND #__articles_comments_rel.article_id=#__articles.article_id ".
					" AND #__ar_comments.ar_comment_id=#__articles_comments_rel.ar_comment_id ".
					" ORDER BY #__ar_comments.timestamp ".
					" DESC ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function removeComment($cid) {
		$content = 'Comment removed!';
		$query = " UPDATE #__ar_comments ".
					" SET ar_comment_content='$content', ".
					" ar_comment_name='Viz', ".
					" ar_comment_email='', ".
					" ar_comment_uri='' ".
					" WHERE ar_comment_id='$cid' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function getArticleCategories() {
		$query = " SELECT * ".
					" FROM #__ar_categories ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getCategory($catid) {
		$query = " SELECT * ".
					" FROM #__ar_categories ".
					" WHERE ar_category_id='$catid' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getArticle($articleid) {
		$query = " SELECT * ".
					" FROM #__articles, #__ar_categories, #__articles_categories_rel ".
					" WHERE #__articles.article_id='$articleid' ".
					" AND #__articles_categories_rel.article_id=#__articles.article_id ".
					" AND #__ar_categories.ar_category_id=#__articles_categories_rel.ar_category_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getCategoryArticles($catid) {
		$query = " SELECT * ".
					" FROM #__articles, #__ar_categories, #__articles_categories_rel ".
					" WHERE #__ar_categories.ar_category_id='$catid' ".
					" AND #__articles_categories_rel.ar_category_id=#__ar_categories.ar_category_id ".
					" AND #__articles.article_id=#__articles_categories_rel.article_id ".
					" ORDER BY #__articles.timestamp ".
					" DESC ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getArticleSEO($articleid) {
		$query = " SELECT * ".
					" FROM #__articles, #__seo, #__articles_seo ".
					" WHERE #__articles.article_id='$articleid' ".
					" AND #__articles_seo.article_id=#__articles.article_id ".
					" AND #__seo.seo_id=#__articles_seo.seo_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function enableArticle($id) {
		$query = " UPDATE #__articles_status ".
					" SET article_status='1' ".
					" WHERE article_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function disableArticle($id) {
		$query = " UPDATE #__articles_status ".
					" SET article_status='0' ".
					" WHERE article_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function getCommentsStatus($id) {
		$query = " SELECT ar_comments_status ".
					" FROM #__ar_comments_status ".
					" WHERE article_id='$id' ";
		$this->setQuery($query);
		$data = $this->getResult();
		return $data;
	}
	
	public function enableCStatus($id) {
		$query = " SELECT COUNT(*) AS count ".
					" FROM #__ar_comments_status ".
					" WHERE article_id='$id' ";
		$this->setQuery($query);
		$exists = $this->getResult();
		if(!$exists) {
			$query = " INSERT INTO #__ar_comments_status(article_id, ar_comments_status, timestamp) ".
						" VALUES('$id', '1', NOW()) ";
		} else {
			$query = " UPDATE #__ar_comments_status ".
						" SET ar_comments_status='1' ".
						" WHERE article_id='$id' ";
		}
		$this->setQuery($query);
		$this->execute();
	}
	
	public function disableCStatus($id) {
		$query = " SELECT COUNT(*) AS count ".
					" FROM #__ar_comments_status ".
					" WHERE article_id='$id' ";
		$this->setQuery($query);
		$exists = $this->getResult();
		if(!$exists) {
			$query = " INSERT INTO #__ar_comments_status(article_id, ar_comments_status, timestamp) ".
						" VALUES('$id', '0', NOW()) ";
		} else {
			$query = " UPDATE #__ar_comments_status ".
						" SET ar_comments_status='0' ".
						" WHERE article_id='$id' ";
		}
		$this->setQuery($query);
		$this->execute();
	}
};
