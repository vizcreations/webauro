<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Components Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ComponentsModel extends DB {
	public function save($vars, $entity, $option) {
/* 
		if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		$vars = $this->escapeSQL($vars);
		// TODO CODE
		switch($entity) {
			case 'components':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__controllers(controller_name, timestamp) VALUES('{$vars['controller']}', NOW()) ";
						break;
				}
				break;
			case 'views':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__views(view_name, argument, timestamp) VALUES('{$vars['view']}', '{$vars['argument']}', NOW()) ";
						break;
				}
				break;
			case 'controller_view':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__controllers_views(controller_id, view_id, timestamp) VALUES('{$vars['controllerid']}', '".self::getInsertId()."', NOW()) ";
						break;
				}
				break;
			case 'views_arguments':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__views_arguments(view_id, argument_table, argument_id, argument_name, timestamp) ".
									" VALUES('".self::getInsertId()."', '{$vars['argumenttable']}', '{$vars['argumentid']}', '{$vars['argumentname']}', NOW()) ";
						break;
				}
		}
		$this->setQuery($query);
		$this->execute();
	}
	
	public function getComponents() {
		$query = " SELECT * ".
					" FROM #__controllers ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getHomeComponents() {
		$query = " SELECT * ".
					" FROM #__controllers ".
					" ORDER BY controller_id ".
					" DESC ".
					" LIMIT 0, 3 ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function getControllers() {
		$this->getComponents();
	}
	
	public function getViews($controllerid) {
		$query = " SELECT * ".
					" FROM #__controllers, #__views, #__controllers_views ".
					" WHERE #__controllers.controller_id='$controllerid' ".
					" AND #__controllers_views.controller_id=#__controllers.controller_id ".
					" AND #__views.view_id=#__controllers_views.view_id ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getComponentName($componentid) {
		$query = " SELECT controller_name ".
					" FROM #__controllers ".
					" WHERE controller_id='$componentid' ";
		$this->setQuery($query);
		$name = $this->getResult();
		return $name;
	}
	
	public function checkArgument($viewid) {
		$query = " SELECT argument ".
					" FROM #__views ".
					" WHERE view_id='$viewid' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
	
	public function getArguments($viewid) {
		$query = " SELECT argument_table, argument_id, argument_name ".
					" FROM #__views_arguments ".
					" WHERE view_id='$viewid' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getSelectedArg($viewid, $menuitemid) {
		$query = " SELECT argument_value ".
					" FROM #__menuitems_views ".
					" WHERE view_id='$viewid' ".
					" AND menuitem_id='$menuitemid' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
	
	public function getArgsList($args) {
		$table = $args[0];
		$idfield = $args[1];
		$namefield = $args[2];
		$query = " SELECT $idfield, $namefield ".
					" FROM #__$table ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getControllerName($cid) {
		$query = " SELECT controller_name ".
					" FROM #__controllers ".
					" WHERE controller_id='$cid' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
	
	public function getViewName($id) {
		$query = " SELECT view_name ".
					" FROM #__views ".
					" WHERE view_id='$id' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
}
