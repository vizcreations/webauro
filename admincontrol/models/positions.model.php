<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Positions Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
class PositionsModel extends DB {
	public function save($vars, $entity, $option) {
		/* if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		
		$vars = $this->escapeSQL($vars);
		// TODO CODE
	}
	
	public function getPositions() {
		$query = " SELECT * ".
					" FROM #__pos ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getPositionMenus($positionid) {
		$query = " SELECT * ".
					" FROM #__pos, #__menus_pos, #__menus ".
					" WHERE #__pos.pos_id='$positionid' ".
					" AND #__menus_pos.pos_id=#__pos.pos_id ".
					" AND #__menus.menu_id=#__menus_pos.menu_id ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getPositionFrags($positionid) {
		$query = " SELECT * ".
					" FROM #__pos, #__frags_pos, #__frags ".
					" WHERE #__pos.pos_id='$positionid' ".
					" AND #__frags_pos.pos_id=#__pos.pos_id ".
					" AND #__frags.frag_id=#__frags_pos.frag_id ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function getPosition($positionid) {
		$query = " SELECT * ".
					" FROM #__pos ".
					" WHERE position_id='$positionid' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
}
