<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol SEO Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
class SEOModel extends DB {
	public function save($vars, $entity, $option) {
		/* if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		
		$vars = $this->escapeSQL($vars);
		switch($entity) {
			case 'seo':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__seo(seo_title, seo_keywords, seo_description, timestamp) ".
									" VALUES('{$vars['seotitle']}', '{$vars['seokeywords']}', '{$vars['seodescription']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__seo ".
									" SET seo_title='{$vars['seotitle']}', ".
									" seo_keywords='{$vars['seokeywords']}', ".
									" seo_description='{$vars['seodescription']}', ".
									" timestamp=NOW() ".
									" WHERE seo_id='{$vars['seoid']}' ";
						break;
				}
				break;
		}
		$this->setQuery($query);
		$this->execute();
	}
}
