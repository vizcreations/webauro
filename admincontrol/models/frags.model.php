<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Frags Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class FragsModel extends DB {
	public function save($vars, $entity, $option) {
		/* if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		
		$vars = $this->escapeSQL($vars);
		switch($entity) {
			case 'frag':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__frags(frag_name, frag_title, timestamp) ".
									" VALUES('{$vars['fragname']}', '{$vars['fragtitle']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__frags ".
									" SET frag_name='{$vars['fragname']}', ".
									" frag_title='{$vars['fragtitle']}', ".
									" timestamp=NOW() ".
									" WHERE frag_id='{$vars['fragid']}' ";
						break;
				}
				break;
			case 'frag_pos':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__frags_pos(frag_id, pos_id, timestamp) ".
									" VALUES('{$vars['fragid']}', '{$vars['fragpos']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__frags_pos ".
									" SET pos_id='{$vars['fragpos']}', ".
									" timestamp=NOW() ".
									" WHERE frag_id='{$vars['fragid']}' ";
						break;
				}
				break;
			case 'frag_type_rel':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__frags_types_rel(frag_id, frag_type_id, timestamp) ".
									" VALUES('{$vars['fragid']}', '{$vars['fragtypeid']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__frags_types_rel ".
									" SET frag_type_id='{$vars['fragtypeid']}', ".
									" timestamp=NOW() ".
									" WHERE frag_id='{$vars['fragid']}' ";
						break;
				}
				break;
			case 'frag_status':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__frags_status(frag_id, frag_status, timestamp) ".
									" VALUES('{$vars['fragid']}', '{$vars['fragstatus']}', NOW()) ";
						break;
				}
				break;
		}
		$this->setQuery($query);
		$this->execute();
	}
	
	public function getFrags() {
		$query = " SELECT * ".
					" FROM #__frags, #__pos, #__frags_pos, #__frags_status ".
					" WHERE #__frags_pos.frag_id=#__frags.frag_id ".
					" AND #__pos.pos_id=#__frags_pos.pos_id ".
					" AND #__frags_status.frag_id=#__frags.frag_id ".
					" ORDER BY #__frags.timestamp ".
					" DESC ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getHomeFrags() {
		$query = " SELECT * ".
					" FROM #__frags ".
					" ORDER BY frag_id ".
					" DESC ".
					" LIMIT 0, 3 ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function getFragTypes() {
		$query = " SELECT * ".
					" FROM #__frags_types ".
					" ORDER BY frag_type_id ".
					" DESC ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function getFragTypeId($fragtype) {
		$query = " SELECT frag_type_id ".
					" FROM #__frags_types ".
					" WHERE frag_type_name='$fragtype' ";
		$this->setQuery($query);
		$data = $this->getResult();
		return $data;
	}
	
	public function getFrag($fragid) {
		$query = " SELECT * ".
					" FROM #__frags, #__pos, #__frags_pos, #__frags_types, #__frags_types_rel ".
					" WHERE #__frags.frag_id='$fragid' ".
					" AND #__frags_pos.frag_id=#__frags.frag_id ".
					" AND #__pos.pos_id=#__frags_pos.pos_id ".
					" AND #__frags_types_rel.frag_id=#__frags.frag_id ".
					" AND #__frags_types.frag_type_id=#__frags_types_rel.frag_type_id ";
		$this->setQuery($query);
		$data = $this->getRow();
		return $data;
	}
	
	public function getFragArgument($fragid) {
		$query = " SELECT * ".
					" FROM #__frags, #__pos, #__frags_pos, #__frags_arguments ".
					" WHERE #__frags.frag_id='$fragid' ".
					" AND #__frags_pos.frag_id=#__frags.frag_id ".
					" AND #__frags_arguments.frag_id=#__frags.frag_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getFragContent($fragid) {
		$query = " SELECT * ".
					" FROM #__frags, #__pos, #__frags_pos, #__frags_content ".
					" WHERE #__frags.frag_id='$fragid' ".
					" AND #__frags_pos.frag_id=#__frags.frag_id ".
					" AND #__pos.pos_id=#__frags_pos.pos_id ".
					" AND #__frags_content.frag_id=#__frags.frag_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getArgument($fragid) {
		$query = " SELECT argument_value ".
					" FROM #__frags, #__frags_arguments ".
					" WHERE #__frags.frag_id='$fragid' ".
					" AND #__frags_arguments.frag_id=#__frags.frag_id ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
	
	public function enableFrag($fragid) {
		$query = " UPDATE #__frags_status ".
					" SET frag_status='1' ".
					" WHERE frag_id='$fragid' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function disableFrag($fragid) {
		$query = " UPDATE #__frags_status ".
					" SET frag_status='0' ".
					" WHERE frag_id='$fragid' ";
		$this->setQuery($query);
		$this->execute();
	}
}
