<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Frags Content Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
 
class FragsContentModel extends DB {
	public function save($vars, $entity, $option) {
		/* if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		
		$vars = $this->escapeSQL($vars);
		switch($entity) {
			case 'frag_content':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__frags_content(frag_id, frag_content, timestamp) ".
									" VALUES('{$vars['fragid']}', '{$vars['fragcontent']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__frags_content ".
									" SET frag_content='{$vars['fragcontent']}', ".
									" timestamp=NOW() ".
									" WHERE frag_id='{$vars['fragid']}' ";
				}
				break;
		}
		$this->setQuery($query);
		$this->execute();
	}
	
	public function check_exists($fragid) {
		$query = " SELECT COUNT(*) ".
					" FROM #__frags_content ".
					" WHERE frag_id='$fragid' ";
		$this->setQuery($query);
		$count = $this->getResult();
		if($count > 0) return true;
		else return false;
	}
}
