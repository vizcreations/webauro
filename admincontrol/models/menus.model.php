<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Menus Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
class MenusModel Extends DB {
	public function save($vars, $entity, $option) {
/* 		if(!get_magic_quotes_gpc()) {
			$thevars = $vars;
			foreach($thevars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		} */
		$vars = $this->escapeSQL($vars);
		switch($entity) {
			case 'menu':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menus(menu_name, timestamp) ".
									" VALUES('{$vars['menuname']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__menus ".
									" SET menu_name='{$vars['menuname']}', ".
									" timestamp=NOW() ".
									" WHERE menu_id='{$vars['menuid']}' ";
						break;
				}
				break;
			case 'menu_pos':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menus_pos(menu_id, pos_id, timestamp) ".
									" VALUES('".self::getInsertId()."', '{$vars['menupos']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__menus_pos ".
									" SET pos_id='{$vars['menupos']}' ".
									" WHERE menu_id='{$vars['menuid']}' ";
						break;
				}
				break;
			case 'menu_status':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menus_status(menu_id, menu_status, timestamp) ".
									" VALUES('{$vars['menuid']}', '{$vars['menustatus']}', NOW()) ";
						break;
				}
				break;
			case 'menuitem':
				$link = $vars['menuitemcontrollername'].'/'.$vars['menuitemviewname'].'/'.$vars['menuitemargument'];
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menuitems(menuitem_name, menuitem_link, timestamp) ".
									" VALUES('{$vars['menuitemname']}', '$link', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__menuitems ".
									" SET menuitem_name='{$vars['menuitemname']}', ".
									" menuitem_link='$link' ".
									" WHERE menuitem_id='{$vars['menuitemid']}' ";
						break;
				}
				break;
			case 'menuitem_menu':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menuitems_menus(menuitem_id, menu_id, timestamp) ".
									" VALUES('{$vars['menuitemid']}', '{$vars['menuid']}', NOW()) ";
						break;
				}
				break;
			case 'menuitem_view':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menuitems_views(menuitem_id, view_id, argument_value, timestamp) ".
									" VALUES('{$vars['menuitemid']}', '{$vars['menuitemview']}', '{$vars['menuitemargument']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__menuitems_views ".
									" SET view_id='{$vars['menuitemview']}', ".
									" argument_value='{$vars['menuitemargument']}' ".
									" WHERE menuitem_id='{$vars['menuitemid']}' ";
						break;
				}
				break;
			case 'menuitem_template':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menuitems_templates(menuitem_id, template_id, timestamp) ".
									" VALUES('{$vars['menuitemid']}', '{$vars['menuitemtemplate']}', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__menuitems_templates ".
									" SET template_id='{$vars['menuitemtemplate']}' ".
									" WHERE menuitem_id='{$vars['menuitemid']}' ";
						break;
				}
				break;			
			case 'menuitem_parent':				
				switch($option) {					
					case 'add':						
						$query = " INSERT INTO #__menuitems_parents(menuitem_id, parent_menuitem_id, timestamp) ".									
						" VALUES('{$vars['menuitemid']}', '{$vars['menuitemparent']}', NOW()) ";						
						break;					
						case 'edit':						
							$query = " UPDATE #__menuitems_parents ".									
							" SET parent_menuitem_id='{$vars['menuitemparent']}', ".									
							" timestamp=NOW() ".									
							" WHERE menuitem_id='{$vars['menuitemid']}' ";						
							break;				
				}				
				break;
			case 'menuitem_status':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__menuitems_status(menuitem_id, menuitem_status, timestamp) ".
									" VALUES('{$vars['menuitemid']}', '{$vars['menuitemstatus']}', NOW()) ";
						break;
				}
				break;
		}
		if(!$query) return;
		$this->setQuery($query);
		$this->execute();
	}
	
	public function getMenus() {
		$query = " SELECT * ".
					" FROM #__menus, #__pos, #__menus_pos, #__menus_status ".
					" WHERE #__menus_pos.menu_id=#__menus.menu_id ".
					" AND #__pos.pos_id=#__menus_pos.pos_id ".
					" AND #__menus_status.menu_id=#__menus.menu_id ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getHomeMenus() {
		$query = " SELECT * ".
					" FROM #__menus ".
					" ORDER BY menu_id ".
					" DESC ".
					" LIMIT 0, 3 ";
		$this->setQuery($query);
		$data = $this->getRows();
		return $data;
	}
	
	public function getMenu($menuid) {
		$query = " SELECT * ".
					" FROM #__menus, #__pos, #__menus_pos ".
					" WHERE #__menus.menu_id='$menuid' ".
					" AND #__menus_pos.menu_id=#__menus.menu_id ".
					" AND #__pos.pos_id=#__menus_pos.pos_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getMenuitems($menuid) {
		$query = " SELECT * ".
					" FROM #__menus, #__menuitems, #__menuitems_menus, #__menuitems_status ".
					" WHERE #__menus.menu_id='$menuid' ".
					" AND #__menuitems_menus.menu_id=#__menus.menu_id ".
					" AND #__menuitems.menuitem_id=#__menuitems_menus.menuitem_id ".
					" AND #__menuitems_status.menuitem_id=#__menuitems.menuitem_id ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getMenuitem($menuitemid) {
		$query = " SELECT * ".
					" FROM #__menuitems, #__menus, #__menuitems_menus, #__templates, #__menuitems_templates, #__menuitems_parents, ".
					" #__controllers, #__controllers_views, #__views, #__menuitems_views ".
					" WHERE #__menuitems.menuitem_id='$menuitemid' ".
					" AND #__menuitems_menus.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__menus.menu_id=#__menuitems_menus.menu_id ".
					" AND #__menuitems_views.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__views.view_id=#__menuitems_views.view_id ".
					" AND #__controllers_views.view_id=#__views.view_id ".
					" AND #__controllers.controller_id=#__controllers_views.controller_id ".					" AND #__menuitems_parents.menuitem_id=#__menuitems.menuitem_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getMenuitemTemplate($menuitemid) {
		$query = " SELECT #__menuitems_templates.template_id, #__templates.template_name ".
					" FROM #__templates, #__menuitems_templates, #__menuitems ".
					" WHERE #__menuitems.menuitem_id='$menuitemid' ".
					" AND #__menuitems_templates.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__templates.template_id=#__menuitems_templates.template_id ";
		$this->setQuery($query);
		$tmpl = $this->getResult();
		return $tmpl;
	}
	
	public function enableMenu($menuid) {
		$query = " UPDATE #__menus_status ".
					" SET menu_status='1' ".
					" WHERE menu_id='$menuid' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function disableMenu($menuid) {
		$query = " UPDATE #__menus_status ".
					" SET menu_status='0' ".
					" WHERE menu_id='$menuid' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function enableMenuitem($id) {
		$query = " UPDATE #__menuitems_status ".
					" SET menuitem_status='1' ".
					" WHERE menuitem_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}
	
	public function disableMenuitem($id) {
		$query = " UPDATE #__menuitems_status ".
					" SET menuitem_status='0' ".
					" WHERE menuitem_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}
};
