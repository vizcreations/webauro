<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Admincontrol Users Model
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 * 
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 *
 */
 
class UsersModel extends DB {
	public function save($vars, $entity, $option) {		
		/* if(!get_magic_quotes_gpc()) {			
			foreach($vars as $key => $val) {				
				$vars[$key] = mysql_real_escape_string($val);			
			}		
		} */
		
		$vars = $this->escapeSQL($vars);
		switch($entity) {			
			case 'users':				
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__users(user_name, user_password, user_email, user_status, user_online, ".
									" user_location, user_joined, user_image, timestamp) ".
									" VALUES('{$vars['username']}', MD5('{$vars['password']}'), '{$vars['email']}', '1', '0', ".
									" '{$vars['location']}', NOW(), '', NOW()) ";
						break;
					case 'edit':						
						$query = " UPDATE #__users ".									
									" SET user_name='{$vars['username']}', ".									
									" user_email='{$vars['useremail']}', ".									
									" user_status='{$vars['userstatus']}', ".									
									" user_location='{$vars['userlocation']}', ".									
									" timestamp=NOW() ".									
									" WHERE user_id='{$vars['userid']}' ";
						break;				
				}				
				break;
			case 'users_roles_rel':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__users_roles_rel(user_id, user_role_id, timestamp) ".
									" VALUES('{$vars['userid']}', '{$vars['userrole']}', NOW()) ";
						break;
					case 'edit':						
						$query = " UPDATE #__users_roles_rel ".									
									" SET user_role_id='{$vars['userrole']}' ".									
									" WHERE user_id='{$vars['userid']}' ";
						break;
				}
				break;
			case 'users_pass':
				switch($option) {
					case 'edit':
						$query = " UPDATE #__users ".
									" SET user_password=MD5('{$vars['newpassword']}') ".
									" WHERE user_id='{$vars['userid']}' ";
						break;
				}
				break;
			case 'users_roles':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__users_roles(user_role_name, user_role_desc, timestamp) ".
									" VALUES('{$vars['rolename']}', '{$vars['roledesc']}', NOW()) ";
						break;
				}
				break;
		}
		
		$this->setQuery($query);
		$this->execute();
	}
	
	public function check_login($username, $password) {
		$query = " SELECT COUNT(*) ".
					" FROM #__users, #__users_roles, #__users_roles_rel ".
					" WHERE #__users.user_name='$username' ".
					" AND #__users.user_password=MD5('$password') ".
					" AND #__users_roles_rel.user_id=#__users.user_id ".
					" AND #__users_roles.user_role_id=#__users_roles_rel.user_role_id ".					
					" AND #__users.user_status='1' ".
					" AND (#__users_roles.user_role_name='superadmin' ".
					" OR #__users_roles.user_role_name='admin') ";
		$this->setQuery($query);
		$result = $this->getResult();
		if($result > 0) return true;
		else return false;
	}
	
	public function getLatestUsers() {
		
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}		
	
	public function getUsers() {		
		$query = " SELECT * ".					
					" FROM #__users, #__users_roles_rel, #__users_roles ".					
					" WHERE #__users_roles_rel.user_id=#__users.user_id ".					
					" AND #__users_roles.user_role_id=#__users_roles_rel.user_role_id ".					
					" ORDER BY #__users.user_joined ".					
					" DESC ";		
		$this->setQuery($query);		
		$rows = $this->getRows();		
		return $rows;	
	}		
	
	public function deactivateUser($userid) {		
		$query = " UPDATE #__users ".					
					" SET user_status='0' ".					
					" WHERE user_id='$userid' ";		
		$this->setQuery($query);		
		$this->execute();	
	}		
	
	public function activateUser($userid) {		
		$query = " UPDATE #__users ".					
					" SET user_status='1' ".					
					" WHERE user_id='$userid' ";		
		$this->setQuery($query);		
		$this->execute();	
	}		
	
	public function getUsersRoles() {		
		$query = " SELECT * ".					
					" FROM #__users_roles ".					
					" ORDER BY timestamp ".					
					" DESC ";		
		$this->setQuery($query);		
		$rows = $this->getRows();		
		return $rows;	
	}		
	
	public function getRoleUsers($roleid) {		
		$query = " SELECT * ".					
					" FROM #__users, #__users_roles, #__users_roles_rel ".					
					" WHERE #__users_roles.user_role_id='$roleid' ".					
					" AND #__users_roles_rel.user_role_id=#__users_roles.user_role_id ".					
					" AND #__users.user_id=#__users_roles_rel.user_id ";		
		$this->setQuery($query);		
		$rows = $this->getRows();		
		return $rows;	
	}
	
	public function getUser($userid) {
		$query = " SELECT * ".					
					" FROM #__users, #__users_roles, #__users_roles_rel ".					
					" WHERE (#__users.user_id='$userid' ".
					" OR #__users.user_name='$userid') ".
					" AND #__users_roles_rel.user_id=#__users.user_id ".					
					" AND #__users_roles_rel.user_role_id=#__users_roles.user_role_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function deleteUser($userid) {
		
		$this->setQuery($query);
		$this->execute();
	}
	
	public function checkExists($username) {
		$query = " SELECT * ".
					" FROM #__users ".
					" WHERE (user_name='$username' ".
					" OR user_email='$username')";
		$this->setQuery($query);
		$count = $this->getCount();
		if($count > 0) return true;
		else return false;
	}
	
	public function getVisitors($unixtime) {
		$query = " SELECT * ".
					" FROM #__visitors ".
					" WHERE DATE(timestamp)=CURDATE() ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
}
