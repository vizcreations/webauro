/* Content JavaScript From XML */
/**
 * @abstract Admincontrol Javascript Library
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */


function genContent(option) {
	ajaxObject = getAJAXObject();
	if(ajaxObject == null) {
		window.alert("Sorry, your browser doesn't support AJAX!");
		return;
	}
	ajaxObject.onreadystatechange = function() {
		if(ajaxObject.readyState == 4 || ajaxObject.readyState == 'complete') {
			printContent(option, ajaxObject.responseXML);
		}
	}
	var url = option + '.xml';
	ajaxObject.open("POST", url, true);
	ajaxObject.send(null); 
}

function printContent(option, response) {
	var xmlDoc = response.documentElement;
	var intro, web, soft, footer;
	intro = document.getElementById('intro');
	if(option == 'index') {
		web = document.getElementById('web');
		soft = document.getElementById('soft');
	}
	footer = document.getElementById('footer');
	if(!intro) {
		return;
	}
	if(option == 'index') {
		if(!web) {
			return;
		}
		if(!soft) {
			return;
		}
	}
	if(!footer) {
		return;
	}
	var introcontent, webcontent, softcontent, footercontent;
	introcontent = xmlDoc.getElementsByTagName('intro')[0].childNodes[0].nodeValue;
	introcontent = introcontent.replace(/{/g, "<");
	introcontent = introcontent.replace(/}/g, ">");
	introcontent = introcontent.replace(/raquo/g, "&raquo;");
	intro.innerHTML = introcontent;
	
	if(option == 'index') {
		webcontent = xmlDoc.getElementsByTagName('web')[0].childNodes[0].nodeValue;
		webcontent = webcontent.replace(/{/g, "<");
		webcontent = webcontent.replace(/}/g, ">");
		webcontent = webcontent.replace(/raquo/g, "&raquo;");
		web.innerHTML = webcontent;
		softcontent = xmlDoc.getElementsByTagName('soft')[0].childNodes[0].nodeValue;
		softcontent = softcontent.replace(/{/g, "<");
		softcontent = softcontent.replace(/}/g, ">");
		softcontent = softcontent.replace(/raquo/g, "&raquo;");
		soft.innerHTML = softcontent;
	}
	footercontent = xmlDoc.getElementsByTagName('footer')[0].childNodes[0].nodeValue;
	footercontent = footercontent.replace(/{/g, "<");
	footercontent = footercontent.replace(/}/g, ">");
	footer.innerHTML = footercontent;
}

