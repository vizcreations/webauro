<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Index Controller Class which loads home page
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class definition for Index Controller */

class IndexController extends Controller {
	
	public function index() {
		$sitename = $this->registry->sitename;
		$this->registry->template->title = "Welcome to ".$sitename;
		$model = new UsersModel();
		if(isset($this->httpvars['message'])) {
			$var = $this->httpvars['message'];
			switch($var) {
				case 'empty':
					$message = 'Please enter mandatory fields!';
					break;
				case 'auth':
					$message = 'Incorrect email or password!';
					break;
				default:
					$message = '';
			}
			$this->registry->template->loginmessage = $message;
		} else {
			$this->registry->template->loginmessage = "";
		}
		$this->registry->template->intro = "Learn about ".$sitename;
		$this->registry->template->show( 'home' );
	}
	
	public function home() {
		$this->index();
	}
};
