<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Users Controller Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

class UsersController extends Controller {	
	public function index() {
		if($this->registry->isLogged)
			$this->mypage();
		else
			$this->unauthorized();
	}

	public function registration() {
        $model = new UsersModel();
		if(isset($this->httpvars['regmessage'])) {
			switch($this->httpvars['regmessage']) {
				case 'empty':
					$msg = "Please enter all required fields!";
					break;
				case 'exists':
					$msg = "Username or email already exists! Please choose another";
					break;
				case 'mysql':
					$msg = "Technical problem!";
					break;
				default:
					$msg = "";
					break;
			}
			$this->registry->template->regmessage = $msg;
		} else {
			$this->registry->template->regmessage = "";
		}
		
		$this->registry->template->loginmessage = "";
		$this->registry->template->title = "Register";
		$this->registry->template->intro = "";
		$this->registry->template->show( 'registration' );
	}

	public function check_user() {
		$username = $this->httpvars['username'];
		$lname = $this->httpvars['lname'];
		if(!empty($username) && !empty($lname)) {
			$model = new UsersModel();
			try {
				$exists = $model->checkUser($username, $lname);
				$this->registry->template->exists = $exists;
			} catch(Exception $e) {
				$this->registry->template->exists = true;
				$this->registry->template->message = $e->getMessage();
			}
		} else {
			$this->registry->template->exists = true;
		}
		$this->registry->template->show( 'checkuser' );
	}

	public function check_email($email='') {
		if(!empty($email)) {
			$model = new UsersModel();
			try {
				$exists = $model->checkEmail($email);
				$this->registry->template->exists = $exists;
			} catch(Exception $e) {
				$this->registry->template->exists = true;
				$this->registry->template->message = $e->getMessage();
			}
		} else {
			$this->registry->template->exists = true;
		}
		$this->registry->template->show_( 'checkemail' );
	}

	public function upload_photo() {
		if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
			$imgname = $_FILES['image']['name'];
			$imgtmpname = $_FILES['image']['tmp_name'];
			$imgsize = $_FILES['image']['size'];
			$imgtype = $_FILES['image']['type'];
			
			/*$ext = substr(strrchr($imgname, '.'), 1);
			$randname = md5(rand() * time());
			$imgname = strtolower($randname.'.'.$ext);*/
			$imgname = $this->httpvars['imagechosen'].'.jpg';
			$uploaddir = 'images/profiles/';
			$imgpath = $uploaddir.$imgname;
			$thumbspath = $uploaddir.DS."thumbs".DS.$imgname;
			move_uploaded_file($imgtmpname, $imgpath);
			require_once( __API.DS.'imageresize.php' );
			$resizer = new ImageTransform();
			$resizer->crop($imgpath, 300, 300, $imgpath);
			$resizer->crop($imgpath, 45, 45, $thumbspath);
		} else {
			//
		}
	}

	public function save_reg() {
		$this->model = new UsersModel();
		if(isset($this->httpvars['save'])) {
			$number = $this->httpvars['imgv'];
			$sitename = $this->registry->sitename;
			if(md5($number) == $_SESSION[$sitename]['image_random_value']) {
				if(isset($this->httpvars['regform_token']) && isset($_SESSION[$sitename]['regform_token'])) {
					if($this->httpvars['regform_token'] == $_SESSION[$sitename]['regform_token']) {
						if(empty($this->postvars['email']) || empty($this->postvars['uname']) || empty($this->postvars['password']) || $this->postvars['agree'] != 'yes') {
							$this->registry->router->header( "index.php?route=users/registration", "&regmessage=empty" );
							exit;
						}
						if(!$this->model->checkEmReg($this->postvars['email'])) {
							$this->registry->router->header( "index.php?route=users/registration", "&regmessage=emexists" );
							exit;
						}
						if(!$this->model->checkUser($this->postvars['uname'])) {
							$this->registry->router->header( "index.php?route=users/registration", "&regmessage=uexists" );
							exit;
						}
						if($this->postvars['password2'] != $this->postvars['password']) {
							$this->registry->router->header( "index.php?route=users/registration", "&regmessage=pass");
							exit;
						} 
						$this->model->save($this->postvars, 'users', 'add');
						$insid = $this->model->getInsertId();
						$this->postvars['userid'] = $insid;
						$this->model->save($this->postvars, 'users_roles_rel', 'add');
						$_POST['openinviter'] = true;
						$this->registry->template->postvars = $this->postvars;
						
						require_once( __LIB.DS.'mail.class.php' );
						$mail = new Mail();
						$toemail = $this->postvars['email'];
						$subject = "Activation for ".$this->registry->sitename;
						$message = "Hello, thanks for registering to ".$this->registry->sitename."!. "."\r\n".
									"Your account will be activated once you click the link below. Hope your stay at ".$this->registry->sitename.
									" is productive!"."\r\n"."\r\n".
									$this->registry->url."/index.php?route=users/activate/".$insid;
						$fromemail = $this->registry->admin->adminemailex;
						$headers = "From: $fromemail\r\nReply-To: $fromemail\r\nReturn-Path: $fromemail\r\n";
						try {
							$mail->sendEmail($toemail, $subject, $message, $headers);
							$this->registry->template->message = "We have sent an email to you.<br /><br />Click the link in the email to finish the registration process.";
						} catch(Exception $e) {
							$this->registry->template->message = "Incomplete Registration! You have been registered, but we could not send you email to finish your registration to the website. Please contact webmaster immediately.";
						}
						unset($_SESSION[$sitename]['regform_token']);
					} else {
						$this->registry->template->message = "Sorry, but you cannot submit twice!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
				$this->registry->template->postvars = $this->postvars;
				$this->registry->template->title = "Step 2 - Proceed to activate";
				$this->registry->template->intro = "";
				$this->registry->template->show( 'thankyou' );
			} else {
				$this->registry->template->message = "Sorry, but the entered security code was incorrect!";
				$this->registration();
			}
		} else {
			$this->registry->template->title = "Thank you";
			$this->registry->template->intro = "";
			$this->registry->template->show( 'thankyou' );
		}
	}
	
	public function thankyou() {
		$this->registry->template->title = "Thanks for registering with ".$this->registry->sitename;
		$this->registry->template->intro = "";
		$this->registry->template->loginmessage = '';
		$this->registry->template->show( 'thankyou' );
	}
	
	public function activate($id=0) {
		$model = new UsersModel();
		try {
			$model->activateUser($id);
		} catch(Exception $e) {
			$this->registry->template->message = "Problem activating your account. Please contact administrator immediately!";
		}
		$this->registry->template->title = "Activation";
		$this->registry->template->intro = "Your account is now active ".$this->registry->sitename.'!';
		$this->registry->template->loginmessage = '';
		$this->registry->template->show( 'thankyou' );
	}
	
	public function password_form() {
		if(isset($this->httpvars['sendpassreq'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['fform_token']) && isset($_SESSION[$sitename]['fform_token'])) {
				if($this->httpvars['fform_token'] == $_SESSION[$sitename]['fform_token']) {
					$model = new UsersModel();
					try {
						$user = $model->getUser($this->httpvars['uemail']);
						if(!$user)
							$this->registry->template->message = "No records with the given email";
						else {
							$_SESSION['passresponse'] = true;
							$from = $this->registry->admin->adminex;
							$subject = "Change password request";
							$body = "Hello {$user['user_name']}, please click the following link to change your password "."\r\n"."\r\n".
										$this->registry->url."/index.php?route=users/change_password/{$user['user_password']}";
							$to = $user['user_email'];
							mail($to, $subject, $body, 
									"From: $from\r\nReply-to: $from\r\nReturn-path: $from");	
							$this->registry->router->header( "index.php?route=users/pass_response", "" );
							exit();
						}
					} catch(Exception $e) {
						$this->registry->template->message = $e->getMessage();
					}
					unset($_SESSION[$sitename]['fform_token']);
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Sorry, but you cannot submit twice!";
			}
		}
		$this->registry->template->loginmessage = "";
		$this->registry->template->title = "Forgot password?";
		$this->registry->template->intro = "Change your lost password";
		$this->registry->template->show( 'forgotpassword' );
	}

	public function pass_response() {
		if(isset($_SESSION['passresponse']) && $_SESSION['passresponse'] == true) {
			$this->registry->template->message = "We have sent an email to you regarding the changing of your password. Please follow the directions mentioned in the email";
			unset($_SESSION['passresponse']);
		} else {
			$this->registry->template->message = "Unauthorized access";
		}
		$this->registry->template->loginmessage = "";
		$this->registry->template->title = "Change Password";
		$this->registry->template->show( 'passresponse' );
	}
	
	public function change_password($password='') {
		$model = new UsersModel();
		if(isset($this->httpvars['changepass'])) {
			$this->httpvars['oldpassword'] = $password;
			try {
				$model->save($this->httpvars, 'users_pass', 'edit');
				$this->registry->router->header( "index.php?route=users/pass_changed", "&msg=success" );
				exit();
			} catch(Exception $e) {
				$this->registry->template->message = $e->getMessage();
			}
		}
		if(empty($password)) {
			$this->registry->template->message = "Unauthorized access!";
		}
		try {
			$user = $model->getPassUser($password);
			if(!$user)
				$this->registry->template->message = "Unauthorized access!";
			else {
				$this->registry->template->password = $password;
				$this->registry->template->username = $user['username'];
			}
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->title = "Change password";
		$this->registry->template->intro = "Hi {$user['username']}! Please use the following form to change your password.";
		$this->registry->template->show( 'changepassword' );
	}
	
	public function change_userpassword($userid=0) {
		$model = new UsersModel();
		if(isset($this->httpvars['changepass'])) {
			if(isset($this->httpvars['form_token']) && isset($_SESSION['form_token'])) {
				if($this->httpvars['form_token'] == $_SESSION['form_token']) {
					// TODO CODE
					if(!empty($this->httpvars['newpassword']) && !empty($this->httpvars['newpassword2'])) {
						if($this->httpvars['newpassword'] == $this->httpvars['newpassword2']) {
							$this->httpvars['userid'] = $userid;
							try {
								$model->save($this->httpvars, 'members_pass_user', 'edit');
								$this->registry->router->header( "index.php?route=users/pass_changed", "" );
							} catch(Exception $e) {
								$this->registry->template->message = $e->getMessage();
							}
							unset($_SESSION['form_token']);
						} else {
							$this->registry->template->message = "Passwords do not match!";
						}
					} else {
						$this->registry->template->message = "Please fill in the new password twice!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			}
		}
		if(empty($userid)) {
			$this->registry->template->message = "Unauthorized access!";
		}
		try {
			$username = $model->getUsername($userid);
			$user = $model->getTheUser($username);
			if(!$user)
				$this->registry->template->message = "Unauthorized access!";
			else {
				$this->registry->template->password = $user['user_password'];
				$this->registry->template->username = $user['user_name'];
			}
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->title = "Change password";
		$this->registry->template->intro = "Hi {$user['user_name']}! Please use the following form to change your password";
		$this->registry->template->show( 'changepassword' );
	}
	
	public function pass_changed() {
		$this->registry->template->title = "Password changed!";
		$this->registry->template->intro = "You have successfully changed your password!";
		$this->registry->template->show( 'passchanged' );
	}
	
	public function login() {
		if($this->registry->user->isLoggedIn) {
			$this->registry->router->header( 'index.php', '?route=users/mypage' );
			exit();
		}
		$this->registry->template->title = "Login";
		$this->registry->template->intro = "";
		$this->registry->template->show('login');
	}
	
	public function do_login() {
        if($this->registry->user->isLoggedIn) {
        	$this->registry->router->header( 'index.php', '?route=users/mypage' );
        	exit();
        }
		$model = new UsersModel();
		$this->registry->template->title = "Login";
		if(isset($this->httpvars['login']) || isset($this->httpvars['login_x'])) {
			if(empty($this->postvars['username']) || empty($this->postvars['password'])) {
				$message = "empty";
				$this->registry->router->header( "index.php?route=users/login", "&message=".$message );
				exit;
			} else {
				if(!$model->checkLogin($this->postvars)) {
					$message = "auth";
					$this->registry->router->header( "index.php?route=users/login", "&message=".$message);
					exit;
				} else {
					$site = $this->registry->sitename;
					$row = $model->getRow();
					$_SESSION[$site.'_user_is_logged_in'] = true;
					$_SESSION[$site.'_userid'] = $row['user_id'];
					$_SESSION[$site.'_username'] = $row['user_name'];
					$_SESSION[$site.'_location'] = $row['user_location'];
					$model->setSession($row['user_id']);
					//$this->registry->router->setTheCookie();
					//$this->registry->cookie = $this->registry->router->ck;
					$this->registry->user->isLoggedIn = true;
					$this->registry->user->userid = $row['user_id'];
					$this->registry->user->username = $row['user_name'];
					$this->registry->user->userrole = $row['user_role_name'];
					if($this->registry->userrole == 'superadmin') $_SESSION[$site.'_superadmin_is_logged_in'] = true;
					$this->registry->location = $row['user_location'];
					$this->registry->router->header( 'index.php', '?route=users/mypage' );
				}
			}
		} else {
			$this->registry->template->title = "Unauthorized access!";
			$this->registry->template->message = "You are trying to enter into a restricted area!";
			$this->registry->template->show( 'unauthorized' );
		}
	}
	
	public function mypage() {
        if($this->registry->user->isLoggedIn == false) {
        	$this->registry->router->header( 'index.php', '?route=users/login' );
        	exit();
       	}
		$model = new UsersModel();
		if(!$this->registry->userid) {
			$this->registry->template->title = "Unauthorized access";
			$this->registry->template->message = "Either your session has ended or you are trying to enter a restricted area!";
			$this->registry->template->show( 'unauthorized' );
		} else {
			$this->registry->template->title = "Hi ".$this->registry->username.", what are you doing today?";
			$this->registry->template->intro = "Welcome to your personal section";
			$this->registry->template->show( 'mypage' );
		}
	}
	
	public function logout() {
		$model = new UsersModel();
		try {
			//$model->closeSession($this->registry->userid);
		} catch(Exception $e) {
			$this->registry->template->message = $e->getMessage();
		}
		$site = $this->registry->sitename;
		unset($_SESSION[$site.'_user_is_logged_in']);
		unset($_SESSION[$site.'_username']);
		unset($_SESSION[$site.'_userimage']);
		unset($_SESSION[$site.'_userid']);
		unset($_SESSION[$site.'_superadmin_is_logged_in']);
		unset($_COOKIE[$site.'_cookie']);
		$this->registry->template->title = "Logged out!";
		$this->registry->template->message = "You have successfully logged out!";
		//$this->registry->template->show( 'login' );
		$this->registry->router->header( 'index.php' );
	}
	
	public function sessionout() {
		$this->registry->template->title = "Login";
		$this->registry->template->intro = "Your session has been timed out. Please login again!";
		$this->registry->template->show( 'sessionout' );
	}
	
	public function add_image() {
		$model = new UsersModel();
		if(isset($this->httpvars['form_token']) && isset($_SESSION['form_token'])) {
			if($this->httpvars['form_token'] == $_SESSION['form_token']) {			
				if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
					$imgname = $_FILES['image']['name'];
					$imgtmpname = $_FILES['image']['tmp_name'];
					$imgsize = $_FILES['image']['size'];
					$imgtype = $_FILES['image']['type'];
					
					$ext = substr(strrchr($imgname, '.'), 1);
					$randname = md5(rand() * time());
					$imgname = strtolower($randname.'.'.$ext);
					$uploaddir = 'images/profiles/';
					$imgpath = $uploaddir.$imgname;
					$thumbspath = $uploaddir.DS."thumbs".DS.$imgname;
					move_uploaded_file($imgtmpname, $imgpath);
					require_once( __API.DS.'imageresize.php' );
					$resizer = new ImageTransform();
					$resizer->crop($imgpath, 300, 300, $imgpath);
					$resizer->crop($imgpath, 45, 45, $thumbspath);
					$_SESSION['txiacc_userimage'] = $imgname;
					$vars['userid'] = $this->registry->userid;
					$vars['image'] = $imgname;
					try {
						$model->save($vars, 'members_image', 'add');
					} catch(Exception $e) {
						$this->registry->template->message = $e->getMessage();
					}
				} else {
					$this->registry->template->message = "Error uploading image";
				}
			}
			unset($_SESSION['form_token']);
		}
		$this->registry->router->header( "index.php?route=users/city", "" );
	}
	
	public function update_profile() {
		$model = new UsersModel();
		$msg = '';
		if(isset($this->httpvars['update'])) {
			$sitename = $this->registry->sitename;
			if(isset($this->httpvars['prform_token']) && isset($_SESSION[$sitename]['prform_token'])) {
				if($this->httpvars['prform_token'] == $_SESSION[$sitename]['prform_token']) {
					if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
						$imgname = $_FILES['image']['name'];
						$imgtmpname = $_FILES['image']['tmp_name'];
						$imgsize = $_FILES['image']['size'];
						$imgtype = $_FILES['image']['type'];
						
						$ext = substr(strrchr($imgname, '.'), 1);
						$ext = strtolower($ext);
						if($ext != 'jpg' && $ext != 'png' && $ext != 'bmp') {
							$this->registry->router->header( "index.php?route=users/mypage", "&message=imgerror" );
							exit;
						}
						$randname = md5(rand() * time());
						$imgname = strtolower($randname.'.'.$ext);
						$uploaddir = 'images/profiles/';
						if(file_exists($uploaddir.$this->registry->user->userimage)) unlink($uploaddir.$this->registry->user->userimage);
						$imgpath = $uploaddir.$imgname;				
						$thumbspath = $uploaddir.DS."thumbs".DS.$imgname;
						if(is_writable($uploaddir) && is_writable($uploaddir.DS."thumbs")) {
							move_uploaded_file($imgtmpname, $imgpath);
							require_once( __LIB.DS.'imageresize.class.php' );
							$resizer = new ImageTransform();
							$resizer->crop($imgpath, 300, 300, $imgpath);
							$resizer->crop($imgpath, 45, 45, $thumbspath);
							$_SESSION[$sitename.'_userimage'] = $imgname;
							$vars['image'] = $imgname;
						} else {
							$vars['image'] = $this->httpvars['oimg'];
							$msg = '(Images folder is not writable) ';
						}
					} else {
						$vars['image'] = $this->httpvars['oimg'];
					}
					$this->httpvars['image'] = $vars['image'];
					$this->httpvars['userid'] = $this->registry->user->userid;
					try {
						$model->save($this->httpvars, 'users', 'edit');
						$msg .= "Success";
						//$_SESSION[$sitename.'_userid'] = $this->httpvars['userid'];
						$_SESSION[$sitename.'_username'] = $this->httpvars['uname'];
						$_SESSION[$sitename.'_name'] = $this->httpvars['uname'];
						$_SESSION[$sitename.'_userimage'] = $this->httpvars['image'];
					} catch(Exception $e) {
						$msg .= $e->getMessage();
					}
					$this->registry->template->message = $msg;
				}
				unset($_SESSION[$sitename]['pform_token']);
			}
			//$this->edit_profile($this->registry->user->userid);
			$this->registry->router->header( 'index.php?route=users/edit_profile/'.$this->registry->user->userid, '&msg='.$msg );
			exit();
		} else {
			$this->mypage();
		}
		/*$this->registry->router->header( "index.php?route=users/edit_profile/".$this->registry->userid, "&message=$msg" );
		exit();*/
		//$this->profile($this->registry->user->username);
	}
	
	public function unauthorized() {
		$this->registry->template->loginmessage = "";
		$this->registry->template->title = "Unauthorized access";
		$this->registry->template->intro = "You are trying to access a restricted page";
		$this->registry->template->show( 'unauthorized' );
	}
	
	public function imagev() {
		$this->registry->template->show_( 'imagev' );
	}

	public function remove_photo($pic='empty') {
		$pic = $pic.'.jpg';
		$path = "images".DS."profiles".DS;
		if(file_exists($path.$pic)) unlink($path.$pic);
		if(file_exists($path."thumbs".DS.$pic)) unlink($path."thumbs".DS.$pic);
		$this->registry->template->message = "<p class=\"small\">Picture removed</p>";
		$this->registry->template->show_( 'message' );
	}
	
	public function profile($myuser=null) {
		if($this->registry->user->isLoggedIn) {
			$model = new UsersModel();
			try {
				$exists = $model->checkIdUser($myuser);
			} catch(Exception $e) {
				$exists = 0;
				$this->registry->template->message = $e->getMessage();
			}
			if(!$exists) {
				echo '<div style="height: 600px;"><p class="red">User does not exist!</p></div>';
			} else {
				try {
					$user = $model->getIdUser($myuser);
					$username = $user['user_name'];
					if(empty($username)) {
						$this->registry->template->title = "Empty";
						$this->registry->template->message = "User not found!";
					} else {
						$this->registry->template->title = "Profile - $username";
						$this->registry->template->user = $user;
					}
				} catch(Exception $e) {
					$this->registry->template->message = $e->getMessage();
					$this->registry->template->user = null;
				}
				
				$this->registry->template->intro = "Welcome to the profile section";
				$this->registry->template->show( 'profile' );
			}
		} else {
			$this->unauthorized();
		}
	}
	
	public function edit_profile($userid=0) {
		if($this->registry->isLogged) {
			$model = new UsersModel();
			
			try {
				$username = $model->getUsername($userid);
				try {
					$user = $model->getUser($username);
					$this->registry->template->user = $user;
				} catch(Exception $e) {
					$this->registry->template->user = 0;
					$this->registry->template->message = $e->getMessage();
				}
			} catch(Exception $e) {
				$this->registry->template->username = null;
				$this->registry->template->message = $e->getMessage();
			}
			$this->registry->template->title = "Edit Profile - $username";
			$this->registry->template->intro = "Edit your record details";
			$this->registry->template->edit = true;
			$this->registry->template->show( 'profile' );
		} else {
			$this->unauthorized();
		}
	}
	
	public function background($userid=0) {
		if($this->registry->isLogged) {
			$model = new UsersModel();
			if(isset($this->httpvars['save'])) {
				$site = $this->registry->sitename;
				if(isset($this->httpvars['bgform_token']) && isset($_SESSION[$site]['bgform_token'])) {
					if($this->httpvars['bgform_token'] == $_SESSION[$site]['bgform_token']) {
						if(isset($_FILES['bgimage']['name']) && !empty($_FILES['bgimage']['name'])) {
							$bgname = $_FILES['bgimage']['name'];
							$tmpname = $_FILES['bgimage']['tmp_name'];
							$uploaddir = __ROOT.DS.'images'.DS.'bg';
							$ext = substr(strrchr($bgname, '.'), 1);
							$randname = md5(rand() * time());
							$bgname = $randname.'.'.$ext;
							move_uploaded_file($tmpname, $uploaddir.DS.$bgname);
							if($this->httpvars['tile'] == 'yes') $tile = 1;
							else $tile = 0;
							$vars = array("bg_image" => $bgname, "tile" => $tile, "userid" => $this->registry->userid);
							try {
								$model->save($vars, 'members_bg', 'default');
								$this->registry->router->header( 'index.php?route=users/background/'.$userid, '&message=bgsuccess' );
							} catch(Exception $e) {
								$this->registry->router->header( 'index.php?route=users/background/'.$userid, '&message=bgerror' );
							}
						} else {
							$this->registry->router->header( 'index.php?route=users/background/'.$userid, '&message=bgerror' );
						}
						unset($_SESSION[$site]['form_token']);
					} else {
						$this->registry->router->header( 'index.php/route=users/background/'.$userid, '' );
					}
				}
			}
			$this->registry->template->title = "Background image";
			$this->registry->template->intro = "Change how your background looks";
			$this->registry->template->show( 'background' );
		} else {
			$this->unauthorized();
		}
	}
};
