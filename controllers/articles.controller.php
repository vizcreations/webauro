<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Articles Controller Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations, Kevin Waterson
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ArticlesController extends Controller {
	private $page = 1;
	private $rowsperpage = 7;
	
	public function index() {
		$this->registry->router->header( 'index.php', '' );
		exit();
	}
	
	public function full_article($id=0) {
		if($id > 0) {
			$amodel = new ArticlesModel();
			try {
				$article = $amodel->getArticle($id);
                if(!$article) {
                	$article = array(
                					"article_id" => 0, 
                					"article_title" => "404 Error", 
                					"article_author" => "Anonymous",
                					"article_content" => "Article content not found.",
                					"ar_category_name" => "Unknown",
                					"ar_category_id" => 6,
                					"timestamp" => '0000-00-00 00:00'
                				);
                    $this->registry->template->message = "This article doesn't exist in the database!";
                }
				$articlecontent = $article['article_content'];
                $comments = $amodel->getArticleComments($id);
			} catch(Exception $e) {
				$article = array();
				$comments = array();
				$this->registry->template->message = $e->getMessage();
			}
			$this->registry->template->comments = $comments;
			$this->registry->template->article = $article;
			$this->registry->template->articlecontent = nl2br($articlecontent);
			$this->registry->template->title = $article['article_title'];
			$this->registry->template->intro = "";
			$this->registry->template->show( 'fullarticle' );
		} else {
			$this->index();
		}
	}
	
	public function latest_category_articles($catid=0) {
		$amodel = new ArticlesModel();
		$offset = ($this->page - 1) * $this->rowsperpage;
		try {
			$category = $amodel->getCategory($catid);
                        if(!$category) {
                                $category = array("ar_category_name" => "Category not found");
                                $this->registry->template->message = "This category doesn't exist in the database.";
                        }
                        $articles = $amodel->getCategoryArticles($catid, $offset, $this->rowsperpage);
			$total = $amodel->getCatArticlesCount($catid);
			$maxpage = ceil($total / $this->rowsperpage);
		} catch(Exception $e) {
			$category = array("ar_category_name" => $e->getMessage());
			$articles = array();
			$total = 0;
			$maxpage = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->catid = $catid;
		$this->registry->template->category = $category;
		$this->registry->template->articles = $articles;
		$this->registry->template->page = $this->page;
		$this->registry->template->maxpage = $maxpage;
		$this->registry->template->title = $category['ar_category_name'];
		$this->registry->template->intro = "Latest articles";
		$this->registry->template->show( 'latestarticles' );
	}
	
	public function page_latest_category_articles($catid=0) {
		$this->page = $this->httpvars['page'];
		$amodel = new ArticlesModel();
		$offset = ($this->page - 1) * $this->rowsperpage;
		try {
			$category = $amodel->getCategory($catid);
			$articles = $amodel->getCategoryArticles($catid, $offset, $this->rowsperpage);
			$total = $amodel->getCatArticlesCount($catid);
			$maxpage = ceil($total / $this->rowsperpage);
		} catch(Exception $e) {
			$category = array("ar_category_name" => $e->getMessage());
			$articles = 0;
			$total = 0;
			$maxpage = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->catid = $catid;
		$this->registry->template->category = $category;
		$this->registry->template->articles = $articles;
		$this->registry->template->page = $this->page;
		$this->registry->template->maxpage = $maxpage;
		$this->registry->template->title = $category['ar_category_name'];
		$this->registry->template->intro = "Latest articles";
		$this->registry->template->show( 'latestarticles' );
	}
	
	public function latest_articles() {
		$amodel = new ArticlesModel();
		try {
			$articles = $amodel->getLatestArticles();
		} catch(Exception $e) {
			$articles = 0;
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->template->articles = $articles;
		$this->registry->template->title = "Latest articles";
		$this->registry->template->intro = "Newly created articles";
		$this->registry->template->show( 'latestarticles' );
	}
	
	public function save_comment($articleid=0) {
		if(isset($this->httpvars['savecomment'])) {
			$number = $this->httpvars['imgv'];
			if(md5($number) == $_SESSION['image_random_value']) {
				if(isset($this->httpvars['comform_token']) && isset($_SESSION['comform_token'])) {
					if($this->httpvars['comform_token'] == $_SESSION['comform_token']) {
						if(!empty($this->httpvars['articlecommentname']) && !empty($this->httpvars['articlecommentcontent'])) {
							$amodel = new ArticlesModel();
							$this->httpvars['articleid'] = $articleid;
							$amodel->save($this->httpvars, 'comment', 'add');
							$this->httpvars['arcommentid'] = $amodel->getInsertId();
							$amodel->save($this->httpvars, 'comment_rel', 'add');
							require_once( __LIB.DS.'mail.lib.php' );
							$mailer = new Mail();
							$toemail = "viz@vizcreations.com";
							$fromemail = $this->httpvars['articlecommentemail'];
							$subject = "Your article has a comment.";
							$message = "Check out this new comment for the article http://www.vizcreations.com/index.php?route=articles/full_article/".$this->httpvars['articleid'];
							$headers = "From: $fromemail\r\nReply-to: $fromemail\r\nReturn-path: $fromemail\r\n";
							try {
								$mailer->sendEmail($toemail, $subject, $message, $headers);
							} catch(Exception $e) {
								$this->registry->template->message = $e->getMessage();
							}
							unset($_SESSION['comform_token']);
							unset($_SESSION['image_random_value']);
						} else {
							$this->registry->template->message = "Please make sure you enter mandatory fields!";
						}
					} else {
						$this->registry->template->message = "Sorry, but you cannot submit twice!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Incorrect image code. Enter again!";
			}
		}
		$this->full_article($articleid);
	}
};
