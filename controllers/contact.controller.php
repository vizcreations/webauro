<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Contact Controller Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ContactController extends Controller {
	public function index() {
		$xmlDoc = new DOMDocument();
		$xmlFile = __ROOT.DS.'webauro'.DS.'contact.xml';
		if(file_exists($xmlFile)) {
			$xmlDoc->load($xmlFile);
			$name = $xmlDoc->getElementsByTagName( 'name' )->item(0)->nodeValue;
			$email = $xmlDoc->getElementsByTagName( 'email' )->item(0)->nodeValue;
			$telephone = $xmlDoc->getElementsByTagName( 'telephone' )->item(0)->nodeValue;
			$address = $xmlDoc->getElementsByTagName( 'address' )->item(0)->nodeValue;
		} else {
			$name = $this->registry->sitename;
			$email = $this->registry->admin->adminemailex;
			$telephone = 00000000;
			$address = 'Foobar';
		}

		$this->registry->template->name = $name;
		$this->registry->template->email = $email;
		$this->registry->template->telephone = $telephone;
		$this->registry->template->address = nl2br($address);
		$this->registry->template->title = "Contact";
		$this->registry->template->intro = "Contact us using details or the form";
		$this->registry->template->show( 'contactform' );
	}

	public function contact_form() {
		$this->index();
	}

	public function submit() {
		$postd = $this->registry->session->postvars;
//		if(isset($this->httpvars['submitcontact'])) {
		if(isset($postd->submitcontact)) {
//			$number = $this->httpvars['imgv'];
			$number = $postd->imgv;
			$sitename = $this->registry->sitename;
//			if(md5($number) == $_SESSION[$sitename]['image_random_value']) {
			if(isset($this->registry->session->site->image_random_value)
			&& md5($number) == $this->registry->session->site->image_random_value) {
				$site = $this->registry->sitename;


//				if(isset($this->httpvars['conform_token']) 
				if(isset($postd->conform_token)
				//&& isset($_SESSION[$site]['conform_token'])
				&& isset($this->registry->session->site->conform_token)
				) {
//					if($this->httpvars['conform_token'] == $_SESSION[$site]['conform_token']) {
					if($postd->conform_token == $this->registry->session->site->conform_token) {
//						if(!empty($this->httpvars['contactname']) && !empty($this->httpvars['contactmessage'])) {
						if(!empty($postd->contactname)
						&& !empty($postd->contactmessage)
						) {
							$xmlDoc = new DOMDocument();
							$xmlFile = __ROOT.DS.'contact.xml';
							if(file_exists($xmlFile)) {
								$xmlDoc->load($xmlFile);
								$toEmail = $xmlDoc->getElementsByTagName('email')->item(0)->nodeValue;
							} else {
								$toEmail = $this->registry->admin->adminemailex;
							}
/*							$fromEmail = $this->httpvars['contactemail'];
							$subject = $this->httpvars['contactsubject'];
							$messagebody = $this->httpvars['contactmessage'];
*/
							$fromEmail = $postd->contactemail;
							$subject = $postd->contactsubject;
							$messagebody = $postd->contactmessage;
							$headers = "From: $fromEmail\r\nReply-To: $fromEmail\r\nReturn-Path: $fromEmail\r\n";
							$ret = mail($toEmail, $subject, $messagebody, $headers);
							if($ret!==FALSE){
								$this->registry->template->message = "Email sent successfull!";
							} else {
								$this->registry->template->message = "Technical problem sending email!";
							}

							unset($_SESSION[$site]['conform_token']); // Unset from the global array
						} else {
							$this->registry->template->message = "Name and message are mandatory!";
						}
					} else {
						$this->registry->template->message = "Sorry, but you cannot submit twice!";
					}
				} else {
					$this->registry->template->message = "Sorry, but you cannot submit twice!";
				}
			} else {
				$this->registry->template->message = "Invalid code entered! Try again.";
			}
		}
		$this->contact_form();
	}
};

