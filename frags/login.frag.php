<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Login Frag Class to display the login snippet form
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class LoginFrag extends Frag {
	public function index() {
		
	}
	
	public function loadFrag($fragname='none') {
		$model = new LoginFragModel();
		try {
			$enabled = $model->getFrag($fragname);
		} catch(Exception $e) {
			$this->registry->fragtemplate->message = $e->getMessage();
			$enabled = false;
		}
		if($enabled) {
			if($this->registry->isLogged) {
				$template = 'logout';
			} else {
				$template = 'login';
			}
			$this->registry->fragtemplate->frag = $template;
			$this->registry->fragtemplate->show( $template );
		}
	}
};

