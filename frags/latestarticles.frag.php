<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Latest Articles Frag Class to display list of articles from a given category
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class LatestArticlesFrag extends Frag {
	public function loadFrag($fragname='none') {
		$afrmodel = new LatestArticlesFragModel();
		try {
			$fragargument = $afrmodel->getArgument($fragname);
		} catch(Exception $e) {
			$fragargument = 0;
			$this->registry->fragtemplate->message = $e->getMessage();
		}
		if(empty($fragargument)) {
			$fragargument = 0;
		}
		try {
			$articles = $afrmodel->getCatArticles($fragargument);
		} catch(Exception $e) {
			$articles = 0;
			$this->registry->fragtemplate->message = $e->getMessage();
		}
		try {
			$frag = $afrmodel->getFrag($fragname);
		} catch(Exception $e) {
			$frag = array("frag_name" => "Error");
			$this->registry->template->message = $e->getMessage();
		}
		$this->registry->fragtemplate->fragargument = $fragargument;
		$this->registry->fragtemplate->frag = $frag;
		$this->registry->fragtemplate->articles = $articles;
		$this->registry->fragtemplate->show( 'latestarticles' );
	}
};

