<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Breadcrumbs Frag Class to apply logic and deliver variables to breadcrumbs fragment
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class BreadcrumbsFrag extends Frag {
    public function loadFrag($fragname='none') {
        $httpvars = $_REQUEST;
		$model = new BreadcrumbsFragModel();
        try {
			$fragdata = $model->getFrag($fragname);
			if(!$fragdata) $fragdata = 0;
		} catch(Exception $e) {
			$fragdata = '';
			$this->registry->fragtemplate->message = $e->getMessage();
		}
        $controller = $this->registry->router->controller;
        $action = $this->registry->router->action;
        $id = $this->registry->router->id;
        if($controller == 'index') $cont = '';
        else $cont = '&nbsp;<a href="index.php?route='.$controller.'">'.ucfirst($controller).'</a>&nbsp;&raquo;';
        if($action == 'index' || empty($action) && empty($id)) $act = '';
        else if(!empty($id)) $act = '&nbsp;<a href="index.php?route='.$controller.'/'.$action.'/'.$id.'">'.ucfirst($action).'</a>&nbsp;&raquo;';
        else $act = '&nbsp;<a href="index.php?route='.$controller.'/'.$action.'">'.ucfirst($action).'</a>&nbsp;&raquo;';

        $id = str_replace("---", "-", $id);
        if($id == '' || empty($id)) $arg = '';
        else $arg = '&nbsp;<a href="index.php?route='.$controller.'/'.ucfirst($action).'/'.$id.'">'.str_replace("db;", "\"", str_replace("amp;", "&", str_replace("-", " ", $id))).'</a>&nbsp;&raquo;';
        $arg = str_replace("amp;", "&", str_replace("-", " ", $arg));
        $act = '';
        $cont = '';

                
        $fragdata = array("ofragdata" => $fragdata, "controller" => $cont, "action" => 
                                    $act, "id" => $arg);
		$this->registry->fragtemplate->fragdata = $fragdata;
		$this->registry->fragtemplate->show( 'breadcrumbs' );
	}
};

