<?php

if( !defined("__ROOT") ) die( "Unauthorized access!" );

/**
 * @abstract Front End CHMOD Library Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations, varied
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */


class CHMOD {
	public $file;
	public $dir;
	protected $mod;
	private $ftp_details;
	
	function __construct() {
		$this->ftp_details = null;
	}
		
	function chmod_11oo10($path, $mod) {
		$this->mod = mode;
		$this->dir = $path;
	    // extract ftp details (array keys as variable names)
	    extract ($this->ftp_details);
	    
	    // set up basic connection
	    $conn_id = ftp_connect($ftp_server);
	    
	    // login with username and password
	    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	    
	    // try to chmod $path directory
	    if (ftp_site($conn_id, 'CHMOD '.$mod.' '.$ftp_root.$path) !== false) {
	        $success=TRUE;
	    } 
	    else {
	        $success=FALSE;
	    }
	
	    // close the connection
	    ftp_close($conn_id);
	    return $success;
	}
}
