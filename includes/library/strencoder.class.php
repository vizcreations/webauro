<?php

/**
 * @abstract String manipulation Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */


/* Class definition for StrEncoder Class */

class StrEncoder {
	private $__hash = "";
	public $__string = "";
	private $__splchar_ptr = "/[^a-zA-Z0-9_-]/";
	private $__non_ascii_ptr = "/\p{Cc}+/u";

	public __construct($string) {
		$this->__string = $string;
	}

	public strEncode($str="abcd", $pattern=null) {
		$this->__string = $str;
		if($pattern == null) {
			return false;
		} else {
			$this->__string = preg_replace($pattern, $str);
		}
		return $this->__string;
	}

	public strDecode($str="dcba", $pattern=null) {
		$this->__string = $str;
		if($pattern == null) {
			return $this->__string;
		}
		return $this->__string;
	}

	public strEncodeSplChars($str) {
		$this->__string = preg_replace($this->__splchar_ptr, $str);
		return $this->__string;
	}

	public strEncodeNonASCII($str) {
		$this->__string = preg_replace($this->__non_ascii_ptr, $str);
		return $this->__string;
	}

};

