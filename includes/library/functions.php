<?php

if(!defined( "__ROOT" )) die( "Unauthorized access!" );

/**
 * @abstract Front End General PHP Functions
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations, varied
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */



/**
 * 
 * @param string
 * @return bool
 * @abstract Log messages for the application in general. This log function is called before every specific log
 */
function log_app($msg) {
	$logfile = __ROOT.DS."logs".DS."log_app.txt";
	if(file_exists($logfile))
		$fp = fopen( $logfile, "a+" );
	else
		$fp = fopen( $logfile, "w" );
	if($fp) {
		fwrite( $fp, $msg."\r\n", 256 );
		fclose( $fp );
		return true;
	} else {
		return false;
	}
}

/* Log messages for database connections and transactions */
function log_db($msg) {
	$logfile = __ROOT.DS."logs".DS."log_db.txt";
	if(file_exists($logfile))
		$fp = fopen( $logfile, "a+" );
	else
		$fp = fopen( $logfile, "w" );
		
	if($fp) {
		fwrite( $fp, $msg."\r\n", 256 );
		fclose( $fp );
		return true;
	} else {
		return false;
	}
}

function log_htaccess($msg) {
	// TODO CODE
}

/* Log messages for user activity throughout */
function log_user_activity($msg) {
	// TODO CODE
}
	
function floor_it($number) {
	return floor($number);
}

function get_formatted_time($time) {
	$postedtime = date("Hi", strtotime($time));
	$postedhour = date("H", strtotime($time));
	$postedminute = date("i", strtotime($time));
	$postedmonth = date("m", strtotime($time));
	$postedyear = date("Y", strtotime($time));
	$postedday = date("d", strtotime($time));
	$diff = date("jS M, g:i a", strtotime($time)).' '.$this->timeformat;
	$time = date("jS M, g:i a", strtotime($time)).' '.$this->timeformat;
	if($this->curwday == $postedday && $this->curmonth == $postedmonth && $this->curyear == $postedyear) {
		$thehrdiff = $this->curhours - $postedhour;
		if($thehrdiff == 0) {
			if($this->curminutes < $postedminute) $cmin = $this->curminutes + 60;
			$themindiff = $this->curminutes - $postedminute;
			if($themindiff == 0) $thediff = 'A few seconds back..';
			else if($themindiff == 1) $thediff = $themindiff.' minute back';
			else $thediff = $themindiff.' minutes back';
		} else if($thehrdiff == 1) {
			$themindiff = $this->curminutes - $postedminute;
			if($themindiff == 0) $thediff = 'A few seconds back..';
			else if($themindiff == 1) $thediff = $themindiff.' minute back';
			else if($themindiff >= 60) $thediff = $themindiff.' hour back';
			else $thediff = $themindiff.' minutes back';
			//$thediff = $thehrdiff.' hour back';
		} else $thediff = $thehrdiff.' hours back';
		$diff = $time;
	} else if(($this->curwday - $postedday) == 1 && $this->curmonth == $postedmonth && $this->curyear == $postedyear) {
		if($this->curhours < 05) {
			$chour = $this->curhours + 24;
			$thehrdiff = $chour - $postedhour;
			if($thehrdiff == 0) {
				if($this->curminutes < $postedminute) $cmin = $this->curminutes + 60;
				$themindiff = $this->curminutes - $postedminute;
				if($themindiff == 0) $thediff = 'A few seconds back..';
				else if($themindiff == 1) $thediff = $themindiff.' minute back';
				else $thediff = $themindiff.' minutes back';
			} else if($thehrdiff == 1)
				$thediff = $thehrdiff.' hour back';
				else if($thehrdiff >= 24) $thediff = 'Yesterday';
				else $thediff = $thehrdiff.' hours back';
		} else $thediff = $time;
		$diff = $time;
	} else if(($this->curwday - $postedday) == 1 && ($this->curmonth - $postedmonth) == 1 && $this->curyear == $postedyear) {
		if($this->curhours < 05) {
			$chour = $this->curhours + 24;
			$thehrdiff = $chour - $postedhour;
			if($thehrdiff == 0) {
				if($this->curminutes < $postedminute) $cmin = $this->curminutes + 60;
				$themindiff = $this->curminutes - $postedminute;
				if($themindiff == 0) $thediff = 'A few seconds back..';
				else if($themindiff == 1) $thediff = $themindiff.' minute back';
				else $thediff = $themindiff.' minutes back';
			} else if($thehrdiff == 1)
				$thediff = $thehrdiff.' hour back';
				else if($thehrdiff >= 24) $thediff = 'Yesterday';
				else $thediff = $thehrdiff.' hours back';
		} else $thediff = $time;
		$diff = $time;
	} else if(($this->curwday - $postedday) == 1 && ($this->curmonth - $postedmonth) == 1 && ($this->curyear - $postedyear) == 1) {
		if($this->curhours < 05) {
			$chour = $this->curhours + 24;
			$thehrdiff = $chour - $postedhour;
			if($thehrdiff == 0) {
				if($this->curminutes < $postedminute) $cmin = $this->curminutes + 60;
				$themindiff = $this->curminutes - $postedminute;
				if($themindiff == 0) $thediff = 'A few seconds back..';
				else if($themindiff == 1) $thediff = $themindiff.' minute back';
				else $thediff = $themindiff.' minutes back';
			} else if($thehrdiff == 1)
				$thediff = $thehrdiff.' hour back';
				else if($thehrdiff >= 24) $thediff = 'Yesterday';
				else $thediff = $thehrdiff.' hours back';
		} else $thediff = $time;
		$diff = $time;
	} else {
		$diff = $time;
	}
	return $diff;
}


	
function get_curl_resource($url){
 
    // is curl installed?
    if (!function_exists('curl_init')) {
    	$msg = "No curl installed. Line #152 in functions.php";
    	log_app( $msg );
    	die( $msg );
        return 0;
    }
 
    // create a new curl resource
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER, 0);
 
    /*
    Here you find more options for curl:
    http://www.php.net/curl_setopt
    */
 
    // set URL to download
    /*curl_setopt($ch, CURLOPT_URL, $url);
 
    // set referer:
    curl_setopt($ch, CURLOPT_REFERER, "http://www.google.com/");
 
    // user agent:
    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
 
    // remove header? 0 = yes, 1 = no
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // should curl return or print the data? true = return, false = print
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 1);*/
 
    // download the given URL, and return output
    $output = curl_exec($ch);
 
    // close the curl resource, and free system resources
    curl_close($ch);
 
    // print output
    return $output;
}

function get_meta_info($uri) {
	$meta = get_meta_tags($uri);
	return $meta;
}
