/**
 * @abstract Front End JavaScript - Library And Objects
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */


/* JavaScript functions and variables */

var cFrame;
var img, image, imgname, imagename, imgsrc;
var intVal, intVal2;
var speed = 10;
var height = 10;
var fading = false;
var width = 10;
var galOp = 20;
var sliderDiv;
var sliding = false;
var xmlHttp = getXmlHttpObject();
var newXmlHttp;
var chatDiv, chatFrame, top, left;
var theurl;
var thecount;
var theuserid;
var foobar;
var leftheight, ress;
var theimage, theresponse;
var photoCounter;

var json;
var xmlDoc;

function slide(id, speed) {
	sliderDiv = document.getElementById(id);
	if(sliding == false) {
		if(sliderDiv.style.height < 10) {
			sliderDiv.style.height++;
		}
	}
}

function zoomPhoto(id, img) {
	//var biggallery = document.getElementById('biggallery');
	var biggallery = document.getElementById('photo'+id);
	biggallery.innerHTML = '';
	var wrapper = document.getElementById('wrapper');
	var table = document.getElementById('imgtable'+id);
	var cells = table.rows[0].cells;
	for(i=0; i<cells.length; i++) {
		if((cells[i].innerHTML) == img) {
			var num = i;
			if((i-1) >= 0)
				prev = cells[(i-1)].innerHTML;
			else
				prev = cells[(cells.length)-1].innerHTML;
			if((i+1) < cells.length)
				next = cells[(i+1)].innerHTML;
			else
				next = cells[0].innerHTML;
		}
	}
	var src = 'photos/'+img+'.jpg';
	biggallery.style.display = 'block';
	if(biggallery.clientHeight < 200) {
		biggallery.style.height = '645px';
	}
	biggallery.style.width = '850px';
	biggallery.style.overflow = 'hidden';
	biggallery.style.position = 'absolute';
	biggallery.style.textAlign = 'center';
	biggallery.style.background = "#758494 url('../images/popupbg.jpg') 0 0 repeat-x";
	biggallery.style.border = '1px #DCDCDC solid';
	var left = document.getElementById('left');
	left.style.overflow = 'visible';
	//response = '<p class="para bold"><a class="white" href="javascript:hidePhoto('+id+');">Close</a></p>';
	response = '<table style="margin: 0 auto; text-align: center; width: 100%;"><tr>';
	response += '<td><a href="javascript:zoomPhoto('+id+', \''+prev+'\');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
	response += '<td><a class="bold small white" href="javascript:hidePhoto('+id+');">Close</a></td>';
	response += '<td><a href="javascript:zoomPhoto('+id+', \''+next+'\');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
	response += '</td></tr>';
	response += '</table>';
	response += '<table style="margin: 0 auto; text-align: center; width: 100%;">';
	response += '<tr><td align="center" style="text-align: center;">';
	response += '<img src="'+src+'" border="0" alt="" onclick="hidePhoto('+id+');" />';
	response += '</td></tr>';
	response += '</table>';
	biggallery.innerHTML = response;
	/*biggallery.style.left = (screen.width - 810) / 2+'px';
	biggallery.style.top = 10+'px';*/
	/*wrapper.style.opacity = '0.2';
	wrapper.style.mozOpacity = '0.2';
	wrapper.style.filter = 'alpha(opacity=20)';
	biggallery.style.opacity = '1.0';
	biggallery.style.mozOpacity = '1.0';
	biggallery.style.filter = 'alpha(opacity=100)';*/
	/*window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);*/
}

function sendAJAXReq(div, uri, theload) {
	request = getXmlHttpObject();
	if(request == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	if(theload == null)
		loader = document.getElementById('ajaxloader');
	else
		loader = theload;
            
        if(loader)
            loader.innerHTML = 'loading..';
	request.onreadystatechange = function() {
		if(request.readyState == 4 || request.readyState == 'complete') {
			if(request.status == 200) {
				div.innerHTML = request.responseText;
				leftheight = document.getElementById('left').clientHeight;
				/*if(leftheight < 422)
					document.getElementById('left').style.height = "500px";
				else
					document.getElementById('left').style.height = 'auto';*/
                                if(loader)
                                    loader.innerHTML = '';
			}
		}
	}
	request.open("GET", uri, true);
	request.send(null);
}

function sendAJAXRequest(url, sloader) {
	var theXmlHttp2 = getXmlHttpObject();
	if(theXmlHttp2 == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	if(sloader == null)
		loader = document.getElementById('ajaxloader');
	else
		loader = sloader;
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	theXmlHttp2.onreadystatechange = function() {
		if(theXmlHttp2.readyState == 4 || theXmlHttp2.readyState == 'complete') {
			if(theXmlHttp2.status == 200) {
				getAJAXResponse(theXmlHttp2.responseText);
				leftheight = document.getElementById('left').clientHeight;
				/*if(leftheight < 422)
					document.getElementById('left').style.height = "500px";
				else
					document.getElementById('left').style.height = 'auto';*/
				loader.innerHTML = '';
			}
		}
	}
	theXmlHttp2.open("GET", url, true);
	theXmlHttp2.send(null);
	return false;
}

function getAJAXResponse(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function zoomProfileImage(img, divid) {
	thediv = document.getElementById(divid);
	thediv.style.display = 'block';
	thediv.style.position = 'absolute';
	thediv.style.height = '302px';
	thediv.style.width = '302px';
	thediv.style.overflow = 'visible';
	thediv.style.border = '2px #DCDCDC solid';
	thediv.style.backgroundImage = "url('images/profiles/" + img + "')";
	thediv.style.backgroundPosition = "center";
	thediv.style.backgroundRepeat = "no-repeat";
}

function processAJAXRequest(uri, msg) {
	// TODO CODE
	xmlHttp = getXmlHttpObject();
	if(xmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
			if(xmlHttp.status == 200) {
				if(msg != null)
				window.alert(msg);
			}
		}
	}
	xmlHttp.open("GET", uri, true);
	xmlHttp.send(null);
}

function showFullVideo(id) {
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	biggallery.style.display = 'block';
	biggallery.style.width = (1080)+'px';
	biggallery.style.height = 'auto';
	biggallery.style.left = (screen.width - (1080)) / 2 + 'px';
	biggallery.style.top = '10px';
	biggallery.style.textAlign = 'center';
	biggallery.style.backgroundColor = '#555555';
	biggallery.style.border = '1px #DCDCDC solid';
	biggallery.style.align = 'center';
	uri = 'index.php';
	uri = uri + '?route=ajax/video_source/' + id;
	uri = uri + '&browser=' + navigator.appName;
	uri = uri + '&version=' + navigator.appVersion;
	uri = uri + '&sid=' + Math.random();
	table = document.getElementById('images');
	imagecells = table.rows[0].cells;
	for(i=0; i<imagecells.length; i++) {
		if(parseInt((imagecells[i].innerHTML)) == parseInt(id)) {
			if((i-1) >= 0)
				prev = imagecells[(i-1)].innerHTML;
			else
				prev = imagecells[(imagecells.length)-1].innerHTML;
			if((i+1) < imagecells.length)
				next = imagecells[(i+1)].innerHTML;
			else
				next = imagecells[0].innerHTML;
		}
	}
	xmlHttp = getXmlHttpObject();
	if(xmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
			if(xmlHttp.status == 200) {
				/*response = '<table><tr><td colspan="3" align="center"><a class="bold white" href="javascript:hideBigGallery();">Close</a></td></tr>';
				response += '<tr><td><a href="#"><img onclick="javascript:showFullVideo(\''+prev+'\');" src="images/left.png" border="0" alt="prev" /></a></td>';
				response += '<td>'+xmlHttp.responseText+'</td>';
				response += '<td><a href="#"><img onclick="javascript:showFullVideo(\''+next+'\');" src="images/right.png" border="0" alt="next" /></a></td></tr></table>';*/
				response = '<table><tr><td align="left">';
				response += '<table class="video"><tr>';
				response += '<td align="left" width="43%"><a href="javascript:showFullVideo('+prev+');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
				response += '<td align="left" width="23%"><a class="bold small white" href="javascript:hideBigGallery();">Close</a></td>';
				response += '<td align="center" width="33%"><a href="javascript:showFullVideo('+next+');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
				response += '</td></tr>';
				response += '<tr><td>' + xmlHttp.responseText + '</td></tr>';
				response += '</table>';
				biggallery.innerHTML = response;
			}
		}
	}
	xmlHttp.open("GET", uri, true);
	xmlHttp.send(null);
	/*wrapper.style.opacity = '0.2';
	wrapper.style.mozOpacity = '0.2';
	wrapper.style.filter = 'alpha(opacity=20)';*/
	loader.innerHTML = '';
	window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
	//return false;
}

function getPlaylist(catid) {
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	biggallery.style.display = 'block';
	biggallery.style.width = '616px';
	biggallery.style.height = 'auto';
	biggallery.style.overflow = 'hidden';
	biggallery.style.left = (screen.width - 610) / 2 + 'px';
	biggallery.style.top = '10px';
	biggallery.style.textAlign = 'center';
	biggallery.style.background = '#555555';
	biggallery.style.border = '1px #DCDCDC solid';
	biggallery.style.align = 'center';
	uri = 'index.php';
	uri = uri + '?route=ajax/video_playlist/' + catid;
	uri = uri + '&browser=' + navigator.appName;
	uri = uri + '&version=' + navigator.appVersion;
	uri = uri + '&sid=' + Math.random();
	xmlHttp = getXmlHttpObject();
	if(xmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
			if(xmlHttp.status == 200) {
				response = '<table><tr><td align="center"><a class="bold small white" href="javascript:hideBigGallery();">Close</a></td></tr>';
				//response += '<tr><td><a href="#"><img onclick="javascript:showFullVideoMain(\''+prev+'\');" src="images/left.png" border="0" alt="prev" /></a></td>';
				response += '<tr>';
				response += '<td>'+xmlHttp.responseText+'</td>';
				//response += '<td><a href="#"><img onclick="javascript:showFullVideoMain(\''+next+'\');" src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
				response += '</tr>';
				biggallery.innerHTML = response;
			}
		}
	}
	xmlHttp.open("GET", uri, true);
	xmlHttp.send(null);	
	/*wrapper.style.opacity = '0.2';
	wrapper.style.mozOpacity = '0.2';
	wrapper.style.filter = 'alpha(opacity=20)';*/
	loader.innerHTML = '';
	window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
	return false;
}

function getGallery(gid, cityid) {
	uri = 'index.php';
	uri = uri + '?route=imagegallery/gallery/' + gid;
	uri = uri + '&cityid=' + cityid;
	uri = uri + '&sid=' + Math.random();
	table = document.getElementById('gheadings');
	cells = table.rows[0].cells;
	cells[(parseInt(gid)-1)].style.background = '#F5F5F5';
	for(i=0; i<cells.length; i++) {
		if(i != (parseInt(gid)-1)) {
			cells[i].style.backgroundImage = "url('images/gheading.jpg')";
			cells[i].style.backgroundRepeat = "no-repeat";
		}
	}
	xmlHttp = getXmlHttpObject();
	if(xmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	var imload = document.getElementById('simload');
	if(imload)
		loader = imload;
	else
		loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
			if(xmlHttp.status == 200) {
				printGallery(xmlHttp.responseText);
				loader.innerHTML = '';
			}
		}
	}
	xmlHttp.open("GET", uri, true);
	xmlHttp.send(null);
}

function showImage(img) {
	var biggallery = document.getElementById('biggallery');
	var prev, next;
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	for(i=0; i<imagecells.length; i++) {
		if((imagecells[i].innerHTML) == img) {
			prev = imagecells[(i-1)];
			next = imagecells[(i+1)];
		}
	}
	response = '<a href="javascript:showBigGallery(\''+prev+'\');">Prev</a>';
	response += '<img src="admincontrol/images/image_gallery/' + img + '" border="0" alt="image" onclick="hideBigGallery();" />';
	response += '<a href="javascript:showBigGallery(\''+next+'\');">Next</a>';
	biggallery.innerHTML = response;
	loader.innerHTML = '';
}

function hideBigGallery() {
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	biggallery.innerHTML = '';
	biggallery.style.display = 'none';
	/*wrapper.style.opacity = '1.0';
	wrapper.style.filter = 'alpha(opacity=100)';
	wrapper.style.mozOpacity = '1.0';*/
	clearTimeout(timer);
	clearInterval(intVal);
	sliding = false;
	/*intVal2 = setInterval("closeImage()", speed);*/
}

function startSlideShow(im, imname, thespeed) {
	sliding = true;
	speed = thespeed;
	img = im;
	imagename = imname;
	var theimageTb = document.getElementById('theimages');
	var imageTb = document.getElementById('images');
	var theimagecells = theimageTb.rows[0].cells;
	var imagecells = imageTb.rows[0].cells;
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	var prev, next, previmg, nextimg;
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	for(i=0; i<imagecells.length; i++) {
		if((imagecells[i].innerHTML) == img) {
			if((i-1) >= 0)
				prev = imagecells[(i-1)].innerHTML;
			else
				prev = imagecells[(imagecells.length)-1].innerHTML;
			if((i+1) < imagecells.length)
				next = imagecells[(i+1)].innerHTML;
			else
				next = imagecells[0].innerHTML;
		}
	}
	for(i=0; i<theimagecells.length; i++) {
		if((theimagecells[i].innerHTML) == imagename) {
			if((i-1) >= 0)
				previmg = theimagecells[(i-1)].innerHTML;
			else
				previmg = theimagecells[(theimagecells.length)-1].innerHTML;
			if((i+1) < theimagecells.length)
				nextimg = theimagecells[(i+1)].innerHTML;
			else
				nextimg = theimagecells[0].innerHTML;
		}
	}
	for(i=0; i<imagecells.length; i++) {
		if(imagecells[i].innerHTML == img) {
			if(galOp == 20) {
				//intVal = setInterval("openImage()", 30);
			}
			if((i+1) < imagecells.length) {
				image = imagecells[(i+1)].innerHTML;
			} else {
				image = imagecells[0].innerHTML;
			}
			uri = 'index.php';
			uri += '?route=imagegallery/full_image/' + img;
			uri += '&sid=' + Math.random();
			imagename = nextimg;
			img = next;
			xmlHttp = getXmlHttpObject();
			sliding = true;
			speed = thespeed;
			
			if(xmlHttp == null) {
				return;
			}
			xmlHttp.onreadystatechange = function() {
				if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
					if(xmlHttp.status == 200) {
						biggallery.style.display = 'block';
						biggallery.style.width = (1080) + 'px';
						if(biggallery.clientHeight < 200) {
							biggallery.style.height = 'auto';
						}
						biggallery.style.left = (screen.width - (1080)) / 2 + 'px';
						biggallery.style.top = '10px';
						biggallery.style.background = '#555555';
						biggallery.style.border = '1px #F5F5F5 solid';
						var theresponse = xmlHttp.responseText;
						/*response = '<table class="image"><tr><td colspan="3" align="center"><a class="bold white" href="javascript:hideBigGallery();">Close</a></td></tr>';
						response += '<tr><td><a href="javascript:showBigGallery('+prev+');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
						response += '<td>' + theresponse + '</td>';
						response += '<td><a href="javascript:showBigGallery('+next+');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';*/
						response = '<table><tr><td align="center">';
						response += '<table class="image"><tr>';
						response += '<td><a href="javascript:showBigGallery('+prev+');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
						response += '<td><a class="bold small white" href="javascript:hideBigGallery();">Close</a></td>';
						response += '<td><a href="javascript:showBigGallery('+next+');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
						response += '</td></tr>';
						response += '<tr><td>' + theresponse + '</td></tr>';
						response += '</table>';
						biggallery.innerHTML = response;
						/*wrapper.style.opacity = '0.2';
						wrapper.style.mozOpacity = '0.2';
						wrapper.style.filter = 'alpha(opacity=20)';*/
						loader.innerHTML = '';
						window.location.hash = 'chatwindows';
						window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
						/*var iname = new Image();
						iname.src = 'admincontrol/images/image_gallery/'+imagename;*/
						var iname = document.getElementById('img');
						if(!iname) window.alert('nope');
						imgname = iname;
						timer = setTimeout("checkComplete()", 8000);
						/*if(iname.complete == true || iname.height == 600 || iname.width == 800) {
							switch(parseInt(speed)) {
								case 0:
									timer = setTimeout("nextSlide()", 6000);
									break;
								case 1:
									timer = setTimeout("nextSlide()", 8000);
									break;
								case 2:
									timer = setTimeout("nextSlide()", 4000);
									break;
							}
						}*/
					}
				}
			}
			xmlHttp.open("GET", uri, true);
			xmlHttp.send(null);
			break;
		}
	}
	loader.innerHTML = '';
	window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
}

function nextSlide() {
	sliding = true;
	var theimageTb = document.getElementById('theimages');
	var imageTb = document.getElementById('images');
	var theimagecells = theimageTb.rows[0].cells;
	var imagecells = imageTb.rows[0].cells;
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	var prev, next, previmg, nextimg;
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	for(i=0; i<imagecells.length; i++) {
		if((imagecells[i].innerHTML) == img) {
			if((i-1) >= 0)
				prev = imagecells[(i-1)].innerHTML;
			else
				prev = imagecells[(imagecells.length)-1].innerHTML;
			if((i+1) < imagecells.length)
				next = imagecells[(i+1)].innerHTML;
			else
				next = imagecells[0].innerHTML;
		}
	}
	for(i=0; i<theimagecells.length; i++) {
		if((theimagecells[i].innerHTML) == imagename) {
			if((i-1) >= 0)
				previmg = theimagecells[(i-1)].innerHTML;
			else
				previmg = theimagecells[(theimagecells.length)-1].innerHTML;
			if((i+1) < theimagecells.length)
				nextimg = theimagecells[(i+1)].innerHTML;
			else
				nextimg = theimagecells[0].innerHTML;
		}
	}
	for(i=0; i<imagecells.length; i++) {
		if(imagecells[i].innerHTML == img) {
			if(galOp == 20) {
				//intVal = setInterval("openImage()", 30);
			}
			if((i+1) < imagecells.length) {
				image = imagecells[(i+1)].innerHTML;
			} else {
				image = imagecells[0].innerHTML;
			}
			//timer = setTimeout("nextSlide()", 4000);
			/*imgsrc = 'admincontrol/images/image_gallery/'+imagename;
			var iname = new Image();
			//iname = document.getElementById('img'+imagename);
			iname.src = imgsrc;*/			
			uri = 'index.php';
			uri += '?route=imagegallery/full_image/' + img;
			uri += '&sid=' + Math.random();
			img = next;
			imagename = nextimg;
			newXmlHttp = getXmlHttpObject();
			if(newXmlHttp == null) {
				return;
			}
			newXmlHttp.onreadystatechange = function() {
				if(newXmlHttp.readyState == 4 || newXmlHttp.readyState == 'complete') {
					if(newXmlHttp.status == 200) {
						biggallery.style.display = 'block';
						biggallery.style.width = (1080) + 'px';
						if(biggallery.clientHeight < 200) {
							biggallery.style.height = 'auto';
						}
						biggallery.style.left = (screen.width - (1080)) / 2 + 'px';
						biggallery.style.top = '10px';
						biggallery.style.background = '#555555';
						biggallery.style.border = '1px #F5F5F5 solid';
						var theresponse = newXmlHttp.responseText;
						/*response = '<table class="image"><tr><td colspan="3" align="center"><a class="bold white" href="javascript:hideBigGallery();">Close</a></td></tr>';
						response += '<tr><td><a href="javascript:showBigGallery('+prev+');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
						response += '<td>' + theresponse + '</td>';
						response += '<td><a href="javascript:showBigGallery('+next+');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';*/
						response = '<table><tr><td align="center">';
						response += '<table class="image"><tr>';
						response += '<td><a href="javascript:showBigGallery('+prev+');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
						response += '<td><a class="bold small white" href="javascript:hideBigGallery();">Close</a></td>';
						response += '<td><a href="javascript:showBigGallery('+next+');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
						response += '</td></tr>';
						response += '<tr><td>' + theresponse + '</td></tr>';
						response += '</table>';
						biggallery.innerHTML = response;
						/*wrapper.style.opacity = '0.2';
						wrapper.style.mozOpacity = '0.2';
						wrapper.style.filter = 'alpha(opacity=20)';*/
						loader.innerHTML = '';
						window.location.hash = 'chatwindows';
						window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
						var iname = document.getElementById('img');
						if(!iname) window.alert('nope');
						imgname = iname;
						timer = setTimeout("checkComplete()", 8000);
						/*if(iname.complete == true) {
							switch(parseInt(speed)) {
								case 0:
									timer = setTimeout("nextSlide()", 6000);
									break;
								case 1:
									timer = setTimeout("nextSlide()", 8000);
									break;
								case 2:
									timer = setTimeout("nextSlide()", 4000);
									break;
							}
						}*/
					}
				}
			}
			newXmlHttp.open("GET", uri, true);
			newXmlHttp.send(null);
			break;
		}
	}
	loader.innerHTML = '';
	window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
	return false;
}

function checkComplete() {
	if(!imgname) {
		window.alert('Slideshow stopped');
		return;
	}
	if(imgname.height == 600 || imgname.width == 800) {
		switch(parseInt(speed)) {
		case 0:
			timer = setTimeout("nextSlide()", 800);
			break;
		case 1:
			timer = setTimeout("nextSlide()", 3000);
			break;
		case 2:
			timer = setTimeout("nextSlide()", 500);
			break;
		}
	} else {
		timer = setTimeout(checkComplete(), 8000);
	}
}

function startVideoSlideShow(id) {
	var imageTb = document.getElementById('images');
	var imagecells = imageTb.rows[0].cells;
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	var prev, next;
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	for(i=0; i<imagecells.length; i++) {
		if((imagecells[i].innerHTML) == id) {
			if((i-1) >= 0)
				prev = imagecells[(i-1)].innerHTML;
			else
				prev = imagecells[(imagecells.length)-1].innerHTML;
			if((i+1) < imagecells.length)
				next = imagecells[(i+1)].innerHTML;
			else
				next = imagecells[0].innerHTML;
		}
	}
	for(i=0; i<imagecells.length; i++) {
		if(imagecells[i].innerHTML == id) {
			biggallery.style.display = 'block';
			biggallery.style.width = '720px';
			biggallery.style.height = 'auto';
			biggallery.style.left = (screen.width - 720) / 2 + 'px';
			biggallery.style.top = '10px';
			biggallery.style.textAlign = 'center';
			biggallery.style.background = '#666666';
			biggallery.style.border = '1px #F5F5F5 solid';
			biggallery.style.align = 'center';
			/*wrapper.style.opacity = '0.2';
			wrapper.style.mozOpacity = '0.2';
			wrapper.style.filter = 'alpha(opacity=20)';*/
			xmlHttp = getXmlHttpObject();
			if(xmlHttp == null) {
				window.alert("Sorry, but your browser does not support AJAX!");
				return;
			}
			xmlHttp.onreadystatechange = function() {
				if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
					if(xmlHttp.status == 200) {
						response = '<table><tr><td colspan="3" align="center"><a class="bold white" href="javascript:hideBigGallery();">Close</a></td></tr>';
						response += '<tr><td><a href="javascript:showFullVideo(\''+prev+'\');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
						response += '<td>'+xmlHttp.responseText+'</td>';
						response += '<td><a href="javascript:showFullVideo(\''+next+'\');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
						/*response1 = '<table style="width: 99%;" style="margin: 0 auto;"><tr><td align="center">';
						response1 = response1 + '<b><a class="white" href="javascript:hideBigGallery();">Close</a></b><br />';
						response2 = '</td></tr></table>';*/
						biggallery.innerHTML = response;
						if((i+1) < imagecells.length) {
							videoid = imagecells[(i+1)].innerHTML;
						} else {
							videoid = imagecells[0].innerHTML;
						}
						intVal = setInterval("nextVideoSlide()", 6000);
					}
				}
			}
		}
		break;
	}
	loader.innerHTML = '';
	window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
}

function nextVideoSlide() {
	id = videoid;
	var imageTb = document.getElementById('images');
	var imagecells = imageTb.rows[0].cells;
	var biggallery = document.getElementById('biggallery');
	var wrapper = document.getElementById('wrapper');
	var prev, next;
	loader = document.getElementById('ajaxloader');
	loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
	for(i=0; i<imagecells.length; i++) {
		if((imagecells[i].innerHTML) == img) {
			if((i-1) >= 0)
				prev = imagecells[(i-1)].innerHTML;
			else
				prev = imagecells[(imagecells.length)-1].innerHTML;
			if((i+1) < imagecells.length)
				next = imagecells[(i+1)].innerHTML;
			else
				next = imagecells[0].innerHTML;
		}
	}
	for(i=0; i<imagecells.length; i++) {
		if(imagecells[i].innerHTML == id) {
			biggallery.style.display = 'block';
			biggallery.style.width = '720px';
			biggallery.style.height = 'auto';
			biggallery.style.left = (screen.width - 720) / 2 + 'px';
			biggallery.style.top = '10px';
			biggallery.style.textAlign = 'center';
			biggallery.style.background = '#666666';
			biggallery.style.border = '1px #F5F5F5 solid';
			biggallery.style.align = 'center';
			/*wrapper.style.opacity = '0.2';
			wrapper.style.mozOpacity = '0.2';
			wrapper.style.filter = 'alpha(opacity=20)';*/
			xmlHttp = getXmlHttpObject();
			if(xmlHttp == null) {
				window.alert("Sorry, but your browser does not support AJAX!");
				return;
			}
			xmlHttp.onreadystatechange = function() {
				if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
					if(xmlHttp.status == 200) {
						response = '<table><tr><td colspan="3" align="center"><a class="bold white" href="javascript:hideBigGallery();">Close</a></td></tr>';
						response += '<tr><td><a href="javascript:showFullVideo(\''+prev+'\');"><img src="images/left.png" border="0" alt="prev" /></a></td>';
						response += '<td>'+xmlHttp.responseText+'</td>';
						response += '<td><a href="javascript:showFullVideo(\''+next+'\');"><img src="images/right.png" border="0" alt="next" /></a></td></tr></table>';
						/*response1 = '<table style="width: 99%;" style="margin: 0 auto;"><tr><td align="center">';
						response1 = response1 + '<b><a class="white" href="javascript:hideBigGallery();">Close</a></b><br />';
						response2 = '</td></tr></table>';*/
						biggallery.innerHTML = response;
						if((i+1) < imagecells.length) {
							videoid = imagecells[(i+1)].innerHTML;
						} else {
							videoid = imagecells[0].innerHTML;
						}
						intVal = setInterval("nextVideoSlide()", 6000);
					}
				}
			}
		}
		break;
	}
	loader.innerHTML = '';
	window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);
}

function trim2(s) {
	var l = 0;
	var r = s.length-1;
	while(l < s.length && s[l] == ' ') {
		l++;
	}
	while(r > l && s[r] == ' ') {
		r-=1;
	}
	return s.substring(l, r+1);
}

String.prototype.trim = function() {
	a = this.replace(/^\s+/, '');
	return a.replace(/\s+$/, '');
};

function validate_email(str) {	
  apos = str.indexOf("@");
  dotpos = str.lastIndexOf(".");
  if(apos<1||dotpos-apos<2) {
	  return false;
  } else {
	  return true;
  }
}

function validateRegForm() {
	var semail, suname, spassword, spassword2, sloc, sagree, simgv;
	with(window.document.regform) {
		semail = email;
		suname = uname;
		spassword = password;
		spassword2 = password2;
		sloc = location;
		sagree = agree;
		simgv = imgv;
	}
	
	var suspan, spspan, sp2span, sespan, slocspan, simgvspan, sagspan;
	suspan = document.getElementById('uspan');
	spspan = document.getElementById('pspan');
	sp2span = document.getElementById('p2span');
	sespan = document.getElementById('espan');
	slocspan = document.getElementById('locspan');
	simgvspan = document.getElementById('imgvspan');
	sagspan = document.getElementById('agspan');

	if(semail.value.trim() == '') {
		sespan.innerHTML = "Please enter a valid email";
		semail.style.background = 'Yellow';
		semail.value = '';
		semail.focus();
		return false;
	} else if(!validate_email(semail.value)) {
		sespan.innerHTML = "Please enter a valid email";
		semail.style.background = 'Yellow';
		semail.value = '';
		semail.focus();
		return false;
	/*} else if(!checkEmail(semail.value)) {
		return false;*/
	} else {
		sespan.innerHTML = '';
		semail.style.background = '#FFFFFF';
	}
	
	if(suname.value.trim() == '') {
		suspan.innerHTML = "Please select a first name";
		suname.style.background = 'Yellow';
		suname.value = '';
		suname.focus();
		return false;
	} else if(suname.value.trim().length < 4) {
		suspan.innerHTML = "Less than 4 characters not allowed";
		suname.style.background = 'Yellow';
		suname.value = '';
		suname.focus();
		return false;
	} else if(suname.value.trim().length > 20) {
		suspan.innerHTML = "More than 20 characters not allowed";
		suname.style.background = 'Yellow';
		suname.value = '';
		suname.focus();
		return false;
	} else if(!checkChars(suname.value)) {
		//window.alert(susername.value.indexOf('v'));
		suspan.innerHTML = "Special characters are not allowed.";
		suname.style.background = 'Yellow';
		suname.value = '';
		suname.focus();
		return false;
	} else {
		suspan.innerHTML = '';
		suname.style.background = '#FFFFFF';
	}
	
	if(spassword.value.trim() == '') {
		spspan.innerHTML = "Please select a password";
		spassword.style.background = 'Yellow';
		spassword.value = '';
		spassword.focus();
		return false;
	} else if(spassword.value.trim().length < 5) {
		spspan.innerHTML = "Password length must be more than 5 characters!";
		spassword.style.background = 'Yellow';
		spassword.focus();
		return false;
	} else if(spassword.value.trim().length > 10) {
		spspan.innerHTML = "Password length must not exceed 10 characters!";
		spassword.style.background = 'Yellow';
		spassword.focus();
		return false;
	} else {
		spspan.innerHTML = '';
		spassword.style.background = '#FFFFFF';
	}
	
	if(spassword2.value.trim() == '') {
		sp2span.innerHTML = "Please re-enter the password";
		spassword2.style.background = 'Yellow';
		spassword2.value = '';
		spassword2.focus();
		return false;
	} else if(spassword2.value.trim() != spassword.value.trim()) {
		sp2span.innerHTML = "Passwords do not match!";
		spassword2.style.background = 'Yellow';
		spassword2.value = '';
		spassword2.focus();
		return false;
	} else {
		sp2span.innerHTML = '';
		spassword2.style.background = '#FFFFFF';
	}
	
	if(simgv.value.trim() == '') {
		simgvspan.innerHTML = "Please enter verification code!";
		simgv.style.background = 'Yellow';
		simgv.focus();
		return false;
	} else if(isNaN(simgv.value.trim())) {
		simgvspan.innerHTML = "Please enter the code properly!";
		simgv.style.background = 'Yellow';
		simgv.value = '';
		simgv.focus();
		return false;
	} else {
		simgvspan.innerHTML = '';
		simgv.style.background = '#FFFFFF';
	}
	
	if(sagree.checked == false) {
		sagspan.innerHTML = "Please agree!";
		sagree.focus();
		return false;
	} else {
		sagspan.innerHTML = '';
	}
	
	return true;
}

function checkChars(str) {
	var iChars = "!@#$%^&*()+=-[]';,./{}|\"\\:<>?~_"; 
	for(i=0; i<str.length; i++) {
		if(iChars.indexOf(str.charAt(i)) != -1) {
		  return false;
		}
	}
	return true;
}

function checkUser() {
	var username, slname;
	with(document.bookingform) {
		username = fname.value;
		slname = lname.value;
	}
	if(username != '' && slname != '') {
		uri = 'index.php';
		uri = uri + '?route=users/check_user';
		uri = uri + '&username=' + username;
		uri = uri + '&lname=' + slname;
		uri = uri + '&sid=' + Math.random();
		xmlHttp = getXmlHttpObject();
		if(xmlHttp == null) {
			window.alert("Sorry, but your browser does not support AJAX!");
			return;
		}
		loader = document.getElementById('userload');
		loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
		xmlHttp.onreadystatechange = function() {
			if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
				if(xmlHttp.status == 200) {
					var sp;
					if(xmlHttp.responseText.trim() == 'Combination available, Congrats!') sp = 'blue';
					else sp = 'red';
					document.getElementById('lspan').innerHTML = '<small class="'+sp+'">'+xmlHttp.responseText+'</small>';
					loader.innerHTML = '';
				}
			}
		}
		xmlHttp.open("GET", uri, true);
		xmlHttp.send(null);
	} else {
		return;
	}
}

function checkEmail(email) {
	if(email != '') {
		uri = 'index.php';
		uri = uri + '?route=users/check_email/' + email;
		uri = uri + '&sid=' + Math.random();
		xmlHttp = getXmlHttpObject();
		if(xmlHttp == null) {
			window.alert("Sorry, but your browser does not support AJAX!");
			return;
		}
		loader = document.getElementById('eload');
		loader.innerHTML = '<img src="images/loader2.gif" alt="loading" border="0" />';
		xmlHttp.onreadystatechange = function() {
			if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
				if(xmlHttp.status == 200) {
					var sp;
					if(xmlHttp.responseText.trim() == 'Available, Congrats!') sp = 'blue';
					else sp = 'red';
					document.getElementById('espan').innerHTML = '<small class="'+sp+'">'+xmlHttp.responseText+'</small>';
					loader.innerHTML = '';
				}
			}
		}
		xmlHttp.open("GET", uri, true);
		xmlHttp.send(null);
	} else {
		document.getElementById('espan').innerHTML = "Please enter a valid email!";
		return false;
	}
}

function showYTVideo(id, video) {
	//var biggallery = document.getElementById('biggallery');
	var biggallery = document.getElementById('vid'+id);
	var wrapper = document.getElementById('wrapper');
	xmlHttp = getXmlHttpObject();
	if(xmlHttp == null) {
		window.alert("Cannot create AJAX object!");
		return;
	}
	var table = document.getElementById('vidtable'+id);
	cells = table.rows[0].cells;
	for(i=0; i<cells.length; i++) {
		if((cells[i].innerHTML) == video) {
			var num = i;
			if((i-1) >= 0)
				prev = cells[(i-1)].innerHTML;
			else
				prev = cells[(cells.length)-1].innerHTML;
			if((i+1) < cells.length)
				next = cells[(i+1)].innerHTML;
			else
				next = cells[0].innerHTML;
		}
	}
	xmlHttp.onreadystatechange = function() {
		if(xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete') {
			if(xmlHttp.status == 200) {
				biggallery.style.display = 'block';
				/*biggallery.style.left = (screen.width - 610) / 2+'px';
				biggallery.style.top = 10+'px';*/
				biggallery.style.width = '620px';
				biggallery.style.height = '470px';
				biggallery.style.overflow = 'hidden';
				biggallery.style.background = "#758494 url('../images/popupbg.jpg') 0 0 repeat-x";
				biggallery.style.position = 'absolute';
				biggallery.style.border = '1px #DCDCDC solid';
				biggallery.style.textAlign = 'center';
				var left = document.getElementById('left');
				left.style.overflow = 'visible';
				/*wrapper.style.opacity = '0.2';
				wrapper.style.mozOpacity = '0.2';
				wrapper.style.filter = 'alpha(opacity=20)';*/
				response = '<table style="margin: 0 auto; text-align: center;" width="100%"><tr>';
				response += '<td><a style="border: 0;" href="javascript:showYTVideo('+id+', \''+prev+'\');"><img src="images/left.png" border="0" alt="prev" style="border: 0;" /></a></td>';
				response += '<td><a class="bold small white" href="javascript:hideVid('+id+');">Close</a></td>';
				response += '<td><a style="border: 0;" href="javascript:showYTVideo('+id+', \''+next+'\');"><img src="images/right.png" border="0" alt="next" style="border: 0;" /></a></td></tr></table>';
				response += '</td></tr>';
				response += '</table>';
				response += '<table style="margin: 0 auto; text-align: center;" width="100%">';
				response += '<tr><td align="center">';
				response += xmlHttp.responseText;
				response += '</td></tr>';
				response += '</table>';
				biggallery.innerHTML = response;
			}
		}
	}
	uri = 'index.php';
	uri += '?route=ajax/show_yt_video';
	uri += '&video=' + video;
	uri += '&sid=' + Math.random();
	xmlHttp.open("GET", uri, true);
	xmlHttp.send(null);
	/*window.location.hash = 'chatwindows';
	window.document.getElementsByName('chatwindows')[0].scrollIntoView(true);*/
}

function hideVid(id) {
	var biggallery = document.getElementById('vid'+id);
	biggallery.innerHTML = '';
	biggallery.style.display = 'none';
	left = document.getElementById('left');
	left.style.overflow = 'hidden';
}

function checkLoginForm() {
	var semail, spass;
	with(window.document.loginform) {
		semail = email;
		spass = password;
	}
	if(semail.value.trim() == '') {
		semail.style.background = 'Yellow';
		semail.focus();
		return false;
	} else {
		semail.style.background = '#FFFFFF';
	}
	
	if(spass.value.trim() == '') {
		spass.style.background = 'Yellow';
		spass.focus();
		return false;
	} else {
		spass.style.background = '#FFFFFF';
	}
	return true;
}

function showDiv(elm) {
    var el = document.getElementById(elm);
    el.style.display = 'block';
}

function hideDiv(elm) {
    var el = document.getElementById(elm);
    el.style.display = 'none';
}

function getJSON() {
	// TODO CODE
}

function readXML() {
	// TODO CODE
}

