/* JavaScript Functions 2 for CDESI */

var sliding = false, speed = 5, commentSpeed = 5, slideSpeed = 2, commentSlideSpeed = 2, divHeight, startHeight, finalHeight, theDiv = null, slideTimer, commentSlideTimer;
var divOpac, opSpeed = 0.2;
var browser = navigator.appName, bversion = navigator.appVersion;
var sdisplay = 'block', theImg;

function slideDiv(divname, sheight, fheight, disp, sspeed, slspeed, img) {
	// TODO CODE
	if(disp == null) disp = sdisplay;
	sdisplay = disp;
	if(sspeed == null) sspeed = speed;
	speed = sspeed;
	if(slspeed == null) slspeed = slideSpeed;
	slideSpeed = slspeed;
	if(img == null) img = null;
	theImg = img;
	if(sliding) {
		return;
	}
	if(theDiv == null) {
		theDiv = document.getElementById(divname);
	} else {
		theDiv = document.getElementById(divname);
	}
	/*window.alert(browser);
	return;*/
	theDiv.style.display = 'block';
	startHeight = sheight;
	finalHeight = fheight;
	divHeight = theDiv.clientHeight;
	// divOpac = theDiv.style.opacity;
	// window.alert(divHeight+' '+startHeight);
	if(divHeight == finalHeight) {
		sliding = true;
		slideUp();
	} else if(divHeight == startHeight) {
		sliding = true;
		slideDown();
	}
}

function slideUp() {
	// TODO CODE
	divHeight = theDiv.clientHeight;
	// divOpac = theDiv.style.opacity;
	/*if(!divOpac)
		divOpac = theDiv.style.MozOpacity;
	if(!divOpac && browser == 'Microsoft Internet Explorer')
		divOpac = theDiv.filters.alpha.opacity;*/
	if(divHeight > startHeight) {
		theDiv.style.height = (divHeight - slideSpeed)+'px';
		/*theDiv.style.opacity = (divOpac - opSpeed);
		if(browser == 'Microsoft Internet Explorer')
			theDiv.filters.alpha.opacity -= speed;
		theDiv.style.MozOpacity = (divOpac - opSpeed);*/
		slideTimer = setTimeout("slideUp()", speed);
	} else {
		theDiv.style.height = startHeight+'px';
		theDiv.style.display = sdisplay;
		if(theImg != null) {
			document.getElementById(theImg).src = "images/down.png";
			document.getElementById(theImg).alt = "collapse";
		}
		/*theDiv.style.opacity = 0;
		theDiv.filter = 'alpha(opacity = 0)';
		theDiv.style.MozOpacity = 0;*/
		sliding = false;
		clearTimeout(slideTimer);
	}
}

function slideDown() {
	divHeight = theDiv.clientHeight;
	// divOpac = theDiv.style.opacity;
	/*if(!divOpac)
		divOpac = theDiv.style.MozOpacity;
	if(!divOpac && browser == 'Microsoft Internet Explorer')
		divOpac = theDiv.filters.alpha.opacity;*/
	if(divHeight < finalHeight) {
		theDiv.style.height = (divHeight + speed)+'px';
		/*theDiv.style.opacity = (divOpac + opSpeed);
		if(browser == 'Microsoft Internet Explorer')
			theDiv.filters.alpha.opacity += speed;
		theDiv.style.MozOpacity = (divOpac + opSpeed);*/
		slideTimer = setTimeout("slideDown()", speed);
	} else {
		theDiv.style.height = finalHeight+'px';
		if(theImg != null) {
			document.getElementById(theImg).src = "images/up.png";
			document.getElementById(theImg).alt = "up";
		}
		/*theDiv.style.opacity = 1.0;
		theDiv.filter = 'alpha(opacity = 100)';
		theDiv.style.MozOpacity = 1.0;*/
		sliding = false;
		clearTimeout(slideTimer);
	}
}

function slideCommentDiv(divname, sheight, fheight, disp, sspeed, slspeed, img) {
	// TODO CODE
	if(disp == null) disp = sdisplay;
	sdisplay = disp;
	if(sspeed == null) sspeed = commentSpeed;
	commentSpeed = sspeed;
	if(slspeed == null) slspeed = commentSlideSpeed;
	commentSlideSpeed = slspeed;
	if(img == null) img = null;
	theImg = img;
	if(sliding) {
		return;
	}
	if(theDiv == null) {
		theDiv = document.getElementById(divname);
	} else {
		theDiv = document.getElementById(divname);
	}
	/*window.alert(browser);
	return;*/
	theDiv.style.display = 'block';
	startHeight = sheight;
	finalHeight = fheight;
	divHeight = theDiv.clientHeight;
	// divOpac = theDiv.style.opacity;
	// window.alert(divHeight+' '+startHeight);
	if(divHeight == finalHeight) {
		sliding = true;
		commentSlideUp();
	} else if(divHeight == startHeight) {
		sliding = true;
		commentSlideDown();
	}
}

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

// close layer when click-out
document.onclick = mclose;

