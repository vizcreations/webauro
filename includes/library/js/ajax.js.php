<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract JavaScript - PHP - Library
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */


?><!--

<script type="text/javascript">


/*uri = 'image.php?image=' + img;
var width  = 300;
var height = 300;
var left   = (screen.width  - width)/2;
var top    = (screen.height - height)/2;
var params = 'width='+width+', height='+height;
params += ', top='+top+', left='+left;
params += ', directories=no';
params += ', location=no';
params += ', menubar=no';
params += ', resize=0';
params += ', scrollbars=no';
params += ', status=no';
params += ', toolbar=no';
newwin = window.open(uri,'windowname5', params);
if (window.focus) { newwin.focus(); }*/

var theXmlHttp = getXmlHttpObject();
var uri;
var cityajax;
var emails;
var messageajax;
var commentajax;
var thediv;

function postMessage() {
	var theMessage = document.getElementById('message').value;
	document.getElementById('message').value = '';
	document.getElementById('message').focus();
	var option;
	var theOption = document.getElementById('option');
	if(theOption) option = theOption.value;
	else option = 'default';
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printMessage(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/postmsg';
	uri = uri + '&option=' + option;
	uri = uri + '&message=' + theMessage;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printMessage(response) {
	var target = document.getElementById('messageajax');
	target.innerHTML = response;
}

function getMyPage() {
	uri = 'async.php';
	uri = uri + '?route=ajax/mypage';
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

function getCity(cityid) {
	if(cityid == 'select') return;
	var xmlHttpObject = getXmlHttpObject();
	if(xmlHttpObject == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	xmlHttpObject.onreadystatechange = function() {
		if(xmlHttpObject.readyState == 4 || xmlHttpObject.readyState == 'complete') {
			if(xmlHttpObject.status == 200) {
				printCity(xmlHttpObject.responseText);
			}
		}
	}
	var url = 'async.php?route=ajax/city';
	url = url + '&cityid=' + cityid;
	url = url + '&sid=' + Math.random();
	xmlHttpObject.open("GET", url, true);
	xmlHttpObject.send(null);
}

function printCity(response) {
	var tar = document.getElementById('cityajax');
	tar.innerHTML = response;
}

function changeCity(cityid) {
	window.location.href = 'index.php?route=users/city/' + cityid;
}

function getProfile(id) {
	// TODO CODE
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printProfile(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/profile/' + id;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printProfile(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function getFollowing(id) {
	// TODO CODE
	/*if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printFollowing(theXmlHttp.responseText);
			}
		}
	}*/
	uri = 'async.php';
	uri = uri + '?route=ajax/following/' + id;
	uri = uri + '&sid=' + Math.random();
	/*theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);*/
	// window.location.href = uri;
	sendAJAXRequest(uri);
}

function printFollowing(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function getFollowers(id) {
	// TODO CODE
	/*if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printFollowers(theXmlHttp.responseText);
			}
		}
	}*/
	uri = 'async.php';
	uri = uri + '?route=ajax/followers/' + id;
	uri = uri + '&sid=' + Math.random();
	/*theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);*/
	// window.location.href = uri;
	sendAJAXRequest(uri);
}

function printFollowers(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function inviteFriends(id) {
	// TODO CODE
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printInvitation(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/invite/' + id;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printInvitation(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function sendInvite() {
	emails = document.getElementById('emails');
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printResult(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/send_invite';
	uri = uri + '&emails=' + emails.value;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printResult(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function follow(id, url) {
	if(confirm("Do you really want to follow " + id + "?")) {
		if(theXmlHttp == null) {
			window.alert("Sorry, but your browser does not support AJAX!");
			return;
		}
		theXmlHttp.onreadystatechange = function() {
			if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
				if(theXmlHttp.status == 200) {
					printFollowing(theXmlHttp.responseText);
				}
			}
		}
		uri = 'async.php';
		uri = uri + '?route=ajax/follow/' + id;
		uri = uri + '&uri=' + url;
		uri = uri + '&sid=' + Math.random();
		theXmlHttp.open("GET", uri, true);
		theXmlHttp.send(null);
		//window.location.href = uri;
	}
}

function printFollowing(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function unfollow(id) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printUnfollowing(theXmlHttp.responseText);
			}
		}
	}
	if(confirm("Do you really want to unfollow " + id + "?")) {
		uri = 'async.php';
		uri = uri + '?route=ajax/unfollow/' + id;
		uri = uri + '&sid=' + Math.random();
		theXmlHttp.open("GET", uri, true);
		theXmlHttp.send(null);
		//window.location.href = uri;
	}
}

function printUnfollowing(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function block(id) {
	if(confirm("Do you really want to block this user?")) {
		if(theXmlHttp == null) {
			window.alert("Sorry, but your browser does not support AJAX!");
			return;
		}
		theXmlHttp.onreadystatechange = function() {
			if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
				if(theXmlHttp.status == 200) {
					printUnfollowing(theXmlHttp.responseText);
				}
			}
		}
		uri = 'async.php';
		uri = uri + '?route=ajax/block/' + id;
		uri = uri + '&sid=' + Math.random();
		theXmlHttp.open("GET", uri, true);
		theXmlHttp.send(null);
	}
}

function printBlock(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function showLegend(value) {
	var index, newvalue;
	var thelegend = document.getElementById("thelegend");
	var thesave = document.getElementById("save");
	if(value.substr(0, 1) == '@') {
		if(value.match(" ")) {
			index = value.indexOf(" ");
			newvalue = value.substr(0, index);
			newvalue = newvalue.replace(/@/, "Reply to ");
			thelegend.innerHTML = newvalue;
			thesave.value = "Reply";
		}
	}
}

function showReplies(username) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printReplies(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/replies/' + username;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printReplies(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function showPM(username) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printPM(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/show_pm/' + username;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printPM(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function showPMSent(username) {
	uri = 'async.php';
	uri = uri + '?route=ajax/show_pmsent/' + username;
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

function sendPM(id) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printPMForm(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/send_pm/' + id;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printPMForm(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function savePM(id) {
	var pm = document.getElementById('pm');
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printPMSent(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/save_pm/' + id;
	uri = uri + '&pm=' + pm.value;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printPMSent(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function getTweets(username) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				printTweets(theXmlHttp.responseText);
			}
		}
	}
	uri = 'async.php';
	uri = uri + '?route=ajax/tweets/' + username;
	uri = uri + '&sid=' + Math.random();
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function printTweets(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function commentMessage(id) {
	commentajax = document.getElementById(id);
	if(commentajax) {
		if(commentajax.style.display == 'none')
			commentajax.style.display = 'block';
		else if(commentajax.style.display == 'block')
			commentajax.style.display = 'none';
	} else {
		window.alert("not working!");
	}
}

function printCommentForm(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function sendAJAXRequest(url) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				getAJAXResponse(theXmlHttp.responseText);
			}
		}
	}
	theXmlHttp.open("GET", url, true);
	theXmlHttp.send(null);
}

function getAJAXResponse(response) {
	cityajax = document.getElementById('cityajax');
	cityajax.innerHTML = response;
}

function findFriends(id) {
	uri = 'async.php';
	uri = uri + '?route=ajax/search';
	uri = uri + '&sid=' + Math.random;
	sendAJAXRequest(uri);
}

function searchFriend() {
	var q = document.getElementById('q');
	uri = 'async.php';
	uri = uri + '?route=ajax/search_results';
	uri = uri + '&q=' + q.value;
	uri = uri + '&sid=' + Math.random;
	sendAJAXRequest(uri);
}

function zoomImage(img, divid) {
	thediv = document.getElementById(divid);
	thediv.style.display = 'block';
	thediv.style.position = 'absolute';
	thediv.style.height = '302px';
	thediv.style.width = '302px';
	thediv.style.border = '2px #DCDCDC solid';
	thediv.style.backgroundImage = "url('images/profiles/" + img + "')";
	thediv.style.backgroundPosition = "center";
	thediv.style.backgroundRepeat = "no-repeat";
}

function getUsers(cityid) {
	uri = 'async.php';
	uri = uri + '?route=ajax/users/' + cityid;
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

function saveComment(id) {
	thecmtid = 'thecomment' + id + '';
	thecmt = document.getElementById(thecmtid);
	if(!thecmt)
		window.alert(thecmtid + ' is not there');
	cmtid = 'comment' + id;
	uri = 'async.php';
	uri = uri + '?route=ajax/save_comment/' + id;
	uri = uri + '&comment=' + (thecmt).value;
	uri = uri + '&sid=' + Math.random();
	sendAJAXCommentRequest(uri, cmtid);
}

function sendAJAXCommentRequest(request, cmtid) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				getAJAXCommentRequest(theXmlHttp.responseText, cmtid);
			}
		}
	}
	uri = request;
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function getAJAXCommentRequest(response, id) {
	messageajax = document.getElementById(id);
	messageajax.innerHTML = response;
}

function sendAJAXMessageRequest(uri) {
	if(theXmlHttp == null) {
		window.alert("Sorry, but your browser does not support AJAX!");
		return;
	}
	theXmlHttp.onreadystatechange = function() {
		if(theXmlHttp.readyState == 4 || theXmlHttp.readyState == 'complete') {
			if(theXmlHttp.status == 200) {
				getAJAXMessageRequest(theXmlHttp.responseText);
			}
		}
	}
	theXmlHttp.open("GET", uri, true);
	theXmlHttp.send(null);
}

function getAJAXMessageRequest(response) {
	messageajax = document.getElementById('messageajax');
	messageajax.innerHTML = response;
}

function deleteMessage(id) {
	if(confirm("Are you sure you want to delete this message?")) {
		uri = 'async.php';
		uri = uri + '?route=ajax/delete_msg/' + id;
		uri = uri + '&sid=' + Math.random();
		sendAJAXMessageRequest(uri);
	}
}

function videosMain() {
	uri = 'async.php';
	uri = uri + '?route=videos';
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

function getSubCategories(catid) {
	uri = 'async.php';
	uri = uri + '?route=videos/subcategories/' + catid;
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

function getVideos(catid) {
	uri = 'async.php';
	uri = uri + '?route=videos/show_videos/' + catid;
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

function getVideo(id) {
	uri = 'async.php';
	uri = uri + '?route=videos/show_video/' + id;
	uri = uri + '&sid=' + Math.random();
	sendAJAXRequest(uri);
}

//
</script>-->
