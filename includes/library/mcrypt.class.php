<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End MCrypt Library Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @author VizCreations, varied
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

 
class ProCrypt {
	
	/**
	 * 
	 * @param $name
	 * @param $value
	 * @return void
	 */
	public function __set($name, $value) {
		switch($name) {
			case 'key':
			case 'ivs':
			case 'iv':
				$this->name = $value;
			break;
			
			default:
				throw new Exception( "$name cannot be set!" );
				break;
		}
	}
	
	/**
	 * 
	 * @param $name
	 * @return string
	 */
	public function __get($name) {
		switch($name) {
			case 'key':
				return 'keee';
				break;				
			case 'ivs':
				return mcrypt_get_iv_size( MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB );
				break;
			case 'iv':
				return mcrypt_create_iv( $this->ivs );
				break;
				
			default:
				throw new Exception( "$name cannot be called!" );
				break;
		}
	}
	
	/**
	 * 
	 * @param $text
	 * @return encrypted string
	 */
	public function encrypt( $text ) {
		$data = mcrypt_encrypt( MCRYPT_RIJNDAEL_128, $this->key, $text, MCRYPT_MODE_ECB, $this->iv );
		return base64_encode($data);
	}
	
	public function decrypt( $text ) {
		$text = base64_decode( $text );
		return mcrypt_decrypt( MCRYPT_RIJNDAEL_128, $this->key, $text, MCRYPT_MODE_ECB, $this->iv );
	}
}
