<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End init script to initialize the application objects and variables
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Constants Definitions */
define( "DS", DIRECTORY_SEPARATOR );
define( "__INC", __ROOT.DS."includes" );
define( "__LIB", __INC.DS."library" );
define( "__SELF", $_SERVER['PHP_SELF'] );
define( "__BASE", __ROOT.DS."application" );
define( "__CONTROLLERS", __ROOT.DS."controllers" );
define( "__SEO", __ROOT.DS."seo" );
define( "__MODELS", __ROOT.DS."models" );
define( "__VIEWS", __ROOT.DS."views" );
define( "__WEBTEMPLATES", __ROOT.DS."templates" );
define( "__FRAGS", __ROOT.DS."frags" );
define( "__FRAGMENTS", __ROOT.DS."fragments" );
define( "__WB", __ROOT.DS."webauro" );
define( "__LOG", __ROOT.DS."logs" );
define( "__TMP", __ROOT.DS."tmp" );
/* Require your constant definitions */
require_once( __INC.DS."constants.php" );

/* Include the application configuration*/
require_once(__ROOT.DS."configuration.php");

/* Require all application classes to instantiate objects */
require_once(__BASE.DS.'registry.class.php');
require_once(__BASE.DS.'database.class.php');
require_once(__BASE.DS.'router.class.php');
require_once(__BASE.DS.'controller.class.php');
require_once(__BASE.DS.'template.class.php');
require_once(__BASE.DS.'frag.class.php');
require_once(__BASE.DS.'fragtemplate.class.php');
require_once(__BASE.DS.'fragmodel.class.php');
require_once(__BASE.DS.'seo.class.php');
require_once( __BASE.DS."cache.class.php" );
/*require_once(__LIB.DS.'mcrypt.class.php');
require_once(__LIB.DS.'mail.class.php');*/ // Called on Request

/* Also require my functions */
require_once( __LIB.DS."system".DS."functions.php" );

/* Load global variable app */
$site = $app['sitename'];

/**
 * ##
 * @abstract The global registry object DEFINE AND DECLARE MEMBERS BEFORE OTHER objects
 * @var Object
 * ##
 */
$registry = new Registry();
$registry->sitename = $site;
$registry->url = $webserver['url'];
$registry->hostip = $app['hostip'];
$registry->err_message = null;
$registry->webTemplate = $template['deftemplate'];

/* ## Set my database server settings ## */
DB::$dbhost = $dbserver['host'];
DB::$dbuser = $dbserver['user'];
DB::$dbpass = $dbserver['pass'];
DB::$dbschema = $dbserver['schema'];
$registry->db = DB::getInstance(); // Singleton Instance always open
if(DB::$error != null || !empty(DB::$error)) {
	log_db( DB::$error );
	die(DB::$error);
}

$registry->isLogged = false; // Legacy ##CODE##

$registry->session = new stdClass();
$registry->session->server = new stdClass();
$registry->session->httpvars = new stdClass();
$registry->session->postvars = new stdClass();
$registry->session->getvars = new stdClass();
$registry->session->filecontrol = new stdClass();
$registry->session->cookie = new stdClass();
$registry->session->sessionarr = $_SESSION;
$registry->session->site = new stdClass();

/**
* Set up Admin details
*/
$registry->admin = new stdClass();
$registry->admin->adminemail = $admin['email'];
$registry->admin->adminemailex = $admin['emailex'];

/* Set up User details */
/**
 * ##
 */
$registry->user = new stdClass();
$registry->user->isLoggedIn = false;
$registry->user->id = 0;
$registry->user->name = "Anonymous";
$registry->user->email = "someone@somemail.com";
$registry->user->image = "images/logo.png";
$registry->user->isSuperadmin = false;

/* Set app cookie */
$name = $app['sitename']."_wbcookie";
$value = "true";
$time = 3600;
$dir = "/";
setcookie($name, $value, time()+$time);

if(isset($_SESSION[$site.'_user_is_logged_in'])) {
	if($_SESSION[$site.'_user_is_logged_in'] == true) { 
		$registry->isLogged = true; // Legacy ##CODE##
		$registry->user->isLoggedIn = true;
	}
}

if(!isset($_COOKIE[$site.'_wbcookie'])) {
	//$registry->router->unsetTheCookie(); // Legacy ##CODE## commented out
	if(isset($_SESSION[$site.'_user_is_logged_in'])) {
		unset($_SESSION[$site.'_user_is_logged_in']);
		unset($_SESSION[$site.'_userid']);
		unset($_SESSION[$site.'_username']);
		unset($_SESSION[$site.'_userimage']);
		unset($_SESSION[$site.'_superadmin_is_logged_in']);
		$registry->isLogged = false; // Legacy ##CODE##
		$registry->userid = 0; // Legacy ##CODE##
		
		// Recommended way to determine user login status
		$registry->user->isLoggedIn = false;
		$registry->user->id = 0;
	}

	if(isset($_COOKIE[$site."_user_is_logged_in"])) { // User exists in session => boot him out!
		setcookie($site."_user_is_logged_in",false,time()-3600,$dir);
		setcookie($site."_userid",0,time()-3600,$dir);
		setcookie($site."_username","foo",time()-3600,$dir);
		setcookie($site."_userimage","bar",time()-3600,$dir);
		setcookie($site."_useremail","foo@bar.com",time()-3600,$dir);
		setcookie($site."_superadmin_is_logged_in",false,time()-3600,$dir);
	}

	$registry->user->isLoggedIn = false;
	$registry->user->id = 0;
}



/* Initialize customized cookie/session/http variables before standard objects */




/**
 * Update User settings and parameters according to dynamic session data
 */
// Legacy ##CODE##
if(isset($_SESSION[$site.'_userid'])) $registry->userid = $registry->user->id = $_SESSION[$site.'_userid'];
if(isset($_SESSION[$site.'_username'])) $registry->username = $registry->user->name = $_SESSION[$site.'_username'];
if(isset($_SESSION[$site.'_userimage'])) $registry->userimage = $registry->user->image = $_SESSION[$site.'_userimage'];
if(isset($_SESSION[$site.'_superadmin_is_logged_in'])) 
	$registry->superadminIsLogged = $registry->user->isSuperadmin = true;


/** Using cookies to determine user session data **/
if(isset($_COOKIE[$site."_userid"])) $registry->userid = $registry->user->id = $_COOKIE[$site."_userid"];
if(isset($_COOKIE[$site."_username"])) $registry->username = $registry->user->name = $_COOKIE[$site."_username"];
if(isset($_COOKIE[$site."_userimage"])) $registry->userimage = $registry->user->image = $_COOKIE[$site."_userimage"];
if(isset($_COOKIE[$site."_superadmin_is_logged_in"])) 
	$registry->superadminIsLogged = $registry->user->isSuperadmin = TRUE;


/**
* @objectify all arrays in HTTP POST/GET or SERVER environment
*/

if(isset($_SESSION[$site])
&& is_array($_SESSION[$site])
) {
	foreach($_SESSION[$site] as $key => $val) :
		$registry->session->site->$key = $val; // setting name magically
	endforeach;
}

if(isset($_SERVER)) {
	foreach($_SERVER as $key => $val) :
		$registry->session->server->$key = $val;
	endforeach;
}

if(isset($_REQUEST)) {
	foreach($_REQUEST as $key => $val) :
		$registry->session->httpvars->$key = $val;
	endforeach;
}

if(isset($_COOKIE)) {
	foreach($_COOKIE as $key => $val) :
		$registry->session->cookie->$key = $val;
	endforeach;
}

if(isset($_GET)) {
	foreach($_GET as $key => $val) :
		$registry->session->getvars->$key = $val;
	endforeach;
}

if(isset($_POST)) {
	foreach($_POST as $key => $val) :
		$registry->session->postvars->$key = $val;
	endforeach;
}

if(isset($_FILES)) {
	foreach($_FILES as $key => $val) :
		$registry->session->filecontrol->$key = $val;
	endforeach;
}

/** META/SEO data propogated by default to the application
 * @var string
 * 
 */
$registry->seo = new stdClass();
$registry->seo->title = $registry->sitename;
$registry->seo->keywords = "keywords";
$registry->seo->description = "description";


/**
* Set up the Router object for URL re-routing and controller logic
*
*/
$registry->router = new Router($registry);
$registry->router->setControllerPath(__CONTROLLERS);
$registry->router->setSEOPath(__SEO); // Legacy ##CODE##

/* ## Set HTML engines / objects ready ##
*
*/
$registry->template = new Template($registry);
$registry->frag = new Frag($registry);
$registry->fragtemplate = new FragTemplate($registry);
$registry->cache = new Cache($registry);


/** Application status */
if(!empty($registry->router->message)) {
	log_app( $registry->router->message." in ".__FILE__.", line ".__LINE__ );
	die( $registry->router->message." in ".__FILE__." line ".__LINE__ ); // Legacy ##CODE##
}

if(!empty($registry->err_message)) {
	log_app( $registry->err_message." in ".__FILE__.", line ".__LINE__ );
	die($registry->err_message." in ".__FILE__.", line ".__LINE__);
}

if($registry->sitename == "") {
	$registry->err_message = "Site name variable missing from configuration!";
	log_app( $registry->err_message );
	die( "<b>Application failed:</b> Improper configuration ({$registry->err_message})" );
}


/* Default timezone to UTC */
date_default_timezone_set('UTC');

/* Autoload models without class */
function __autoload($class) {
	$file = str_replace('model','',strtolower($class));
	$model = __ROOT.DS.'models'.DS.strtolower($file).'.model.php';
	if(file_exists($model) && is_readable($model)) {
		require_once $model;
	} else {
		$msg = "Could not load model class in ".__FILE__.", line ".__LINE__;
		log_app( $msg );
		log_db( $msg );
		return false;
	}
}
