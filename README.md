Webauro README - Please read carefully before proceeding:
=========================================================

Webauro is an open source web application framework written in PHP for mid-level
web programmers. If you love PHP, you will really like using Webauro.
Webauro is also a super-light CMS built on top of the framework written originally
by Kevin Waterson. You can reach him at http://phpro.org.


How to use it:
==============

1. Extract the compressed file to your host. You should have already 
done this by now!
2. Upload the schema file (.sql) to your database. Get the resource
from https://bitbucket.org/vizcreations/webauro => Downloads
3. Edit the configuration file (configuration.php) to suit your server.
4. Setup your home page and menus using the Administration Control Panel, 
by going to yoursite/webauro. The default credentials to login to the 
admin control are username: admin, password: admin. You can always change it. 
5. Enjoy Webauro and feel free to enhance it.
 

What is in for the developers:
==============================

Developers working on various web projects can use Webauro now and be 
productive immediately. It requires a minimal learning curve, and
when you understand it more, you are in for a smoother ride.


What is in it for the users:
============================

Webauro's clean design and ease in methodology will allow users to 
enjoy their web experience immensely. Users, we mean, the people using 
a website driven by Webauro.


GPL License and future of Webauro:
==================================

Webauro's future also lies in the programmers.
We have made it open source to make it accessible to everyone 
Clone/fork it today.


COMING SOON: A full fledged manual and API documentation for Webauro..


~VIZCREATIONS
http://vizcreations.com https://facebook.com/pages/VizCreations/196503337046748