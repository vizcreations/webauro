-- MySQL dump 10.13  Distrib 5.5.29, for linux2.6 (i686)
--
-- Host: localhost    Database: viz_base
-- ------------------------------------------------------
-- Server version	5.5.29-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_ar_categories`
--

DROP TABLE IF EXISTS `tbl_ar_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ar_categories` (
  `ar_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_category_name` varchar(100) NOT NULL,
  `ar_category_description` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ar_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ar_categories`
--

LOCK TABLES `tbl_ar_categories` WRITE;
/*!40000 ALTER TABLE `tbl_ar_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ar_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ar_comments`
--

DROP TABLE IF EXISTS `tbl_ar_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ar_comments` (
  `ar_comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ar_comment_name` varchar(50) NOT NULL,
  `ar_comment_email` varchar(100) NOT NULL,
  `ar_comment_uri` varchar(100) NOT NULL,
  `ar_comment_content` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ar_comment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ar_comments`
--

LOCK TABLES `tbl_ar_comments` WRITE;
/*!40000 ALTER TABLE `tbl_ar_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ar_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_ar_comments_status`
--

DROP TABLE IF EXISTS `tbl_ar_comments_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_ar_comments_status` (
  `ar_comments_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ar_comments_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ar_comments_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_ar_comments_status`
--

LOCK TABLES `tbl_ar_comments_status` WRITE;
/*!40000 ALTER TABLE `tbl_ar_comments_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_ar_comments_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_articles`
--

DROP TABLE IF EXISTS `tbl_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_articles` (
  `article_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_title` varchar(100) NOT NULL,
  `article_author` varchar(100) NOT NULL,
  `article_content` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_articles`
--

LOCK TABLES `tbl_articles` WRITE;
/*!40000 ALTER TABLE `tbl_articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_articles_categories_rel`
--

DROP TABLE IF EXISTS `tbl_articles_categories_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_articles_categories_rel` (
  `article_category_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ar_category_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_category_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_articles_categories_rel`
--

LOCK TABLES `tbl_articles_categories_rel` WRITE;
/*!40000 ALTER TABLE `tbl_articles_categories_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_articles_categories_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_articles_comments_rel`
--

DROP TABLE IF EXISTS `tbl_articles_comments_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_articles_comments_rel` (
  `article_comment_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `ar_comment_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_comment_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_articles_comments_rel`
--

LOCK TABLES `tbl_articles_comments_rel` WRITE;
/*!40000 ALTER TABLE `tbl_articles_comments_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_articles_comments_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_articles_ratings`
--

DROP TABLE IF EXISTS `tbl_articles_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_articles_ratings` (
  `article_rating_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `article_rating` int(2) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_rating_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_articles_ratings`
--

LOCK TABLES `tbl_articles_ratings` WRITE;
/*!40000 ALTER TABLE `tbl_articles_ratings` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_articles_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_articles_seo`
--

DROP TABLE IF EXISTS `tbl_articles_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_articles_seo` (
  `article_seo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `seo_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_seo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_articles_seo`
--

LOCK TABLES `tbl_articles_seo` WRITE;
/*!40000 ALTER TABLE `tbl_articles_seo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_articles_seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_articles_status`
--

DROP TABLE IF EXISTS `tbl_articles_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_articles_status` (
  `article_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `article_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`article_status_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_articles_status`
--

LOCK TABLES `tbl_articles_status` WRITE;
/*!40000 ALTER TABLE `tbl_articles_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_articles_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_contact`
--

DROP TABLE IF EXISTS `tbl_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_contact` (
  `contact_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `contact_email` varchar(100) DEFAULT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_contact`
--

LOCK TABLES `tbl_contact` WRITE;
/*!40000 ALTER TABLE `tbl_contact` DISABLE KEYS */;
INSERT INTO `tbl_contact` VALUES (1,'viz@vizcreations.com','2010-03-23 10:49:34');
/*!40000 ALTER TABLE `tbl_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_controllers`
--

DROP TABLE IF EXISTS `tbl_controllers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_controllers` (
  `controller_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller_name` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`controller_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_controllers`
--

LOCK TABLES `tbl_controllers` WRITE;
/*!40000 ALTER TABLE `tbl_controllers` DISABLE KEYS */;
INSERT INTO `tbl_controllers` VALUES (1,'index','2010-03-18 12:37:29'),(2,'articles','2010-03-18 12:37:29'),(3,'contact','2010-03-18 12:37:29'),(4,'users','2010-04-26 17:25:38');
/*!40000 ALTER TABLE `tbl_controllers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_controllers_templates`
--

DROP TABLE IF EXISTS `tbl_controllers_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_controllers_templates` (
  `controller_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`controller_template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_controllers_templates`
--

LOCK TABLES `tbl_controllers_templates` WRITE;
/*!40000 ALTER TABLE `tbl_controllers_templates` DISABLE KEYS */;
INSERT INTO `tbl_controllers_templates` VALUES (1,1,1,'2010-03-18 12:37:29'),(2,1,2,'2010-03-18 12:37:29'),(3,1,3,'2010-03-18 12:37:29');
/*!40000 ALTER TABLE `tbl_controllers_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_controllers_views`
--

DROP TABLE IF EXISTS `tbl_controllers_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_controllers_views` (
  `controller_view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller_id` int(11) NOT NULL,
  `view_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`controller_view_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_controllers_views`
--

LOCK TABLES `tbl_controllers_views` WRITE;
/*!40000 ALTER TABLE `tbl_controllers_views` DISABLE KEYS */;
INSERT INTO `tbl_controllers_views` VALUES (1,1,1,'2010-03-18 12:37:30'),(2,2,2,'2010-03-18 12:37:30'),(3,3,3,'2010-03-18 12:37:30'),(4,2,4,'2010-03-24 11:59:01'),(5,4,5,'2010-04-26 17:25:53'),(6,4,6,'2010-04-26 17:26:02');
/*!40000 ALTER TABLE `tbl_controllers_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forums`
--

DROP TABLE IF EXISTS `tbl_forums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forums` (
  `forum_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_title` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forums`
--

LOCK TABLES `tbl_forums` WRITE;
/*!40000 ALTER TABLE `tbl_forums` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_forums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forums_categories`
--

DROP TABLE IF EXISTS `tbl_forums_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forums_categories` (
  `forum_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_category_name` varchar(50) NOT NULL,
  `forum_category_desc` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forums_categories`
--

LOCK TABLES `tbl_forums_categories` WRITE;
/*!40000 ALTER TABLE `tbl_forums_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_forums_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forums_categories_rel`
--

DROP TABLE IF EXISTS `tbl_forums_categories_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forums_categories_rel` (
  `forum_category_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `forum_category_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_category_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forums_categories_rel`
--

LOCK TABLES `tbl_forums_categories_rel` WRITE;
/*!40000 ALTER TABLE `tbl_forums_categories_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_forums_categories_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forums_posts`
--

DROP TABLE IF EXISTS `tbl_forums_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forums_posts` (
  `forum_post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `forum_post_content` text NOT NULL,
  `forum_post_posted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forums_posts`
--

LOCK TABLES `tbl_forums_posts` WRITE;
/*!40000 ALTER TABLE `tbl_forums_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_forums_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forums_users_rel`
--

DROP TABLE IF EXISTS `tbl_forums_users_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forums_users_rel` (
  `forum_user_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`forum_user_rel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forums_users_rel`
--

LOCK TABLES `tbl_forums_users_rel` WRITE;
/*!40000 ALTER TABLE `tbl_forums_users_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_forums_users_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags`
--

DROP TABLE IF EXISTS `tbl_frags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags` (
  `frag_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_name` varchar(50) NOT NULL,
  `frag_title` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags`
--

LOCK TABLES `tbl_frags` WRITE;
/*!40000 ALTER TABLE `tbl_frags` DISABLE KEYS */;
INSERT INTO `tbl_frags` VALUES (1,'latestarticles','Blog','2012-11-16 16:18:20'),(2,'websitelogo','Logo','2014-01-02 13:40:44'),(3,'topmenu','Top Menu','2010-04-26 13:17:19'),(4,'userlogin','User Login','2010-06-06 06:28:12'),(5,'search','Search','2010-07-24 07:59:46'),(6,'adsenseside','AdSense Images','2011-07-30 12:12:49'),(7,'adsenseside','AdSense Content','2010-09-21 06:40:53'),(8,'adsense','AdSense Banner','2010-08-26 08:34:56'),(9,'web','Web Box','2014-01-02 13:45:29'),(10,'extlinks','External links','2011-06-15 19:18:34'),(11,'footer','Footer info','2011-07-18 09:59:50'),(13,'exp','Research & Development','2014-01-02 13:46:38'),(12,'adsense','Windows 7','2010-07-23 14:09:59'),(14,'study','Learn','2012-11-16 16:18:11'),(15,'intro','Introduction','2014-01-02 13:43:41'),(16,'adsense','AdSense Text Banner','2010-08-29 07:43:38'),(17,'googleanalytics','Google Analytics','2011-07-22 04:19:47');
/*!40000 ALTER TABLE `tbl_frags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags_arguments`
--

DROP TABLE IF EXISTS `tbl_frags_arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags_arguments` (
  `frag_argument_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `argument_value` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_argument_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags_arguments`
--

LOCK TABLES `tbl_frags_arguments` WRITE;
/*!40000 ALTER TABLE `tbl_frags_arguments` DISABLE KEYS */;
INSERT INTO `tbl_frags_arguments` VALUES (1,1,'0','2012-11-16 16:18:20'),(2,3,'1','2010-04-25 12:06:19'),(3,12,'2','2010-07-23 14:02:57'),(4,14,'0','2012-11-16 16:18:11');
/*!40000 ALTER TABLE `tbl_frags_arguments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags_content`
--

DROP TABLE IF EXISTS `tbl_frags_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags_content` (
  `frag_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `frag_content` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_content_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags_content`
--

LOCK TABLES `tbl_frags_content` WRITE;
/*!40000 ALTER TABLE `tbl_frags_content` DISABLE KEYS */;
INSERT INTO `tbl_frags_content` VALUES (1,2,'<!--<img src=\"templates/vizcreations/images/logo.png\" alt=\"logo\" border=\"0\" />--><h1>Webauro</h1>','2014-01-02 13:40:44'),(2,6,'<div class=\"container\">\r\n<script type=\"text/javascript\"><!--\r\ngoogle_ad_client = \"ca-pub-5541402001080255\";\r\n/* VC_NEW */\r\ngoogle_ad_slot = \"3454684004\";\r\ngoogle_ad_width = 200;\r\ngoogle_ad_height = 200;\r\n//-->\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>\r\n</div>','2011-07-30 12:12:49'),(3,7,'<div class=\"container\">\r\n<script type=\"text/javascript\"><!--\r\ngoogle_ad_client = \"pub-5541402001080255\";\r\n/* VZC side ad, 180x150, created 4/5/10 */\r\ngoogle_ad_slot = \"3704230740\";\r\ngoogle_ad_width = 180;\r\ngoogle_ad_height = 150;\r\n//-->\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>\r\n</div>','2010-09-21 06:40:53'),(4,8,'<script type=\"text/javascript\"><!--\r\ngoogle_ad_client = \"pub-5541402001080255\";\r\n/* 468x60, VZC smallbanner, created 4/12/09 */\r\ngoogle_ad_slot = \"1878081845\";\r\ngoogle_ad_width = 468;\r\ngoogle_ad_height = 60;\r\n//-->\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>','2010-08-26 08:34:56'),(5,9,'<ul class=\"menu\">\r\n<li>Light weight abstraction</li>\r\n<li>Object oriented</li>\r\n<li>Straight forward</li>\r\n</ul>','2014-01-02 13:45:29'),(6,10,'<div class=\"container\">\r\n<h3>Links</h3>\r\n<div class=\"bbar\"></div>\r\n<ul class=\"links\">\r\n<li><a href=\"http://www.yourhtmlsource.com\" title=\"html-tutorial\">Beginning with HTML, CSS, JS</a></li>\r\n<li><a href=\"http://www.php-mysql-tutorial.com\" title=\"php-mysql-tutorial\">Beginning with web development</a></li>\r\n</ul>\r\n</div>','2011-06-15 19:18:34'),(7,11,'<p class=\"inside white\"><b>VizCreations</b> © 2011. All rights reserved on graphics and content. <a href=\"http://www.freedigitalphotos.net/images/view_photog.php?photogid=851\">Image: <b>Filomena Scalise</b></a> <a href=\"http://www.freedigitalphotos.net/images/view_photog.php?photogid=1152\">Image: <b>jscreationzs</b></a> / FreeDigitalPhotos.net</p>','2011-07-18 09:59:50'),(10,5,'<form action=\"http://www.vizcreations.com/index.php?route=search\" id=\"cse-search-box\">\r\n  <div>\r\n    <input type=\"hidden\" name=\"cx\" value=\"partner-pub-5541402001080255:vb4a16h0yyw\" />\r\n    <input type=\"hidden\" name=\"cof\" value=\"FORID:11\" />\r\n    <input type=\"hidden\" name=\"ie\" value=\"ISO-8859-1\" />\r\n    <input type=\"text\" name=\"q\" size=\"45\" />\r\n    <input type=\"submit\" name=\"sa\" value=\"Search\" />\r\n  </div>\r\n</form>\r\n<script type=\"text/javascript\" src=\"http://www.google.com/cse/brand?form=cse-search-box&amp;lang=en\"></script>','2010-07-24 07:59:46'),(8,12,'<object width=\"87\" height=\"70\"><param name=\"movie\" value=\"http://www.youtube.com/v/vRlo6Kjy02M&hl=en_US&fs=1\"></param><param name=\"allowFullScreen\" value=\"true\"></param><param name=\"allowscriptaccess\" value=\"always\"></param><embed src=\"http://www.youtube.com/v/vRlo6Kjy02M&hl=en_US&fs=1\" type=\"application/x-shockwave-flash\" allowscriptaccess=\"always\" allowfullscreen=\"true\" width=\"480\" height=\"385\"></embed></object>','2010-07-23 14:09:59'),(9,13,'<ul class=\"menu\">\r\n<li>Suits mid-level PHP programmers</li>\r\n<li>Templating for each page</li>\r\n<li>Easy to setup</li>\r\n</ul>','2014-01-02 13:46:38'),(11,15,'<div class=\"paraside\">\r\n<h4>What is?</h4>\r\n<h5>About »</h5>\r\n<p class=\"parabottom\"><img src=\"templates/vizcreations/images/vteam.jpg\" alt=\"Team\" border=\"0\" align=\"right\" />Webauro is an open source web application framework well suited for efficient programmers well versed with PHP.</p>\r\n\r\n<h5>Focus »</h5>\r\n<p class=\"parabottom\">The main reason for development of this software was to make sure we do not get off the PHP core API signatures, and have a light weight content management system to add to it.</p>\r\n<br />\r\n<h5>Next »</h5>\r\n<p class=\"parabottom\"><small class=\"small\">Webauro has a cloud based super-control system that works and also feels like it, it\'s called Prasaasna and you can straight away test it at www.prasaasna.com.</small></p>\r\n</div>\r\n<img src=\"templates/vizcreations/images/head2.jpg\" alt=\"\" border=\"0\" style=\"width: 100%;\" />','2014-01-02 13:43:41'),(12,16,'<script type=\"text/javascript\"><!--\r\ngoogle_ad_client = \"pub-5541402001080255\";\r\n/* 468x60, VC Text Banner, created 8/28/10 */\r\ngoogle_ad_slot = \"0373694304\";\r\ngoogle_ad_width = 468;\r\ngoogle_ad_height = 60;\r\n//-->\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"http://pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>','2010-08-29 07:43:38'),(13,17,'<script type=\"text/javascript\">\r\n\r\n  var _gaq = _gaq || [];\r\n  _gaq.push([\'_setAccount\', \'UA-24695839-1\']);\r\n  _gaq.push([\'_trackPageview\']);\r\n\r\n  (function() {\r\n    var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;\r\n    ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';\r\n    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);\r\n  })();\r\n\r\n</script>','2011-07-22 04:19:47');
/*!40000 ALTER TABLE `tbl_frags_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags_pos`
--

DROP TABLE IF EXISTS `tbl_frags_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags_pos` (
  `frag_pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags_pos`
--

LOCK TABLES `tbl_frags_pos` WRITE;
/*!40000 ALTER TABLE `tbl_frags_pos` DISABLE KEYS */;
INSERT INTO `tbl_frags_pos` VALUES (1,1,1,'2012-11-16 16:18:20'),(2,2,1,'2014-01-02 13:40:44'),(3,3,1,'2010-04-25 12:06:19'),(4,4,2,'2010-06-06 06:28:12'),(5,5,1,'2010-07-24 07:59:46'),(6,6,3,'2011-07-30 12:12:49'),(7,7,2,'2010-09-21 06:40:53'),(8,8,1,'2010-08-26 08:34:56'),(9,9,3,'2014-01-02 13:45:29'),(10,10,2,'2011-06-15 19:18:34'),(11,11,3,'2011-07-18 09:59:50'),(12,12,1,'2010-07-23 14:09:59'),(13,13,3,'2014-01-02 13:46:38'),(14,14,2,'2012-11-16 16:18:11'),(15,15,1,'2014-01-02 13:43:41'),(16,16,1,'2010-08-29 07:43:38'),(17,17,1,'2011-07-22 04:19:47');
/*!40000 ALTER TABLE `tbl_frags_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags_status`
--

DROP TABLE IF EXISTS `tbl_frags_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags_status` (
  `frag_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `frag_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags_status`
--

LOCK TABLES `tbl_frags_status` WRITE;
/*!40000 ALTER TABLE `tbl_frags_status` DISABLE KEYS */;
INSERT INTO `tbl_frags_status` VALUES (1,1,1,'2010-06-03 18:47:07'),(2,2,1,'2010-06-03 18:47:07'),(3,3,1,'2010-06-03 18:47:07'),(4,4,1,'2010-06-03 18:47:07'),(5,5,0,'2010-06-03 18:47:07'),(6,6,0,'2010-06-04 12:42:26'),(7,7,0,'2010-06-04 12:53:14'),(8,8,0,'2010-06-04 13:06:27'),(9,9,1,'2010-06-06 06:31:53'),(10,10,1,'2010-06-07 11:03:42'),(11,11,1,'2010-06-07 11:04:21'),(12,12,0,'2010-07-23 14:01:26'),(13,13,1,'2010-07-24 07:24:21'),(14,14,1,'2010-08-29 06:59:30'),(15,15,1,'2010-08-29 07:40:10'),(16,16,0,'2010-08-29 07:43:38'),(17,17,1,'2011-07-22 04:19:47');
/*!40000 ALTER TABLE `tbl_frags_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags_types`
--

DROP TABLE IF EXISTS `tbl_frags_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags_types` (
  `frag_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_type_name` varchar(255) NOT NULL,
  `frag_type_desc` text NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags_types`
--

LOCK TABLES `tbl_frags_types` WRITE;
/*!40000 ALTER TABLE `tbl_frags_types` DISABLE KEYS */;
INSERT INTO `tbl_frags_types` VALUES (1,'latestarticles','Frag displaying latest articles from a particular category','2010-04-23 16:23:58'),(2,'content','Frag displaying text and HTML content','2010-04-23 16:23:58'),(3,'menu','Frag displaying a menu','2010-04-25 11:27:37'),(4,'login','User Login','2010-04-26 11:20:58'),(5,'search','Website Search','2010-04-26 11:20:58');
/*!40000 ALTER TABLE `tbl_frags_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_frags_types_rel`
--

DROP TABLE IF EXISTS `tbl_frags_types_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_frags_types_rel` (
  `frag_type_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `frag_id` int(11) NOT NULL,
  `frag_type_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`frag_type_rel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_frags_types_rel`
--

LOCK TABLES `tbl_frags_types_rel` WRITE;
/*!40000 ALTER TABLE `tbl_frags_types_rel` DISABLE KEYS */;
INSERT INTO `tbl_frags_types_rel` VALUES (1,1,1,'2012-11-16 16:18:20'),(2,2,2,'2014-01-02 13:40:44'),(3,3,3,'2010-04-25 11:27:37'),(4,4,4,'2010-06-06 06:28:12'),(5,5,2,'2010-07-24 07:59:46'),(6,6,2,'2011-07-30 12:12:49'),(7,7,2,'2010-09-21 06:40:53'),(8,8,2,'2010-08-26 08:34:56'),(9,9,2,'2014-01-02 13:45:29'),(10,10,2,'2011-06-15 19:18:34'),(11,11,2,'2011-07-18 09:59:50'),(12,12,2,'2010-07-23 14:09:59'),(13,13,2,'2014-01-02 13:46:38'),(14,14,1,'2012-11-16 16:18:11'),(15,15,2,'2014-01-02 13:43:41'),(16,16,2,'2010-08-29 07:43:38'),(17,17,2,'2011-07-22 04:19:47');
/*!40000 ALTER TABLE `tbl_frags_types_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menuitems`
--

DROP TABLE IF EXISTS `tbl_menuitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menuitems` (
  `menuitem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_name` varchar(100) NOT NULL,
  `menuitem_link` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menuitems`
--

LOCK TABLES `tbl_menuitems` WRITE;
/*!40000 ALTER TABLE `tbl_menuitems` DISABLE KEYS */;
INSERT INTO `tbl_menuitems` VALUES (1,'Home','index/home/','2012-11-16 16:14:05'),(2,'Connect','contact/contact_form/','2012-11-16 16:14:15');
/*!40000 ALTER TABLE `tbl_menuitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menuitems_menus`
--

DROP TABLE IF EXISTS `tbl_menuitems_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menuitems_menus` (
  `menuitem_menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menuitems_menus`
--

LOCK TABLES `tbl_menuitems_menus` WRITE;
/*!40000 ALTER TABLE `tbl_menuitems_menus` DISABLE KEYS */;
INSERT INTO `tbl_menuitems_menus` VALUES (1,1,1,'2012-11-16 16:14:05'),(2,2,1,'2012-11-16 16:14:15');
/*!40000 ALTER TABLE `tbl_menuitems_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menuitems_parents`
--

DROP TABLE IF EXISTS `tbl_menuitems_parents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menuitems_parents` (
  `menuitem_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `parent_menuitem_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_rel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menuitems_parents`
--

LOCK TABLES `tbl_menuitems_parents` WRITE;
/*!40000 ALTER TABLE `tbl_menuitems_parents` DISABLE KEYS */;
INSERT INTO `tbl_menuitems_parents` VALUES (1,1,0,'2012-11-16 16:14:05'),(2,2,0,'2012-11-16 16:14:46');
/*!40000 ALTER TABLE `tbl_menuitems_parents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menuitems_status`
--

DROP TABLE IF EXISTS `tbl_menuitems_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menuitems_status` (
  `menuitem_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `menuitem_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menuitems_status`
--

LOCK TABLES `tbl_menuitems_status` WRITE;
/*!40000 ALTER TABLE `tbl_menuitems_status` DISABLE KEYS */;
INSERT INTO `tbl_menuitems_status` VALUES (1,1,1,'2012-11-16 16:14:05'),(2,2,1,'2012-11-16 16:14:15');
/*!40000 ALTER TABLE `tbl_menuitems_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menuitems_templates`
--

DROP TABLE IF EXISTS `tbl_menuitems_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menuitems_templates` (
  `menuitem_template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menuitems_templates`
--

LOCK TABLES `tbl_menuitems_templates` WRITE;
/*!40000 ALTER TABLE `tbl_menuitems_templates` DISABLE KEYS */;
INSERT INTO `tbl_menuitems_templates` VALUES (1,1,1,'2012-11-16 16:14:05'),(2,2,2,'2012-11-16 16:14:15');
/*!40000 ALTER TABLE `tbl_menuitems_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menuitems_views`
--

DROP TABLE IF EXISTS `tbl_menuitems_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menuitems_views` (
  `menuitem_view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menuitem_id` int(11) NOT NULL,
  `view_id` int(11) NOT NULL,
  `argument_value` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menuitem_view_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menuitems_views`
--

LOCK TABLES `tbl_menuitems_views` WRITE;
/*!40000 ALTER TABLE `tbl_menuitems_views` DISABLE KEYS */;
INSERT INTO `tbl_menuitems_views` VALUES (1,1,1,'','2012-11-16 16:14:05'),(2,2,3,'','2012-11-16 16:14:15');
/*!40000 ALTER TABLE `tbl_menuitems_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menus`
--

DROP TABLE IF EXISTS `tbl_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menus` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menus`
--

LOCK TABLES `tbl_menus` WRITE;
/*!40000 ALTER TABLE `tbl_menus` DISABLE KEYS */;
INSERT INTO `tbl_menus` VALUES (1,'topmenu','2010-04-29 17:12:48'),(2,'sidemenu','2010-03-18 12:37:29'),(3,'footmenu','2010-03-18 12:37:29');
/*!40000 ALTER TABLE `tbl_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menus_pos`
--

DROP TABLE IF EXISTS `tbl_menus_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menus_pos` (
  `menu_pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `pos_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menus_pos`
--

LOCK TABLES `tbl_menus_pos` WRITE;
/*!40000 ALTER TABLE `tbl_menus_pos` DISABLE KEYS */;
INSERT INTO `tbl_menus_pos` VALUES (1,1,1,'2010-03-18 12:37:30'),(2,2,2,'2010-03-18 12:37:30'),(3,3,3,'2010-03-18 12:37:30');
/*!40000 ALTER TABLE `tbl_menus_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menus_status`
--

DROP TABLE IF EXISTS `tbl_menus_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menus_status` (
  `menu_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `menu_status` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`menu_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menus_status`
--

LOCK TABLES `tbl_menus_status` WRITE;
/*!40000 ALTER TABLE `tbl_menus_status` DISABLE KEYS */;
INSERT INTO `tbl_menus_status` VALUES (1,1,1,'2010-06-03 18:47:07'),(2,2,1,'2010-06-03 18:47:07'),(3,3,1,'2010-06-03 18:47:07');
/*!40000 ALTER TABLE `tbl_menus_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pos`
--

DROP TABLE IF EXISTS `tbl_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pos` (
  `pos_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pos_name` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pos_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pos`
--

LOCK TABLES `tbl_pos` WRITE;
/*!40000 ALTER TABLE `tbl_pos` DISABLE KEYS */;
INSERT INTO `tbl_pos` VALUES (1,'top','2010-03-18 12:37:30'),(2,'side','2010-03-18 12:37:30'),(3,'foot','2010-03-18 12:37:30');
/*!40000 ALTER TABLE `tbl_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_seo`
--

DROP TABLE IF EXISTS `tbl_seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_seo` (
  `seo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seo_title` varchar(50) NOT NULL,
  `seo_keywords` varchar(255) NOT NULL,
  `seo_description` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`seo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_seo`
--

LOCK TABLES `tbl_seo` WRITE;
/*!40000 ALTER TABLE `tbl_seo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_templates`
--

DROP TABLE IF EXISTS `tbl_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_name` varchar(50) NOT NULL,
  `template_author` varchar(50) NOT NULL,
  `template_createddate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_templates`
--

LOCK TABLES `tbl_templates` WRITE;
/*!40000 ALTER TABLE `tbl_templates` DISABLE KEYS */;
INSERT INTO `tbl_templates` VALUES (3,'webauro_main','Webauro','2014-01-02 14:20:47','2014-01-02 14:20:47'),(4,'webauro_inner','Webauro','2014-01-02 14:20:56','2014-01-02 14:20:56');
/*!40000 ALTER TABLE `tbl_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_status` tinyint(1) NOT NULL,
  `user_online` tinyint(1) NOT NULL,
  `user_location` varchar(255) NOT NULL,
  `user_joined` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_image` varchar(200) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users`
--

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` VALUES (1,'superadmin','17c4520f6cfd1ab53d8745e84681eb49','superadmin@webauro.com',1,1,'Hyderabad, India','2010-03-23 15:01:13','','2014-01-02 14:43:56'),(2,'admin','21232f297a57a5a743894a0e4a801fc3','admin@webauro.com',1,0,'Hyderabad, India.','2012-12-13 13:12:34','','2014-01-02 14:38:47');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users_roles`
--

DROP TABLE IF EXISTS `tbl_users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users_roles` (
  `user_role_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(50) NOT NULL,
  `user_role_desc` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users_roles`
--

LOCK TABLES `tbl_users_roles` WRITE;
/*!40000 ALTER TABLE `tbl_users_roles` DISABLE KEYS */;
INSERT INTO `tbl_users_roles` VALUES (1,'superadmin','Super Administrators','2010-03-23 15:01:13'),(2,'admin','Administrators','2010-03-23 15:01:13'),(3,'user','Standard Users','2010-04-23 15:44:48');
/*!40000 ALTER TABLE `tbl_users_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_users_roles_rel`
--

DROP TABLE IF EXISTS `tbl_users_roles_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_users_roles_rel` (
  `user_role_rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_role_rel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_users_roles_rel`
--

LOCK TABLES `tbl_users_roles_rel` WRITE;
/*!40000 ALTER TABLE `tbl_users_roles_rel` DISABLE KEYS */;
INSERT INTO `tbl_users_roles_rel` VALUES (1,1,1,'2010-03-23 15:01:13'),(2,2,2,'2012-12-13 13:12:34');
/*!40000 ALTER TABLE `tbl_users_roles_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_views`
--

DROP TABLE IF EXISTS `tbl_views`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_views` (
  `view_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `view_name` varchar(50) NOT NULL,
  `argument` tinyint(1) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`view_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_views`
--

LOCK TABLES `tbl_views` WRITE;
/*!40000 ALTER TABLE `tbl_views` DISABLE KEYS */;
INSERT INTO `tbl_views` VALUES (1,'home',0,'2010-03-18 12:37:30'),(2,'full_article',1,'2010-03-18 12:37:30'),(3,'contact_form',0,'2010-03-18 12:37:30'),(4,'latest_category_articles',1,'2010-03-24 11:59:01'),(5,'registration',0,'2010-04-26 17:25:53'),(6,'login',0,'2010-04-26 17:26:02');
/*!40000 ALTER TABLE `tbl_views` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_views_arguments`
--

DROP TABLE IF EXISTS `tbl_views_arguments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_views_arguments` (
  `view_argument_id` int(11) NOT NULL AUTO_INCREMENT,
  `view_id` int(11) NOT NULL,
  `argument_table` varchar(50) DEFAULT NULL,
  `argument_id` varchar(50) NOT NULL,
  `argument_name` varchar(50) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`view_argument_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_views_arguments`
--

LOCK TABLES `tbl_views_arguments` WRITE;
/*!40000 ALTER TABLE `tbl_views_arguments` DISABLE KEYS */;
INSERT INTO `tbl_views_arguments` VALUES (1,2,'articles','article_id','article_title','2010-03-19 16:50:50'),(2,4,'ar_categories','ar_category_id','ar_category_name','2010-03-24 11:59:01');
/*!40000 ALTER TABLE `tbl_views_arguments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_visitors`
--

DROP TABLE IF EXISTS `tbl_visitors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_visitors` (
  `visitor_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_ip` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`visitor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_visitors`
--

LOCK TABLES `tbl_visitors` WRITE;
/*!40000 ALTER TABLE `tbl_visitors` DISABLE KEYS */;
INSERT INTO `tbl_visitors` VALUES (2,'127.0.5.1','2012-11-16 16:13:15'),(3,'127.0.5.1','2012-11-16 16:14:17'),(4,'127.0.5.1','2012-11-16 16:14:19'),(5,'127.0.5.1','2012-11-16 16:14:20'),(6,'127.0.5.1','2012-11-16 16:14:20'),(7,'127.0.5.1','2012-11-16 16:14:54'),(8,'127.0.5.1','2012-11-16 16:14:55'),(9,'127.0.5.1','2012-11-16 16:14:55'),(10,'127.0.5.1','2012-11-16 16:14:56'),(11,'127.0.5.1','2012-11-16 16:15:04'),(12,'127.0.5.1','2012-11-16 16:15:04'),(13,'127.0.5.1','2012-11-16 16:15:15'),(14,'127.0.5.1','2012-11-16 16:15:52'),(15,'127.0.5.1','2012-11-16 16:17:45'),(16,'127.0.5.1','2012-11-16 16:17:48'),(17,'127.0.5.1','2012-11-16 16:17:55'),(18,'127.0.5.1','2012-11-16 16:18:31'),(19,'127.0.5.1','2012-11-16 16:18:34'),(20,'127.0.5.1','2012-11-16 16:18:37'),(21,'127.0.5.1','2012-11-16 16:18:44'),(22,'127.0.5.1','2012-12-01 17:11:21'),(23,'127.0.4.1','2013-02-21 16:42:55'),(24,'127.0.4.1','2013-02-21 17:23:49'),(25,'127.0.4.1','2013-02-21 17:24:05'),(26,'127.0.4.1','2013-02-21 17:26:37'),(27,'127.0.4.1','2013-02-21 17:27:24'),(28,'127.0.4.1','2013-02-21 17:27:46'),(29,'127.0.4.1','2013-02-21 17:27:52'),(30,'127.0.4.1','2013-02-21 17:28:10'),(31,'127.0.4.1','2013-02-21 17:28:35'),(32,'127.0.4.1','2013-02-21 17:29:06'),(33,'127.0.4.1','2013-02-21 17:29:23'),(34,'127.0.4.1','2013-02-21 17:29:54'),(35,'127.0.4.1','2013-02-21 17:30:34'),(36,'127.0.4.1','2013-02-21 17:30:50'),(37,'127.0.4.1','2013-02-21 17:30:57'),(38,'127.0.4.1','2013-02-21 17:31:24'),(39,'127.0.4.1','2013-02-21 17:31:45'),(40,'127.0.4.1','2013-02-21 17:31:49'),(41,'127.0.4.1','2013-02-21 17:34:02'),(42,'127.0.4.1','2013-02-21 17:34:11'),(43,'127.0.4.1','2013-02-21 17:35:09'),(44,'127.0.4.1','2013-02-21 17:35:12'),(45,'127.0.4.1','2013-02-21 17:35:16'),(46,'127.0.4.1','2013-02-21 17:35:18'),(47,'127.0.4.1','2013-02-21 17:35:18'),(48,'127.0.4.1','2013-02-21 17:35:24'),(49,'127.0.4.1','2013-02-21 17:35:24'),(50,'127.0.4.1','2013-02-21 17:35:25'),(51,'127.0.4.1','2013-02-21 17:37:29'),(52,'127.0.4.1','2013-02-21 17:37:31'),(53,'127.0.4.1','2013-02-21 17:37:33'),(54,'127.0.4.1','2013-02-21 17:37:34'),(55,'127.0.4.1','2013-02-23 16:34:16'),(56,'127.0.4.1','2013-02-23 16:38:09'),(57,'127.0.4.1','2013-02-23 16:38:39'),(58,'127.0.4.1','2013-02-23 16:38:40'),(59,'127.0.4.1','2013-02-23 16:38:45'),(60,'127.0.4.1','2013-02-23 16:38:45'),(61,'127.0.4.1','2013-02-23 16:38:54'),(62,'127.0.4.1','2013-02-23 16:38:55'),(63,'127.0.4.1','2013-02-23 16:38:59'),(64,'127.0.4.1','2013-02-23 16:38:59'),(65,'127.0.4.1','2013-02-23 16:39:05'),(66,'127.0.4.1','2013-02-23 16:39:06'),(67,'127.0.4.1','2013-02-23 17:06:07'),(68,'127.0.4.1','2013-02-23 17:06:17'),(69,'127.0.4.1','2013-02-23 17:06:17'),(70,'127.0.4.1','2013-02-23 17:06:23'),(71,'127.0.4.1','2013-02-24 12:13:34'),(72,'127.0.4.1','2013-03-05 12:33:51'),(73,'127.0.4.1','2013-03-05 13:33:41'),(74,'127.0.4.1','2013-03-05 13:33:47'),(75,'127.0.4.1','2013-03-05 13:33:47'),(76,'127.0.4.1','2013-03-05 13:33:51'),(77,'127.0.4.1','2013-03-05 13:33:51'),(78,'127.0.4.1','2013-03-05 13:33:56'),(79,'127.0.4.1','2013-03-06 12:45:37'),(80,'127.0.4.1','2013-03-06 15:32:18'),(81,'127.0.0.1','2013-09-01 15:46:03'),(82,'127.0.0.1','2013-11-15 16:07:37'),(83,'127.0.0.1','2013-11-15 16:07:46'),(84,'127.0.0.1','2013-11-15 16:07:52'),(85,'127.0.0.1','2013-12-28 19:38:52'),(86,'127.0.0.1','2013-12-28 19:39:01'),(87,'127.0.0.1','2013-12-28 19:39:05'),(88,'127.0.0.1','2013-12-28 19:39:05'),(89,'127.0.0.1','2013-12-28 19:39:07'),(90,'127.0.0.1','2014-01-02 13:39:29'),(91,'127.0.0.1','2014-01-02 13:40:08'),(92,'127.0.0.1','2014-01-02 13:40:47'),(93,'127.0.0.1','2014-01-02 13:43:43'),(94,'127.0.0.1','2014-01-02 13:45:33');
/*!40000 ALTER TABLE `tbl_visitors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-02 15:09:13
