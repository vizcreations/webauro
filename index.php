<?php
/* Begin a Session */
session_start();

/**
 * @abstract Front end Entry Point - Index
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* During development mode change setting for errors to appear */
/* *** */
error_reporting(E_ALL); // "E_ALL" to show, "NULL" to hide
ini_set("display_errors", "On"); // "On" to show, "Off" to hide

/* Set a root path variable to prevent direct include file access */
$path = realpath(dirname( __FILE__ ));
define( "__ROOT", $path );


/* Declarations and Object initializations */
/* *** */
require_once( "includes".DIRECTORY_SEPARATOR."init.php" );
// $registry->router->chmod_11oo10(__ROOT.DS."configuration.xml", 644);


/**
 * Start ouput buffering the HTML content which is getting generated
 */
$registry->cache->startOB();

/* Generate HTML page */
try {
	$registry->router->loadPage();
} catch(Exception $e) {
	die( $e->getMessage() );
}

/** Caching in configuration will decide on caching */
if($webserver['caching'] == true) {
	$zero = $registry->router->controller;
	$one = $registry->router->action;
	$two = $registry->router->id;
	if(!$two) $two = "default";
	
	if($registry->cache->cachedFileExists($zero, $one, $two))
		$registry->cache->readCachedFile();
	else
		$registry->cache->createCacheFile($zero, $one, $two);
} else {
	$registry->cache->flushOB(); // Just let out the stream to client
}

/** Open database connections will be closed */
if(DB::$conn)
	DB::closeInstance();
