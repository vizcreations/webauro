<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Frags Model Class to apply business logic for fragments in the application
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class FragsModel extends DB {
	public function save($vars, $entity, $option) {
		if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		}
	}

	public function getFragType($fragname) {
		$query = " SELECT #__frags_types.frag_type_name ".
					" FROM #__frags, #__frags_types, #__frags_types_rel ".
					" WHERE #__frags.frag_name='$fragname' ".
					" AND #__frags_types_rel.frag_id=#__frags.frag_id ".
					" AND #__frags_types.frag_type_id=#__frags_types_rel.frag_type_id ";
		$this->setQuery($query);
		$data = $this->getResult();
		return $data;
	}
};

