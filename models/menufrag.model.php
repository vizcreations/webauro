<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Menu Frag Model Class to apply business logic to menus
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class MenuFragModel extends FragModel {
	public function getFrag($fragname) {
		$query = " SELECT * ".
					" FROM #__frags, #__frags_pos, #__pos, #__frags_status ".
					" WHERE #__frags.frag_name='$fragname' ".
					" AND #__frags_pos.frag_id=#__frags.frag_id ".
					" AND #__pos.pos_id=#__frags_pos.pos_id ".
					" AND #__frags_status.frag_id=#__frags.frag_id ".
					" AND #__frags_status.frag_status='1' ";
		$this->setQuery($query);

		$row = $this->getRow();

		return $row;
	}

	public function getParentMenuitems($menuname) {
		$query = " SELECT * ".
					" FROM #__menus, #__menuitems, #__menuitems_menus, #__menuitems_status, #__menuitems_parents ".
					" WHERE #__menus.menu_name='$menuname' ".
					" AND #__menuitems_menus.menu_id=#__menus.menu_id ".
					" AND #__menuitems.menuitem_id=#__menuitems_menus.menuitem_id ".
					" AND #__menuitems_status.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__menuitems_status.menuitem_status='1' ".
					" AND #__menuitems_parents.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__menuitems_parents.parent_menuitem_id='0' ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}

	public function getSubMenuItems($menuitem) {
		$query = " SELECT * ".
					" FROM #__menuitems, #__menuitems_status, #__menuitems_parents ".
					" WHERE #__menuitems_parents.parent_menuitem_id='$menuitem' ".
					" AND #__menuitems.menuitem_id=#__menuitems_parents.menuitem_id ".
					" AND #__menuitems_status.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__menuitems_status.menuitem_status='1' ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}

	public function getMenuname($menuid) {
		$query = " SELECT menu_name ".
					" FROM #__menus ".
					" WHERE menu_id='$menuid' ";
		$this->setQuery($query);
		$data = $this->getResult();
		return $data;
	}
};

