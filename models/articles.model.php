<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Articles Model Class to apply business logic for articles component
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class ArticlesModel extends DB {
	public function save($vars, $entity, $option) {
		if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		}
		switch($entity) {			
			case 'comment':				
				switch($option) {					
					case 'add':						
						$query = " INSERT INTO #__ar_comments(ar_comment_name, ar_comment_email, ".									
						" ar_comment_uri, ar_comment_content, timestamp) ".									
						" VALUES('{$vars['articlecommentname']}', ".									
						" '{$vars['articlecommentemail']}', ".									
						" '{$vars['articlecommenturi']}', ".									
						" '{$vars['articlecommentcontent']}', ".									
						" NOW()) ";						
						break;				
				}				
				break;			
				case 'comment_rel':				
					switch($option) {					
						case 'add':						
							$query = " INSERT INTO #__articles_comments_rel(article_id, ar_comment_id, timestamp) ".									
							" VALUES('{$vars['articleid']}', '{$vars['arcommentid']}', NOW()) ";						
							break;				
					}				
					break;		
		}		
		$this->setQuery($query);		
		$this->execute();
	}
	
	public function getArticle($id) {
		$query = " SELECT * ".
					" FROM #__ar_categories, #__articles_categories_rel, #__articles, #__articles_status ".
					" WHERE #__articles.article_id='$id' ".
					" AND #__articles_categories_rel.article_id=#__articles.article_id ".
					" AND #__ar_categories.ar_category_id=#__articles_categories_rel.ar_category_id ".
					" AND #__articles_status.article_id=#__articles.article_id ".
					" AND #__articles_status.article_status='1' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}		
	
	public function getArticleCategory($id) {		
		$query = " SELECT * ".					
					" FROM #__ar_categories ".					
					" WHERE ar_category_id='$id' ";		
		$this->setQuery($query);		
		$row = $this->getRow();		
		return $row;	
	}		
	
	public function getArticleComments($articleid) {		
		$query = " SELECT * FROM ".					
					" #__ar_comments, #__articles_comments_rel ".					
					" WHERE #__articles_comments_rel.article_id='$articleid' ".					
					" AND #__ar_comments.ar_comment_id=#__articles_comments_rel.ar_comment_id ".					
					" ORDER BY #__ar_comments.timestamp ".					
					" DESC ";		
		$this->setQuery($query);		
		$rows = $this->getRows();		
		return $rows;	
	}
	
	public function getCategoryArticles($catid, $offset, $rowsperpage) {
		$query = " SELECT * ".
					" FROM #__ar_categories, #__articles, #__articles_categories_rel, #__articles_status ".
					" WHERE #__ar_categories.ar_category_id='$catid' ".
					" AND #__articles_categories_rel.ar_category_id=#__ar_categories.ar_category_id ".
					" AND #__articles.article_id=#__articles_categories_rel.article_id ".
					" AND #__articles_status.article_id=#__articles.article_id ".
					" AND #__articles_status.article_status='1' ".
					" ORDER BY #__articles.article_id ".
					" DESC ".
					" LIMIT $offset, $rowsperpage ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getCatArticlesCount($catid) {
		$query = " SELECT COUNT(*) AS count ".
					" FROM #__ar_categories, #__articles, #__articles_categories_rel, #__articles_status ".
					" WHERE #__ar_categories.ar_category_id='$catid' ".
					" AND #__articles_categories_rel.ar_category_id=#__ar_categories.ar_category_id ".
					" AND #__articles.article_id=#__articles_categories_rel.article_id ".
					" AND #__articles_status.article_id=#__articles.article_id ".
					" AND #__articles_status.article_status='1' ";
		$this->setQuery($query);
		$count = $this->getResult();
		return $count;
	}
	
	public function getLatestArticles() {
		$query = " SELECT * ".
					" FROM #__articles ".
					" ORDER BY article_id ".
					" DESC ".
					" LIMIT 0, 5 ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
	
	public function getArticleCommentsCount($articleid) {
		$query = " SELECT COUNT(*) ".
					" FROM #__articles_comments_rel ".
					" WHERE article_id='$articleid' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
	
	public function getCategory($catid) {
		$query = " SELECT * ".
					" FROM #__ar_categories ".
					" WHERE ar_category_id='$catid' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getArticleSEO($id) {
		$query = " SELECT * ".
					" FROM #__articles_seo, #__seo ".
					" WHERE #__articles_seo.article_id='$id' ".
					" AND #__seo.seo_id=#__articles_seo.seo_id ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}
	
	public function getCStatus($articleid) {
		$query = " SELECT ar_comments_status ".
					" FROM #__ar_comments_status ".
					" WHERE article_id='$articleid' ";
		$this->setQuery($query);
		$data = $this->getResult();
		return $data;	
	}
};

