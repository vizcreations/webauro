<?php

/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Index Model Class
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Class declaration for Index Model */

class IndexModel extends DB {
	
	
	/**
	 * (non-PHPdoc)
	 * @see application/DB#save($vars, $entity, $option)
	 */
	public function save($vars, $entity, $option) {
		foreach($vars as $key => $val) {
			$vars[$key] = mysql_real_escape_string($val);
		}
	}

	public function getWebTemplate($controller, $view) { // Method to be moved to Template model
		$query = " SELECT #__templates.template_id FROM #__templates, #__controllers, #__views, ".
					" #__menuitems, #__controllers_views, #__menuitems_views, #__menuitems_templates ".
					" WHERE #__controllers.controller_name='$controller' ".
					" AND #__views.view_name='$view' ".
					" AND #__controllers_views.controller_id=#__controllers.controller_id ".
					" AND #__controllers_views.view_id=#__views.view_id ".
					" AND #__menuitems_views.view_id=#__views.view_id ".
					" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
					" AND #__templates.template_id=#__menuitems_templates.template_id ";
		$this->setQuery($query);
		$template = $this->getResult();
		return $template;
	}

	public function getWebTemplateName($templateid) { // Method to be moved to Template model
		$query = " SELECT template_name ".
					" FROM #__templates ".
					" WHERE template_id='$templateid' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}
};

