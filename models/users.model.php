<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Users Model Class to apply business logic for the Users component
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class UsersModel extends DB {
	
	function UsersModel() {
		$this->tables['members'] = array('email',
											'username',
											'password',
											'country',
											'city',
											'joined',
											'website');
	}
	
	/**
	 * 
	 * (non-PHPdoc)
	 * @see application/DB#save($vars, $entity, $option)
	 */
	public function save($vars, $entity, $option) {
		$vars = $this->escapeString($vars);
		switch($entity) {
			case 'users':
				unset($vars['save']);
				unset($vars['route']);
				unset($vars['form_token']);
				$date = getdate();
				$year = $date['year'];
				$month = $date['mon'];
				$day = $date['mday'];
				$vars['joined'] = $year.'-'.$month.'-'.$day;
				$nvars = array();
				$count = 0;				
				unset($vars['password2']);
				
				require_once( __LIB.DS.'mcrypt.class.php' );
				$crypt = new ProCrypt();
				if(!isset($vars['status'])) $vars['status'] = 0;
				switch($option) {
					case 'add':
						/*$query = " INSERT INTO #__users(".implode(",", $this->tables['members']).") ".
									" VALUES('".implode("','", $thevars)."') ";*/
						$query = " INSERT INTO #__users(user_name, user_email, user_password, user_location, user_joined, ".
									" user_image, user_status, user_online, timestamp) ".
									" VALUES('{$vars['uname']}', '{$vars['email']}', MD5('{$vars['password']}'), '{$vars['location']}', NOW(), ".
									" '', '{$vars['status']}', '0', NOW()) ";
						break;
					case 'edit':
						$query = " UPDATE #__users ".
									" SET user_name='{$vars['uname']}', ".
									" user_email='{$vars['email']}', ".
									" user_location='{$vars['location']}', ".
									" user_image='{$vars['image']}', ".
									" timestamp=NOW() ".
									" WHERE user_id='{$vars['userid']}' ";
						
						break;
				}
				break;
			case 'users_roles_rel':
				switch($option) {
					case 'add':
						$query = " INSERT INTO #__users_roles_rel(user_id, user_role_id, timestamp) ".
									" VALUES('{$vars['userid']}', '3', NOW()) ";
						break;
				}
				break;
                        case 'users_companies':
                            switch($option) {
                                case 'add':
                                    $query = " INSERT INTO #__users_companies(user_id, company_id, timestamp) ".
                                                " VALUES('{$vars['userid']}', '{$vars['cid']}', NOW()) ";
                                    break;
                            }
                            break;
			case 'users_image':
				switch($option) {
					default:
						$query = " UPDATE #__users SET user_image='{$vars['image']}' ".
									" WHERE user_id='{$vars['userid']}' ";
						break;
				}
				break;
			case 'users_bg':
				switch($option) {
					default:
						$query = " UPDATE #__users SET user_bg_image='{$vars['bg_image']}', ".
									" user_bg_tile='{$vars['tile']}' ".
									" WHERE user_id='{$vars['userid']}' ";
						break;
				}
				break;
			case 'users_pass':
				switch($option) {
					case 'edit':
						$query = " UPDATE #__users ".
									" SET user_password=MD5('{$vars['newpassword']}') ".
									" WHERE user_password='{$vars['oldpassword']}' ";
						break;
				}
				break;
			case 'users_pass_user':
				switch($option) {
					case 'edit':
						$query = " UPDATE #__users ".
									" SET user_password=MD5('{$vars['newpassword']}') ".
									" WHERE user_id='{$vars['userid']}' ";
						break;
				}
				break;
		}
		$this->setQuery($query);
		$this->execute();
	}

	public function checkEmReg($email) {
		$query = " SELECT * FROM #__users ".
					" WHERE user_email='$email' ";
		$this->setQuery($query);
		$count2 = $this->getCount();
		if($count2 < 1) {
			return true;
		} else {
			return false;
		}
	}

	public function checkUser($username) {
		$query = " SELECT * ".
				" FROM #__users ".
				" WHERE user_name='$username' ".
				" AND user_status='1' ";
		$this->setQuery($query);
		$count = $this->getCount();
		if($count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function checkIdUser($userid) {
		$name = explode("-", $userid);
		$fname = $name[0];
		$lname = isset($name[1]) ? $name[1] : '';
		$query = " SELECT * ".
					" FROM #__users ".
					" WHERE user_name='$fname' ";
		$this->setQuery($query);
		$count = $this->getCount();
		if($count < 1) {
			$query = " SELECT * ".
						" FROM #__users ".
						" WHERE user_id='$userid' ";
			$this->setQuery($query);
			$count = $this->getCount();
			if($count < 1) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public function checkEmail($email) {
		$query = " SELECT * ".
					" FROM #__users ".
					" WHERE user_email='$email' ";
		$this->setQuery($query);
		$count = $this->getCount();
		if($count < 1) {
			return false;
		} else {
			return true;
		}
	}

	public function activateUser($id) {
		$query = " UPDATE #__users ".
					" SET user_status='1' ".
					" WHERE user_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}

	/**
	 * 
	 * @param $vars
	 * @return bool
	 */
	public function checkLogin($vars) {
		require_once( __LIB.DS."mcrypt.class.php" );
		$crypt = new ProCrypt();
		$username = $vars['username'];
		$password = $vars['password'];
		//$password = $crypt->encrypt($vars['password']);
		if(!get_magic_quotes_gpc()) {
			$username = mysql_real_escape_string($vars['username']);
			$password = mysql_real_escape_string($vars['password']);
			//$password = $crypt->encrypt(mysql_real_escape_string($vars['password']));
		}
		$query = "SELECT * ".
					" FROM #__users, #__users_roles, #__users_roles_rel ".
					" WHERE (#__users.user_name='$username' ".
					" OR #__users.user_email='$username') ".
					" AND #__users.user_password=MD5('$password') ".
					" AND #__users.user_status='1' ".
					" AND #__users_roles_rel.user_id=#__users.user_id ".
					" AND #__users_roles_rel.user_role_id=#__users_roles.user_role_id ";
		$this->setQuery($query);
		$count = $this->getCount();
		if($count == 0) {
			return false;
		} else {
			return true;
		}
	}

	public function setSession($id) {
		$query = " UPDATE #__users ".
					" SET user_online='1' ".
					" WHERE user_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}

	public function unsetSession($id) {
		$query = " UPDATE #__users ".
					" SET user_online='0' ".
					" WHERE user_id='$id' ";
		$this->setQuery($query);
		$this->execute();
	}

	public function getIdUser($id) {
		$name = explode("-", $id);
		if(isset($name[0])) $fname = $name[0]; else $fname = '';
		if(isset($name[1])) $lname = $name[1]; else $lname = '';
		$query = " SELECT * FROM ".
					" #__users ".
					" WHERE user_name='$fname' ";
		$this->setQuery($query);
		$row = $this->getRow();
		if(!$row) {
			$query = " SELECT * FROM ".
						" #__users ".
						" WHERE user_id='$id' ";
			$this->setQuery($query);
			$row = $this->getRow();			
		}
		return $row;
	}

	public function getUser($username) {
		$query = " SELECT * ".
					" FROM #__users ".
					" WHERE user_name='".trim($username)."' ".
					" OR user_email='".trim($username)."' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}

	public function getTheUser($username) {
		$query = " SELECT * ".
					" FROM #__users ".
					" WHERE user_name='$username' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}

	public function getPassUser($password) {
		$query = " SELECT * ".
					" FROM #__users ".
					" WHERE password='$password' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}

	public function getUsername($id) {
		$query = " SELECT user_name ".
					" FROM #__users ".
					" WHERE user_id='$id' ";
		$this->setQuery($query);
		$username = $this->getResult();
		return $username;
	}

	public function clearBG($userid) {
		$query = " UPDATE #__users ".
					" SET user_bg_image='', ".
					" user_bg_tile='0' ".
					" WHERE user_id='$userid' ";
		$this->setQuery($query);
		$this->execute();
	}

	public function saveVisitor($ip) {
		$query = " INSERT INTO #__visitors(visitor_ip, timestamp) ".
					" VALUES('$ip', NOW()) ";
		$this->setQuery($query);
		$this->execute();
	}
};

