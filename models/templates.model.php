<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Templates Model Class to apply business logic for templating in the application
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class TemplatesModel extends DB {
	public function save($vars, $entity, $option) {
		if(!get_magic_quotes_gpc()) {
			foreach($vars as $key => $val) {
				$vars[$key] = mysql_real_escape_string($val);
			}
		}
	}
	
	public function getWebTemplates() {
		$query = " SELECT * ".
				" FROM #__templates ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
};

