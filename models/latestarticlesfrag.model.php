<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Latest Articles Frag Model Class to apply business logic for latest articles frag
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class LatestArticlesFragModel extends FragModel {
	public function getFrag($fragname) {
		$query = " SELECT * ".
					" FROM #__frags, #__frags_pos, #__pos, #__frags_status ".
					" WHERE #__frags.frag_name='$fragname' ".
					" AND #__frags_pos.frag_id=#__frags.frag_id ".
					" AND #__pos.pos_id=#__frags_pos.pos_id ".
					" AND #__frags_status.frag_id=#__frags.frag_id ".
					" AND #__frags_status.frag_status='1' ";
		$this->setQuery($query);
		$row = $this->getRow();
		return $row;
	}

	public function getCatArticles($argument) {
		$query = " SELECT * FROM ".
					" #__articles, #__ar_categories, #__articles_categories_rel, #__articles_status ".
					" WHERE #__ar_categories.ar_category_id='$argument' ".
					" AND #__articles_categories_rel.ar_category_id=#__ar_categories.ar_category_id ".
					" AND #__articles.article_id=#__articles_categories_rel.article_id ".
					" AND #__articles_status.article_status='1' ".
					" AND #__articles_status.article_id=#__articles.article_id ".
					" ORDER BY #__articles.article_id ".
					" DESC ".
					" LIMIT 0, 5 ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}
};

