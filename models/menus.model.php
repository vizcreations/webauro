<?php
/* Security */
defined( "__ROOT" ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Menus Model Class to apply business logic for menus in the application
 * @copyright GNU/GPL
 */

/**
 * @license GNU/GPL 3.0
 *
 *
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */
 
class MenusModel extends DB {
	public function save($vars, $entity, $option) {
		// TODO CODE
	}

	public function getMenuitems($menuname) {
		$query = " SELECT * ".
					" FROM #__menus, #__menuitems, #__menuitems_menus, #__menuitems_status ".
					" WHERE #__menus.menu_name='$menuname' ".
					" AND #__menuitems_menus.menu_id=#__menus.menu_id ".
					" AND #__menuitems.menuitem_id=#__menuitems_menus.menuitem_id ".
					" AND #__menuitems_status.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__menuitems_status.menuitem_status='1' ";
		$this->setQuery($query);
		$rows = $this->getRows();
		return $rows;
	}

	public function getMenuitemWebTemplate($menuitemname) {
		$query = " SELECT template_name ".
					" FROM #__menuitems, #__menuitems_templates, #__templates, #__menuitems_status ".
					" WHERE #__menuitems.menuitem_name='$menuitemname' ".
					" AND #__menuitems_templates.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__templates.template_id=#__menuitems_templates.template_id ".
					" AND #__menuitems_status.menuitem_id=#__menuitems.menuitem_id ".
					" AND #__menuitems_status.menuitem_status='1' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}

	public function getMenuitemLink($menuitemname) {
		$query = " SELECT menuitem_link ".
					" FROM #__menuitems ".
					" WHERE menuitem_name='$menuitemname' ";
		$this->setQuery($query);
		$result = $this->getResult();
		return $result;
	}

	public function getViewId( $controller, $view ) {
		$query = " SELECT #__controllers_views.view_id ".
					" FROM #__views, #__controllers, #__controllers_views ".
					" WHERE #__controllers.controller_name='$controller' ".
					" AND #__views.view_name='$view' ".
					" AND #__controllers_views.controller_id=#__controllers.controller_id ".
					" AND #__controllers_views.view_id=#__views.view_id ";
		$this->setQuery($query);
		$data = $this->getResult();
		return $data;
	}

	/**
	 * Deprecated function
	 */
	/*public function getViewWebTemplate($viewid, $arg) {
		if(!$arg) $arg = '';
		
		$query = " SELECT #__templates.template_name ".
						" FROM #__templates, #__menuitems_views, #__menuitems_templates ".
						" WHERE #__menuitems_views.view_id='$viewid' ".
						" AND #__menuitems_views.argument_value='$arg' ".
						" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
						" AND #__templates.template_id=#__menuitems_templates.template_id ";
		$this->setQuery($query);
		$data = $this->getResult();
		
        // This should only run if arg is empty
		if(!$data && empty($arg)) {
			$query = " SELECT #__templates.template_name ".
						" FROM #__templates, #__menuitems_templates, #__menuitems_views ".
						" WHERE #__menuitems_views.view_id='$viewid' ".
						" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
						" AND #__templates.template_id=#__menuitems_templates.template_id ";
			$this->setQuery($query);
			$data = $this->getResult();
		} else if(!$data && !empty($arg)) {
                    $query = " SELECT #__templates.template_name ".
						" FROM #__templates, #__menuitems_templates, #__menuitems_views ".
						" WHERE #__menuitems_views.view_id='$viewid' ".
                    	" AND #__menuitems.argument_value='$arg' ".
						" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
						" AND #__templates.template_id=#__menuitems_templates.template_id ";
			$this->setQuery($query);
			$data = $this->getResult();
        }
		return $data;
	}*/

	/**
	 * @abstract Recommended function for above
	 * @param Integer $viewid
	 * @param String $arg
	 * @return resource
	 */
	public function getViewWebTemplate($viewid, $arg=null) {
		$data = 0;
		if(!$arg) $arg = '';
	
		$query = " SELECT #__templates.template_name ".
				" FROM #__templates, #__menuitems_views, #__menuitems_templates ".
				" WHERE #__menuitems_views.view_id='$viewid' ".
				" AND #__menuitems_views.argument_value='$arg' ".
				" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
				" AND #__templates.template_id=#__menuitems_templates.template_id ";
		$this->setQuery($query);
		$data = $this->getResult();
	
		if(!$data && !empty($arg)) {
			$arg = '';
			$query = " SELECT #__templates.template_name ".
					" FROM #__templates, #__menuitems_views, #__menuitems_templates ".
					" WHERE #__menuitems_views.view_id='$viewid' ".
					" AND #__menuitems_views.argument_value='$arg' ".
					" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
					" AND #__templates.template_id=#__menuitems_templates.template_id ";
			$this->setQuery($query);
			$data = $this->getResult();
		}
	
		// This should only run if arg is empty
		if(!$data && empty($arg)) {
			$query = " SELECT #__templates.template_name ".
					" FROM #__templates, #__menuitems_templates, #__menuitems_views ".
					" WHERE #__menuitems_views.view_id='$viewid' ".
					" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
					" AND #__templates.template_id=#__menuitems_templates.template_id ";
			$this->setQuery($query);
			$data = $this->getResult();
	
		} else if(!$data && !empty($arg)) {
			$query = " SELECT #__templates.template_name ".
					" FROM #__templates, #__menuitems_templates, #__menuitems_views ".
					" WHERE #__menuitems_views.view_id='$viewid' ".
					" AND #__menuitems.argument_value='$arg' ".
					" AND #__menuitems_templates.menuitem_id=#__menuitems_views.menuitem_id ".
					" AND #__templates.template_id=#__menuitems_templates.template_id ";
			$this->setQuery($query);
			$data = $this->getResult();
				
		}
	
		return $data;
	}
};

