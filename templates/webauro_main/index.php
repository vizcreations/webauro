<?php
/* Security */
defined( '__ROOT' ) or die( "Unauthorized access!" );

/**
 * @abstract Front End Default Template
 * @copyright GNU/GPL
 */

/**
 * @copyright (C) 2009
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 */

/* Development mode so we enable errors to appear */
?>


<!-- HTML -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- W3C Standard MarkUp -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="verify-v1" content="XdPPz+jzay5T6pJsph9ud2ZBPOGoHRdZc6CrtElvHzc=" />
<meta name="y_key" content="f2646f551e7f25c4" />
<?php $this->registry->router->loadSEO(); ?>
<meta name="google-site-verification" content="q1ALSRRJiiZvuN49N5dmvLjegDKik-Uz_i-oXtlfWHY" />
<link rel="stylesheet" href="templates/<?php echo $webTemplate; ?>/stylesheets/styles.css?1.1" type="text/css" media="all" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script type="text/javascript" src="includes/library/js/ajax.js"></script>
<script type="text/javascript" src="includes/library/js/lib.js"></script>
<script type="text/javascript" src="includes/library/js/anime.js"></script>
<link rel="shortcut icon" href="templates/<?php echo $webTemplate; ?>/images/webauro.ico" />
</head>
<body>
	<div id="wrapper">
	<div class="inside">
		<!-- HEADER STARTS -->
		<div id="header">
			<div id="headertop">
				<div id="logo">
					<?php $this->registry->frag->load( 'websitelogo' ); ?>
				</div>				
				<div id="flashad">
					<?php $this->registry->frag->load( 'adsense' ); ?>
				</div>
			</div>
			<div id="headerbottom">
				<div id="menu">
					<?php $this->registry->frag->load( 'topmenu', 'top' ); ?>
				</div>
				<div id="search">
					<?php $this->registry->frag->load( 'search', 'top' ); ?>
				</div>
			</div>
		</div>
		<!-- HEADER ENDS -->
		
		<!-- MID STARTS -->
		<div id="mid">
			<!-- LEFT STARTS -->
			<div id="left">
				<div id="introcontainer">
					<div class="ybar"></div>
					<div id="intro" class="">
						<?php require_once( $view ); ?>
					</div>
				</div>
				<div id="services">
					<div class="services" id="webcontainer">
						<h3>Focus</h3>
						<div class="left">
							<div id="web" class="para">
								<?php $this->registry->frag->load( 'web', 'foot' ); ?>
							</div>
						</div>
					</div>
					<div class="services" id="softcontainer">
						<h3>Research &amp; Study</h3>
						<div id="soft" class="para">
							<?php $this->registry->frag->load( 'exp', 'foot' ); ?>
						</div>
					</div>
				</div> 
			</div>
			<!-- LEFT ENDS -->
			
			<!-- RIGHT STARTS -->
			<div id="right">
				<?php $this->registry->frag->load( 'adsenseside', 'foot' ); ?>
				<?php $this->registry->frag->load( 'userlogin', 'side' ); ?>
				<?php $this->registry->frag->load('latestarticles', 'top'); ?>
				<!--
				<div class="container">
					<table width="100%">
					<tr>
						<td align="center">
							<?php //$this->registry->frag->load( 'adsensecontent', 'side' ); ?>
						</td>
					</tr>
					</table>
				</div>
				-->
				<?php $this->registry->frag->load( 'extlinks', 'side' ); ?>
				<?php $this->registry->frag->load( 'study', 'side' ); ?>
			</div>
			<!-- RIGHT ENDS -->
		</div>
		<!-- MID ENDS -->
		
		<!-- FOOTER STARTS -->
		<div id="footer">
			<!-- Footer content -->
			<table width="100%">
			<tr>
				<td>
					<?php $this->registry->frag->load( 'footer', 'foot' ); ?>
				</td>
			</tr>
			</table>
		</div>
		<div class="ybar" style="width: 960px;"></div>
		<div id="footerex">
		</div>
		<!-- FOOTER ENDS -->
	</div>
	</div>


</body>
</html>
<!-- HTML -->
<?php
DB::closeInstance();
